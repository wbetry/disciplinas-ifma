-- Questão 1

create table Cat (
    idCategoria int primary key,
    categoria_veiculo varchar(45) not null
);

create table estado (
    idestado int primary key,
    nomeEstado varchar(45) not null
);

create table municipio (
    idmunicipio int primary key,
    nomeDoMunicipio varchar(150) not null,
    estado_idestado int,
    foreign key (estado_idestado) references estado(idestado)
);

create table proprietario (
    iddono int primary key,
    nomedono varchar(50),
    endereco varchar(100),
    bairro varchar(45),
    cpf varchar(45) unique not null,
    telefone varchar(45),
    rg varchar(45) unique not null,
    dataNascimentoprop date not null
);

create table Veiculo (
    idVeiculo int primary key,
    placaVeiculo varchar(50) not null,
    anoVeiculo date not null,
    Proprietario_iddono int,
    -- Marca_idveiculo int,
    municipio_idmunicipio int,
    municipio_estado_idestado int,
    Categoria_idCategoria int,
    foreign key (Proprietario_iddono) references proprietario(iddono),
    -- foreign key (Marca_idveiculo) references Marca(idveiculo),
    foreign key (municipio_idmunicipio) references municipio(idmunicipio),
    foreign key (municipio_estado_idestado) references estado(estado_idestado),
    foreign key (Categoria_idCategoria) references Cat(idCategoria)
);


-- Questao 2

-- Letra a

alter table cat rename to categoria;

-- Letra b

create table Marca (
    idVeiculo int primary key,
    marca varchar(45)
);

alter table Veiculo add Marca_idveiculo int;

alter table Veiculo add constraint Marca_idveiculo foreign key (Marca_idveiculo) references Marca(idveiculo);

-- Letra c

alter table Marca modify marca varchar(30);
