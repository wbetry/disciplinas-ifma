-- QUESTÃO 1

-- LETRA A

-- Criação da base de dados

create table cliente (
    idcliente int not null,
    nome varchar(45) not null,
    primary key(idcliente)
);

create table nota_fiscal (
    numnota varchar(20) not null,
    dataemissao date not null,
    idcliente int not null,
    primary key(numnota),
    foreign key(idcliente) references cliente(idcliente)
);

create table produto (
    coditem varchar(10) not null,
    descricao varchar(255) not null,
    valor numeric(5, 2) not null,
    primary key(coditem)
);

create table item_nota (
    numitem int not null,
    coditem varchar(10) not null,
    numnota varchar(20) not null,
    quant int not null,
    valoritem numeric(5, 2) not null,
    primary key(numitem),
    foreign key(coditem) references produto(coditem),
    foreign key(numnota) references nota_fiscal(numnota)
);

-- LETRA B

-- Inserção dos clientes

INSERT INTO cliente VALUES (1, 'Maria João');
INSERT INTO cliente VALUES (2, 'Lurdes Maria');
INSERT INTO cliente VALUES (3, 'João Mariano');
INSERT INTO cliente VALUES (4, 'Mercedes Souza');

-- Inserção das notas fiscais

INSERT INTO nota_fiscal VALUES ('N000001FSLZ', TO_DATE('2018-01-01', 'YYYY-DD-MM'), 1);
INSERT INTO nota_fiscal VALUES ('N000002FSLZ', TO_DATE('2017-02-10', 'YYYY-DD-MM'), 1);
INSERT INTO nota_fiscal VALUES ('N000032FSLZ', TO_DATE('2017-04-10', 'YYYY-DD-MM'), 1);
INSERT INTO nota_fiscal VALUES ('N000042FSLZ', TO_DATE('2018-15-02', 'YYYY-DD-MM'), 2);

-- Inserção dos produtos

INSERT INTO produto VALUES ('P00001', 'Macarrão instantâneo', 0.75);
INSERT INTO produto VALUES ('P00002', 'Arroz integral', 10);
INSERT INTO produto VALUES ('P00003', 'Atum sólido em água', 6.5);
INSERT INTO produto VALUES ('P00004', 'Atum ralado em óleo', 4.75);
INSERT INTO produto VALUES ('P00005', 'Coca-cola', 6);

-- Inserção dos relacionamentos item-nota

INSERT INTO item_nota VALUES (1, 'P00001', 'N000001FSLZ', 2, 1.5);
INSERT INTO item_nota VALUES (2, 'P00002', 'N000001FSLZ', 4, 40);
INSERT INTO item_nota VALUES (3, 'P00003', 'N000001FSLZ', 1, 6.5);
INSERT INTO item_nota VALUES (4, 'P00001', 'N000002FSLZ', 4, 3.0);
INSERT INTO item_nota VALUES (5, 'P00004', 'N000032FSLZ', 2, 9.5);
INSERT INTO item_nota VALUES (6, 'P00005', 'N000032FSLZ', 3, 18);

-- LETRA C

DELETE FROM nota_fiscal WHERE numnota = 'N000042FSLZ';

-- LETRA D

UPDATE item_nota SET quant = 4 WHERE numitem = 6;

-- LETRA E

UPDATE produto SET valor = 12 WHERE descricao = 'Arroz integral';

-- LETRA F

INSERT INTO cliente VALUES (5, 'André Fatala');
INSERT INTO cliente VALUES (6, 'Márcio Almada');
INSERT INTO cliente VALUES (7, 'Lucas Sousa');
INSERT INTO cliente VALUES (8, 'Gulherme Silveira');
INSERT INTO cliente VALUES (9, 'Izabela Silveira');

INSERT INTO nota_fiscal VALUES ('N000003FSLZ', TO_DATE('2015-02-04', 'YYYY-DD-MM'), 3);
INSERT INTO nota_fiscal VALUES ('N000004FSLZ', TO_DATE('2016-25-12', 'YYYY-DD-MM'), 4);
INSERT INTO nota_fiscal VALUES ('N000005FSLZ', TO_DATE('2017-23-04', 'YYYY-DD-MM'), 5);
INSERT INTO nota_fiscal VALUES ('N000006FSLZ', TO_DATE('2018-18-02', 'YYYY-DD-MM'), 6);
INSERT INTO nota_fiscal VALUES ('N000007FSLZ', TO_DATE('2018-01-01', 'YYYY-DD-MM'), 7);
INSERT INTO nota_fiscal VALUES ('N000008FSLZ', TO_DATE('2018-02-03', 'YYYY-DD-MM'), 8);
INSERT INTO nota_fiscal VALUES ('N000009FSLZ', TO_DATE('2018-07-02', 'YYYY-DD-MM'), 9);
INSERT INTO nota_fiscal VALUES ('N000010FSLZ', TO_DATE('2018-14-03', 'YYYY-DD-MM'), 4);
INSERT INTO nota_fiscal VALUES ('N000011FSLZ', TO_DATE('2018-21-05', 'YYYY-DD-MM'), 5);

INSERT INTO produto VALUES ('P00006', 'Fanta', 4.50);
INSERT INTO produto VALUES ('P00007', 'Pepsi', 5);

INSERT INTO item_nota VALUES (7, 'P00005', 'N000032FSLZ', 1, 6);
INSERT INTO item_nota VALUES (8, 'P00001', 'N000007FSLZ', 4, 3);
INSERT INTO item_nota VALUES (9, 'P00002', 'N000004FSLZ', 4, 40);
INSERT INTO item_nota VALUES (10, 'P00003', 'N000003FSLZ', 1, 6.5);
INSERT INTO item_nota VALUES (11, 'P00001', 'N000005FSLZ', 4, 3.0);
INSERT INTO item_nota VALUES (12, 'P00004', 'N000007FSLZ', 2, 9.5);
INSERT INTO item_nota VALUES (13, 'P00005', 'N000008FSLZ', 3, 18);
INSERT INTO item_nota VALUES (14, 'P00005', 'N000009FSLZ', 1, 6);
