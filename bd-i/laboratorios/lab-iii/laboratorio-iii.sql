------------------------------------
-- Laboratorio 3
------------------------------------

------------------------------------
-- Inicio do código do Laboratorio 2
------------------------------------

CREATE TABLE cliente (
    idcliente INT NOT NULL,
    nome VARCHAR(45) NOT NULL,
    PRIMARY KEY(idcliente)
);

CREATE TABLE nota_fiscal (
    numnota VARCHAR(20) NOT NULL,
    dataemissao DATE NOT NULL,
    idcliente INT NOT NULL,
    PRIMARY KEY(numnota),
    FOREIGN KEY(idcliente) REFERENCES cliente(idcliente)
);

CREATE TABLE produto (
    coditem VARCHAR(10) NOT NULL,
    descricao VARCHAR(255) NOT NULL,
    valor NUMERIC(5, 2) NOT NULL,
    PRIMARY KEY(coditem)
);

CREATE TABLE item_nota (
    numitem INT NOT NULL,
    coditem VARCHAR(10) NOT NULL,
    numnota VARCHAR(20) NOT NULL,
    quant INT NOT NULL,
    valoritem NUMERIC(5, 2) NOT NULL,
    PRIMARY KEY(numitem),
    FOREIGN KEY(coditem) REFERENCES produto(coditem),
    FOREIGN KEY(numnota) REFERENCES nota_fiscal(numnota)
);

INSERT INTO cliente VALUES (1, 'Maria João');
INSERT INTO cliente VALUES (2, 'Lurdes Maria');
INSERT INTO cliente VALUES (3, 'João Mariano');
INSERT INTO cliente VALUES (4, 'Mercedes Souza');

INSERT INTO nota_fiscal VALUES ('N000001FSLZ', TO_DATE('2018-01-01', 'YYYY-DD-MM'), 1);
INSERT INTO nota_fiscal VALUES ('N000002FSLZ', TO_DATE('2017-02-10', 'YYYY-DD-MM'), 1);
INSERT INTO nota_fiscal VALUES ('N000032FSLZ', TO_DATE('2017-04-10', 'YYYY-DD-MM'), 1);
INSERT INTO nota_fiscal VALUES ('N000042FSLZ', TO_DATE('2018-15-02', 'YYYY-DD-MM'), 2);

INSERT INTO produto VALUES ('P00001', 'Macarrão instantâneo', 0.75);
INSERT INTO produto VALUES ('P00002', 'Arroz integral', 10);
INSERT INTO produto VALUES ('P00003', 'Atum sólido em água', 6.5);
INSERT INTO produto VALUES ('P00004', 'Atum ralado em óleo', 4.75);
INSERT INTO produto VALUES ('P00005', 'Coca-cola', 6);

INSERT INTO item_nota VALUES (1, 'P00001', 'N000001FSLZ', 2, 1.5);
INSERT INTO item_nota VALUES (2, 'P00002', 'N000001FSLZ', 4, 40);
INSERT INTO item_nota VALUES (3, 'P00003', 'N000001FSLZ', 1, 6.5);
INSERT INTO item_nota VALUES (4, 'P00001', 'N000002FSLZ', 4, 3.0);
INSERT INTO item_nota VALUES (5, 'P00004', 'N000032FSLZ', 2, 9.5);
INSERT INTO item_nota VALUES (6, 'P00005', 'N000032FSLZ', 3, 18);

DELETE FROM nota_fiscal WHERE numnota = 'N000042FSLZ';

UPDATE item_nota SET quant = 4 WHERE numitem = 6;

UPDATE produto SET valor = 12 WHERE descricao = 'Arroz integral';

INSERT INTO cliente VALUES (5, 'André Fatala');
INSERT INTO cliente VALUES (6, 'Márcio Almada');
INSERT INTO cliente VALUES (7, 'Lucas Sousa');
INSERT INTO cliente VALUES (8, 'Gulherme Silveira');
INSERT INTO cliente VALUES (9, 'Izabela Silveira');

INSERT INTO nota_fiscal VALUES ('N000003FSLZ', TO_DATE('2015-02-04', 'YYYY-DD-MM'), 3);
INSERT INTO nota_fiscal VALUES ('N000004FSLZ', TO_DATE('2016-25-12', 'YYYY-DD-MM'), 4);
INSERT INTO nota_fiscal VALUES ('N000005FSLZ', TO_DATE('2017-23-04', 'YYYY-DD-MM'), 5);
INSERT INTO nota_fiscal VALUES ('N000006FSLZ', TO_DATE('2018-18-02', 'YYYY-DD-MM'), 6);
INSERT INTO nota_fiscal VALUES ('N000007FSLZ', TO_DATE('2018-01-01', 'YYYY-DD-MM'), 7);
INSERT INTO nota_fiscal VALUES ('N000008FSLZ', TO_DATE('2018-02-03', 'YYYY-DD-MM'), 8);
INSERT INTO nota_fiscal VALUES ('N000009FSLZ', TO_DATE('2018-07-02', 'YYYY-DD-MM'), 9);
INSERT INTO nota_fiscal VALUES ('N000010FSLZ', TO_DATE('2018-14-03', 'YYYY-DD-MM'), 4);
INSERT INTO nota_fiscal VALUES ('N000011FSLZ', TO_DATE('2018-21-05', 'YYYY-DD-MM'), 5);

INSERT INTO produto VALUES ('P00006', 'Fanta', 4.50);
INSERT INTO produto VALUES ('P00007', 'Pepsi', 5);

INSERT INTO item_nota VALUES (7, 'P00005', 'N000032FSLZ', 1, 6);
INSERT INTO item_nota VALUES (8, 'P00001', 'N000007FSLZ', 4, 3);
INSERT INTO item_nota VALUES (9, 'P00002', 'N000004FSLZ', 4, 40);
INSERT INTO item_nota VALUES (10, 'P00003', 'N000003FSLZ', 1, 6.5);
INSERT INTO item_nota VALUES (11, 'P00001', 'N000005FSLZ', 4, 3.0);
INSERT INTO item_nota VALUES (12, 'P00004', 'N000007FSLZ', 2, 9.5);
INSERT INTO item_nota VALUES (13, 'P00005', 'N000008FSLZ', 3, 18);
INSERT INTO item_nota VALUES (14, 'P00005', 'N000009FSLZ', 1, 6);

------------------------------------
-- Fim código do Laboratorio 2
------------------------------------

------------------------------------
-- Laboratorio 3 em efetivo
------------------------------------

------------------------------------
-- Letra a
------------------------------------

ALTER TABLE cliente ADD(endereco VARCHAR(45),
                        cidade VARCHAR(30),
                        estado VARCHAR(30));

------------------------------------
-- Letra b
------------------------------------

UPDATE cliente SET cidade = 'São Luis' WHERE idcliente = 1;
UPDATE cliente SET cidade = 'Paço do Lumiar' WHERE idcliente = 2;
UPDATE cliente SET cidade = 'São José de Ribamar' WHERE idcliente = 3;
UPDATE cliente SET cidade = 'São Luis' WHERE idcliente = 4;
UPDATE cliente SET cidade = 'Raposa' WHERE idcliente = 5;
UPDATE cliente SET cidade = 'São Luis' WHERE idcliente = 6;
UPDATE cliente SET cidade = 'São Luis' WHERE idcliente = 7;
UPDATE cliente SET cidade = 'Imperatriz' WHERE idcliente = 8;
UPDATE cliente SET cidade = 'São Luis' WHERE idcliente = 9;

------------------------------------
-- letra c
------------------------------------

SELECT DISTINCT cidade FROM cliente;

------------------------------------
-- letra d
------------------------------------

SELECT numnota FROM nota_fiscal
	WHERE dataemissao BETWEEN TO_DATE('01-01-2016', 'dd-mm-yyyy') AND TO_DATE('01-01-2018', 'dd-mm-yyyy');

------------------------------------
-- letra e
------------------------------------

SELECT n.numnota FROM nota_fiscal n
	JOIN cliente c USING(idcliente)
	WHERE c.cidade = 'São Luis';

------------------------------------
-- letra f
------------------------------------

SELECT AVG(i.quant * i.valoritem) FROM cliente c
	JOIN nota_fiscal USING(idcliente)
	JOIN item_nota i USING(numnota)
	WHERE c.cidade = 'São Luis';

------------------------------------
-- letra g
------------------------------------

SELECT DISTINCT p.descricao FROM produto p
	JOIN item_nota USING(coditem)
	JOIN nota_fiscal USING(numnota)
	JOIN cliente c USING(idcliente)
	WHERE c.cidade = 'São Luis';

------------------------------------
-- letra h
------------------------------------

SELECT SUM(i.quant * i.valoritem) FROM cliente
	JOIN nota_fiscal USING(idcliente)
	JOIN item_nota i USING(numnota)
	WHERE idcliente = 1;

------------------------------------
-- letra i
------------------------------------

SELECT AVG(i.valoritem) FROM item_nota i
	JOIN nota_fiscal USING(numnota)
	JOIN cliente c USING(idcliente)
	GROUP BY c.cidade;
