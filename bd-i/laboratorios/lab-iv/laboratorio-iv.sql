-- Inicio

CREATE TABLE laboratorio (
	cod_lab CHAR(5) NOT NULL,
	nome_l VARCHAR(25),
	CONSTRAINT pk_laboratorio PRIMARY KEY(cod_lab)
);

CREATE TABLE remedio (
	cod_rem CHAR(5) NOT NULL,
	nome_r VARCHAR(25) NOT NULL,
	qtd INT,
	preco NUMERIC(7, 2),
	cod_lab CHAR(5) NOT NULL,
	CONSTRAINT pk_remedio PRIMARY KEY(cod_rem),
	CONSTRAINT fk_remedio_lab FOREIGN KEY(cod_lab) REFERENCES laboratorio(cod_lab)
);

CREATE TABLE paciente (
	cpf CHAR(11) NOT NULL,
	nome VARCHAR(25) NOT NULL,
	endereco VARCHAR(30),
	idade INT,
	CONSTRAINT pk_paciente PRIMARY KEY(cpf)
);

CREATE TABLE pac_rem (
	item INT NOT NULL,
	posologia VARCHAR(30),
	data_adm DATE,
	qtd_adm INT,
	cpf CHAR(11) NOT NULL,
	cod_rem CHAR(5) NOT NULL,
	CONSTRAINT pk_pac_rem PRIMARY KEY(item),
	CONSTRAINT fk_pac_rem_pac FOREIGN KEY(cpf) REFERENCES paciente(cpf),
	CONSTRAINT fk_pac_rem_rem FOREIGN KEY(cod_rem) REFERENCES remedio(cod_rem)
);

CREATE TABLE medico (
	cod_med CHAR(5) NOT NULL,
	nome_med VARCHAR(25) NOT NULL,
	especialidade VARCHAR(30),
	CONSTRAINT pk_medico PRIMARY KEY(cod_med)
);

CREATE TABLE pac_med (
	item INT NOT NULL,
	cpf CHAR(11) NOT NULL,
	cod_med CHAR(5),
	CONSTRAINT pk_pac_med PRIMARY KEY(item),
	CONSTRAINT fk_pac_med_pac FOREIGN KEY(cpf) REFERENCES paciente(cpf),
	CONSTRAINT fk_pac_med_med FOREIGN KEY(cod_med) REFERENCES medico(cod_med)
);

-- Inserções de Laboratorio

INSERT INTO laboratorio VALUES ('LAB01', 'Laboratorio 1');
INSERT INTO laboratorio VALUES ('LAB02', 'Laboratorio 2');
INSERT INTO laboratorio VALUES ('LAB03', 'Laboratorio 3');

-- Inserções de Remedio

INSERT INTO remedio (cod_rem, nome_r, qtd, preco, cod_lab)
VALUES ('REM01', 'Dorflex', 10, 1.99, 'LAB01');

INSERT INTO remedio (cod_rem, nome_r, qtd, preco, cod_lab)
VALUES ('REM02', 'Amoxilina', 30, 19.99, 'LAB02');

INSERT INTO remedio (cod_rem, nome_r, qtd, preco, cod_lab)
VALUES ('REM03', 'Olmerzatana', 20, 29.99, 'LAB03');

INSERT INTO remedio (cod_rem, nome_r, qtd, preco, cod_lab)
VALUES ('REM04', 'Dipirona', 15, 3.99, 'LAB01');

INSERT INTO remedio (cod_rem, nome_r, qtd, preco, cod_lab)
VALUES ('REM05', 'Nimesulida', 40, 19.99, 'LAB01');

INSERT INTO remedio (cod_rem, nome_r, qtd, preco, cod_lab)
VALUES ('REM06', 'Rosuvastatina Calcica', 25, 27.99, 'LAB01');

INSERT INTO remedio (cod_rem, nome_r, qtd, preco, cod_lab)
VALUES ('REM07', 'Anlodipino', 15, 39.99, 'LAB03');

-- Inserções de Paciente

INSERT INTO paciente (cpf, nome, endereco, idade)
VALUES ('12345678910', 'Maria', 'R', 37);

INSERT INTO paciente (cpf, nome, endereco, idade)
VALUES ('12345678911', 'José', 'S', 25);

INSERT INTO paciente (cpf, nome, endereco, idade)
VALUES ('12345678912', 'João', 'T', 40);

INSERT INTO paciente (cpf, nome, endereco, idade)
VALUES ('12345678913', 'Joãozinho', 'T', 12);

INSERT INTO paciente (cpf, nome, endereco, idade)
VALUES ('12345678914', 'Mariazinha', 'R', 15);

INSERT INTO paciente (cpf, nome, endereco, idade)
VALUES ('12345678915', 'Marcio', 'T', 10);

INSERT INTO paciente (cpf, nome, endereco, idade)
VALUES ('12345678916', 'Marilia', 'R', 8);

-- Inserções na tabela medico

INSERT INTO medico (cod_med, nome_med, especialidade)
VALUES ('MED01', 'Carlos', 'CLINICO');

INSERT INTO medico (cod_med, nome_med, especialidade)
VALUES ('MED02', 'Mario', 'CLINICO');

INSERT INTO medico (cod_med, nome_med, especialidade)
VALUES ('MED03', 'Cesar', 'FISOTERAPEUTA');

INSERT INTO medico (cod_med, nome_med, especialidade)
VALUES ('MED04', 'Irineu', 'INFECTOLOGISTA');

INSERT INTO medico (cod_med, nome_med, especialidade)
VALUES ('MED05', 'Pedro', 'PEDIATRA');

INSERT INTO medico (cod_med, nome_med, especialidade)
VALUES ('MED06', 'Paulo', 'PEDIATRA');

-- Inserções na tabela pac_med

INSERT INTO pac_med (item, cpf, cod_med)
VALUES (1, '12345678910', 'MED01');

INSERT INTO pac_med (item, cpf, cod_med)
VALUES (2, '12345678910', 'MED02');

INSERT INTO pac_med (item, cpf, cod_med)
VALUES (3, '12345678910', 'MED03');

INSERT INTO pac_med (item, cpf, cod_med)
VALUES (4, '12345678911', 'MED01');

INSERT INTO pac_med (item, cpf, cod_med)
VALUES (5, '12345678911', 'MED04');

INSERT INTO pac_med (item, cpf, cod_med)
VALUES (6, '12345678912', 'MED01');

INSERT INTO pac_med (item, cpf, cod_med)
VALUES (7, '12345678912', 'MED02');

INSERT INTO pac_med (item, cpf, cod_med)
VALUES (8, '12345678912', 'MED04');

INSERT INTO pac_med (item, cpf, cod_med)
VALUES (9, '12345678913', 'MED01');

INSERT INTO pac_med (item, cpf, cod_med)
VALUES (10, '12345678914', 'MED02');

INSERT INTO pac_med (item, cpf, cod_med)
VALUES (11, '12345678914', 'MED06');

INSERT INTO pac_med (item, cpf, cod_med)
VALUES (12, '12345678915', 'MED05');

INSERT INTO pac_med (item, cpf, cod_med)
VALUES (13, '12345678916', 'MED06');

-- Inserções na tabela pac_rem

INSERT INTO pac_rem (item, posologia, data_adm, qtd_adm, cpf, cod_rem)
VALUES (1, '1', TO_DATE('2018-23-01', 'YYYY-DD-MM'), 10, '12345678910', 'REM01');

INSERT INTO pac_rem (item, posologia, data_adm, qtd_adm, cpf, cod_rem)
VALUES (2, '2', TO_DATE('2018-22-02', 'YYYY-DD-MM'), 3, '12345678910', 'REM01');

INSERT INTO pac_rem (item, posologia, data_adm, qtd_adm, cpf, cod_rem)
VALUES (3, '1', TO_DATE('2018-27-01', 'YYYY-DD-MM'), 2, '12345678911', 'REM03');

INSERT INTO pac_rem (item, posologia, data_adm, qtd_adm, cpf, cod_rem)
VALUES (4, '2', TO_DATE('2018-15-02', 'YYYY-DD-MM'), 2, '12345678911', 'REM07');

INSERT INTO pac_rem (item, posologia, data_adm, qtd_adm, cpf, cod_rem)
VALUES (5, '1', TO_DATE('2018-10-01', 'YYYY-DD-MM'), 4, '12345678912', 'REM02');

INSERT INTO pac_rem (item, posologia, data_adm, qtd_adm, cpf, cod_rem)
VALUES (6, '2', TO_DATE('2018-21-02', 'YYYY-DD-MM'), 3, '12345678912', 'REM04');

INSERT INTO pac_rem (item, posologia, data_adm, qtd_adm, cpf, cod_rem)
VALUES (7, '1', TO_DATE('2018-28-01', 'YYYY-DD-MM'), 6, '12345678913', 'REM01');

INSERT INTO pac_rem (item, posologia, data_adm, qtd_adm, cpf, cod_rem)
VALUES (8, '2', TO_DATE('2018-28-02', 'YYYY-DD-MM'), 2, '12345678913', 'REM02');

INSERT INTO pac_rem (item, posologia, data_adm, qtd_adm, cpf, cod_rem)
VALUES (9, '1', TO_DATE('2018-21-01', 'YYYY-DD-MM'), 1, '12345678913', 'REM04');

INSERT INTO pac_rem (item, posologia, data_adm, qtd_adm, cpf, cod_rem)
VALUES (10, '2', TO_DATE('2018-14-02', 'YYYY-DD-MM'), 4, '12345678914', 'REM01');

INSERT INTO pac_rem (item, posologia, data_adm, qtd_adm, cpf, cod_rem)
VALUES (11, '1', TO_DATE('2018-01-01', 'YYYY-DD-MM'), 3, '12345678914', 'REM02');

INSERT INTO pac_rem (item, posologia, data_adm, qtd_adm, cpf, cod_rem)
VALUES (12, '2', TO_DATE('2018-12-02', 'YYYY-DD-MM'), 3, '12345678914', 'REM04');

---------------------------------------------
-- Respostas
---------------------------------------------

-- Questão 1

ALTER TABLE paciente ADD cidade VARCHAR(25);

SELECT m.nome_med FROM medico m;

UPDATE paciente SET cidade = 'São Luis' WHERE cpf = '12345678910';

UPDATE paciente SET cidade = 'Raposa' WHERE cpf = '12345678911';

UPDATE paciente SET cidade = 'São José de Ribamar' WHERE cpf = '12345678912';

UPDATE paciente SET cidade = 'São Luis' WHERE cpf = '12345678913';

UPDATE paciente SET cidade = 'São Luis' WHERE cpf = '12345678914';

UPDATE paciente SET cidade = 'São Luis' WHERE cpf = '12345678915';

UPDATE paciente SET cidade = 'Imperatriz' WHERE cpf = '12345678916';

-- Questão 2

-- TODO Implements

-- Questão 3

SELECT m.nome_med, AVG(p.idade) FROM medico m
JOIN pac_med pm ON pm.cod_med = m.cod_med
JOIN paciente p ON p.cpf = pm.cpf
GROUP BY m.nome_med
ORDER BY m.nome_med;

-- Questão 4

-- Média da idade de todos os pacientes de todos os médicos

SELECT AVG(idade) FROM paciente;

-- Médicos que atenderam pacientes mais velhos do que a média de idade

SELECT DISTINCT m.nome_med FROM medico m
JOIN pac_med pm ON pm.cod_med = m.cod_med
JOIN paciente p ON p.cpf = pm.cpf
GROUP BY m.nome_med, p.idade
HAVING p.idade > (
	SELECT AVG(idade) FROM paciente
) ORDER BY m.nome_med;

-- Questão 5

SELECT SUM(pr.qtd_adm) FROM medico m
JOIN pac_med pm ON pm.cod_med = m.cod_med
JOIN paciente p ON p.cpf = pm.cpf
JOIN pac_rem pr ON pr.cpf = p.cpf;

-- Questão 6

-- Quantidade de remedios receitudos pelos médicos

SELECT COUNT(pr.cod_rem) FROM pac_rem pr
JOIN paciente p ON p.cpf = pr.cpf
JOIN pac_med pm ON pm.cpf = p.cpf
JOIN medico m ON m.cod_med = pm.cod_med
GROUP BY m.nome_med;

-- Questão 7

-- Média dos Gastos

SELECT AVG(qtd_adm * preco) FROM remedio r
JOIN pac_rem pr ON pr.cod_rem = r.cod_rem;

-- Nomes dos pacientes que gastaram mais do que a média dos gastos

SELECT DISTINCT p.nome FROM paciente p
JOIN pac_rem pr ON pr.cpf = p.cpf
JOIN remedio r ON r.cod_rem = pr.cod_rem
WHERE (qtd_adm * preco) > (
	SELECT AVG(qtd_adm * preco) FROM remedio r
	JOIN pac_rem pr ON pr.cod_rem = r.cod_rem
);

-- Questão 8

CREATE VIEW view_questao_8 AS
SELECT nome_r AS "Nome do Remédio" FROM remedio
WHERE cod_rem IN (
	SELECT cod_rem FROM pac_rem
	WHERE cpf IN (
		SELECT cpf FROM paciente
		WHERE cidade = 'São Luis'
	)
) ORDER BY nome_r;

-- Questão 9

CREATE VIEW view_questao_9 AS
SELECT nome AS "Nome do Paciente" FROM paciente
WHERE cpf IN (
	SELECT cpf FROM pac_rem
	WHERE cod_rem IN (
		SELECT cod_rem FROM remedio
		WHERE cod_lab IN (
			SELECT cod_lab FROM laboratorio
			WHERE nome_l = 'Laboratorio 1'
		)
	)
) ORDER BY nome;

-- Questão 10

CREATE VIEW view_questao_10 AS
SELECT nome_med AS "Nome do Médico" FROM medico
WHERE cod_med IN (
	 SELECT cod_med FROM pac_med
	 WHERE cpf IN (
	 	SELECT cpf FROM paciente
	 	WHERE cpf IN (
	 		SELECT cpf FROM pac_rem
	 		WHERE cod_rem IN (
	 			SELECT cod_rem FROM remedio
	 			WHERE cod_lab IN (
	 				SELECT cod_lab FROM laboratorio
	 				WHERE nome_l = 'Laboratorio 3'
	 			)
	 		)
	 	)
	 )
) ORDER BY nome_med;
