/*
 * Aluno: Jorge Luis Lopes Amorim
 */

/* Questão 1 */

-- Criação das tabelas

CREATE TABLE aluno (
	nroalun INT NOT NULL,
	nomealun VARCHAR(45) NOT NULL,
	formacao VARCHAR(25) NOT NULL,
	nivel VARCHAR(20) NOT NULL,
	idade INT NOT NULL,
	PRIMARY KEY(nroalun)
);

CREATE TABLE departamento (
	iddepto INT NOT NULL,
	nomedepto VARCHAR(25),
	PRIMARY KEY(iddepto)
);

CREATE TABLE professor (
	idprof INT NOT NULL,
	nomeprof VARCHAR(45) NOT NULL,
	iddepto INT NOT NULL,
	PRIMARY KEY(idprof),
	FOREIGN KEY(iddepto) REFERENCES departamento(iddepto)
);

CREATE TABLE curso (
	nome VARCHAR(30) NOT NULL,
	horario TIMESTAMP NOT NULL,
	sala VARCHAR(5) NOT NULL,
	idprof INT NOT NULL,
	PRIMARY KEY(nome),
	FOREIGN KEY(idprof) REFERENCES professor(idprof)
);

CREATE TABLE matriculado (
	nroalun INT NOT NULL,
	nomecurso VARCHAR(30) NOT NULL,
	PRIMARY KEY(nroalun, nomecurso),
	FOREIGN KEY(nroalun) REFERENCES aluno(nroalun),
	FOREIGN KEY(nomecurso) REFERENCES curso(nome)
);

-- Inserção de alunos

INSERT INTO aluno VALUES (1, 'Chris', 'História', 'Pleno', 27);
INSERT INTO aluno VALUES (2, 'Marcos', 'História', 'JR', 24);
INSERT INTO aluno VALUES (3, 'Greg', 'História', 'Sênior', 26);
INSERT INTO aluno VALUES (4, 'Kenny', 'Sistemas de Informação', 'JR', 29);
INSERT INTO aluno VALUES (5, 'Larissa', 'Sistemas de Informação', 'Pleno', 32);
INSERT INTO aluno VALUES (6, 'Lucas', 'Eng. Elétrica', 'Sênior', 35);
INSERT INTO aluno VALUES (7, 'Jon', 'Eng. Elétrica', 'Pleno', 28);
INSERT INTO aluno VALUES (8, 'Lourival', 'História', 'Pleno', 27);
INSERT INTO aluno VALUES (9, 'Juvenal', 'Sistemas de Informação', 'JR', 24);
INSERT INTO aluno VALUES (10, 'Gabriela', 'Física', 'JR', 23);
INSERT INTO aluno VALUES (11, 'Mathias', 'História', 'Sênior', 33);
INSERT INTO aluno VALUES (12, 'Mauro', 'Eng. Computação', 'Sênior', 44);


-- Inserção de departamentos

INSERT INTO departamento VALUES(1, 'DEP1');
INSERT INTO departamento VALUES(2, 'DEP2');
INSERT INTO departamento VALUES(3, 'DEP3');
INSERT INTO departamento VALUES(4, 'DEP4');
INSERT INTO departamento VALUES(5, 'DEP5');

-- Inserção de professores

INSERT INTO professor VALUES (1, 'I. Teach', 1);
INSERT INTO professor VALUES (2, 'Tiago', 2);
INSERT INTO professor VALUES (3, 'Julius', 1);
INSERT INTO professor VALUES (4, 'Mario', 3);
INSERT INTO professor VALUES (5, 'Alan', 1);
INSERT INTO professor VALUES (6, 'Caio', 4);
INSERT INTO professor VALUES (7, 'Luis', 1);
INSERT INTO professor VALUES (8, 'Paulo', 5);
INSERT INTO professor VALUES (9, 'Pedro', 5);
INSERT INTO professor VALUES (10, 'Framilson', 2);

-- Inserção de cursos

INSERT INTO curso VALUES ('Curso A', TO_DATE('23-04-2018 14:00:00', 'DD-MM-YYYY HH24:MI:SS'), 'A123', 1);
INSERT INTO curso VALUES ('Curso B', TO_DATE('24-06-2018 08:00:00', 'DD-MM-YYYY HH24:MI:SS'), 'R128', 2);
INSERT INTO curso VALUES ('Curso C', TO_DATE('21-03-2018 14:00:00', 'DD-MM-YYYY HH24:MI:SS'), 'B123', 3);
INSERT INTO curso VALUES ('Curso D', TO_DATE('18-02-2018 19:00:00', 'DD-MM-YYYY HH24:MI:SS'), 'A123', 4);
INSERT INTO curso VALUES ('Curso E', TO_DATE('08-06-2018 14:00:00', 'DD-MM-YYYY HH24:MI:SS'), 'R128', 1);
INSERT INTO curso VALUES ('Curso F', TO_DATE('14-01-2018 16:00:00', 'DD-MM-YYYY HH24:MI:SS'), 'A123', 3);
INSERT INTO curso VALUES ('Curso G', TO_DATE('23-03-2018 14:00:00', 'DD-MM-YYYY HH24:MI:SS'), 'R128', 4);
INSERT INTO curso VALUES ('Curso H', TO_DATE('10-05-2018 14:00:00', 'DD-MM-YYYY HH24:MI:SS'), 'A123', 1);
INSERT INTO curso VALUES ('Curso I', TO_DATE('18-03-2018 16:00:00', 'DD-MM-YYYY HH24:MI:SS'), 'R128', 3);
INSERT INTO curso VALUES ('Curso J', TO_DATE('23-04-2018 14:00:00', 'DD-MM-YYYY HH24:MI:SS'), 'A123', 3);

-- Inserção de matriculas

INSERT INTO matriculado VALUES (1, 'Curso A');
INSERT INTO matriculado VALUES (2, 'Curso B');
INSERT INTO matriculado VALUES (3, 'Curso C');
INSERT INTO matriculado VALUES (4, 'Curso D');
INSERT INTO matriculado VALUES (5, 'Curso E');
INSERT INTO matriculado VALUES (6, 'Curso F');
INSERT INTO matriculado VALUES (7, 'Curso G');
INSERT INTO matriculado VALUES (8, 'Curso H');
INSERT INTO matriculado VALUES (1, 'Curso C');
INSERT INTO matriculado VALUES (3, 'Curso B');
INSERT INTO matriculado VALUES (1, 'Curso B');
INSERT INTO matriculado VALUES (5, 'Curso A');
INSERT INTO matriculado VALUES (5, 'Curso B');
INSERT INTO matriculado VALUES (7, 'Curso A');
INSERT INTO matriculado VALUES (3, 'Curso D');
INSERT INTO matriculado VALUES (6, 'Curso A');
INSERT INTO matriculado VALUES (2, 'Curso A');
INSERT INTO matriculado VALUES (1, 'Curso J');
INSERT INTO matriculado VALUES (5, 'Curso C');
INSERT INTO matriculado VALUES (5, 'Curso D');
INSERT INTO matriculado VALUES (5, 'Curso F');

/* Respostas das consultas */

/* Letra a */

SELECT a.nomealun FROM aluno a
JOIN matriculado m ON m.nroalun = a.nroalun
JOIN curso c ON c.nome = m.nomecurso
JOIN professor p ON p.idprof = c.idprof
WHERE a.nivel = 'JR' AND p.nomeprof = 'I. Teach'
ORDER BY a.nomealun;

/* Letra b */

SELECT nomealun FROM aluno
WHERE idade = (
	SELECT MAX(idade) FROM aluno
	WHERE formacao = 'História' OR nroalun IN (
		SELECT a.nroalun FROM aluno a
		JOIN matriculado m ON m.nroalun = a.nroalun
		JOIN curso c ON c.nome = m.nomecurso
		JOIN professor p ON p.idprof = c.idprof
		WHERE p.nomeprof = 'I. Teach'
	)
);

/* Letra c */

SELECT c.nome FROM curso c
WHERE c.sala = 'R128' OR c.nome IN (
	SELECT c.nome FROM curso c
	JOIN matriculado m ON m.nomecurso = c.nome
	GROUP BY c.nome
	HAVING COUNT(*) >= 5
)
ORDER BY c.nome;

/* Letra d */

SELECT DISTINCT a1.nomealun FROM aluno a1
JOIN matriculado m1 ON m1.nroalun = a1.nroalun
JOIN curso c1 ON c1.nome = m1.nomecurso
WHERE a1.nroalun IN (
	SELECT m2.nroalun FROM matriculado m2
	JOIN curso c2 ON c2.nome = m2.nomecurso
	GROUP BY m2.nroalun, c1.horario, c2.horario
	HAVING COUNT(c2.nome) = 2 AND c1.horario = c2.horario
)
ORDER BY a1.nomealun;

/* Letra e */

SELECT p.nomeprof FROM professor p
JOIN curso c ON c.idprof = p.idprof
GROUP BY p.NOMEPROF
HAVING COUNT(DISTINCT c.sala) = (
	SELECT COUNT(DISTINCT c.sala) FROM professor p
	JOIN curso c ON c.idprof = p.idprof
);

/* Letra f */

-- Solução 1

SELECT nomeprof FROM professor
WHERE idprof IN (
	SELECT idprof FROM curso
	WHERE nome IN (
		SELECT nomecurso FROM matriculado
		GROUP BY nomecurso
		HAVING COUNT(DISTINCT nroalun) < 5
	)
)
ORDER BY nomeprof;

-- Solução 2

SELECT DISTINCT p.nomeprof FROM professor p
JOIN curso c ON c.idprof = p.idprof
WHERE c.nome IN (
	SELECT nomecurso FROM matriculado
	GROUP BY nomecurso
	HAVING COUNT(DISTINCT nroalun) < 5
)
ORDER BY p.nomeprof;

-- Solução 3

SELECT p.nomeprof FROM professor p
JOIN curso c ON c.idprof = p.idprof
JOIN matriculado m ON m.nomecurso = c.nome
GROUP BY p.nomeprof
HAVING COUNT(DISTINCT m.nroalun) < 5
ORDER BY p.nomeprof;

/* Letra g */

SELECT nivel, AVG(idade) FROM aluno
GROUP BY nivel
ORDER BY nivel;

/* Letra h */

SELECT nivel, AVG(idade) FROM aluno
WHERE nivel != 'JR'
GROUP BY nivel
ORDER BY nivel;

/* Letra i */

SELECT p.nomeprof, COUNT(c.nome) FROM professor p
JOIN curso c ON c.idprof = p.idprof
WHERE p.nomeprof NOT IN (
	SELECT p2.nomeprof FROM professor p2
	JOIN curso c2 ON c2.idprof = p2.idprof
	GROUP BY p2.nomeprof
	HAVING COUNT(c2.sala) > 1
) AND c.sala = 'R128'
GROUP BY p.nomeprof
ORDER BY p.nomeprof;

/* Letra j */

SELECT a.nomealun, COUNT(*) FROM aluno a
JOIN matriculado m ON m.nroalun = a.nroalun
GROUP BY a.nomealun
HAVING COUNT(*) <= 6
ORDER BY a.nomealun;

/* Letra k */

-- Solução 1: Somente Com Subconsultas

SELECT nomealun FROM aluno
WHERE nroalun NOT IN (
	SELECT nroalun FROM aluno WHERE nroalun IN (
		SELECT nroalun FROM matriculado
	)
);

-- Solução 2: Mesclando Subconsultas com join

SELECT nomealun FROM aluno
WHERE nroalun NOT IN (
	SELECT a.nroalun FROM aluno a
	JOIN matriculado m ON m.nroalun = a.nroalun
);
