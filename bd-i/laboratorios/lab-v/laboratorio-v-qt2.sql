/*
 * Aluno: Jorge Luis Lopes Amorim
 */

/* Questão 2 */

-- Criação das tabelas

CREATE TABLE voos (
	nrovoo INT NOT NULL,
	de VARCHAR(30) NOT NULL,
	para VARCHAR(30) NOT NULL,
	distancia TIMESTAMP NOT NULL,
	partida TIMESTAMP NOT NULL,
	chegada TIMESTAMP NOT NULL,
	preco NUMERIC(8, 2),
	PRIMARY KEY(nrovoo),
	CHECK(preco > 0),
	CHECK(de != para)
);

CREATE TABLE aeronave (
	idaero INT NOT NULL,
	nomeaero VARCHAR(30) NOT NULL,
	distlimite INT,
	PRIMARY KEY(idaero)
);

CREATE TABLE funcionarios (
	idfuncion INT NOT NULL,
	nomefuncion VARCHAR(45) NOT NULL,
	salario NUMERIC(8, 2) NOT NULL,
	PRIMARY KEY(idfuncion)
);

CREATE TABLE certificado (
	idfuncion INT NOT NULL,
	idaero INT NOT NULL,
	PRIMARY KEY(idfuncion, idaero),
	FOREIGN KEY(idfuncion) REFERENCES funcionarios(idfuncion),
	FOREIGN KEY(idaero) REFERENCES aeronave(idaero)
);

-- Conforme alteração no modelo de dados

ALTER TABLE voos MODIFY distancia INT;

-- Inserindo aeronaves

INSERT INTO aeronave VALUES (1, 'Boeing 777-300ER', 9700);
INSERT INTO aeronave VALUES (2, 'Airbus A320', 5700);
INSERT INTO aeronave VALUES (3, 'Airbus A330', 8000);
INSERT INTO aeronave VALUES (4, 'Airbus A380', 2500);
INSERT INTO aeronave VALUES (5, 'Airbus A400M', 4000);
INSERT INTO aeronave VALUES (6, 'Airbus A318', 5700);
INSERT INTO aeronave VALUES (7, 'Airbus A319', 7200);
INSERT INTO aeronave VALUES (8, 'ATR 42-500', 1500);
INSERT INTO aeronave VALUES (9, 'ATR 72-500', 1000);
INSERT INTO aeronave VALUES (10, 'Embraer 175', 3300);

-- Inserindo funcionarios

INSERT INTO funcionarios VALUES (1, 'Karla', 80000);
INSERT INTO funcionarios VALUES (2, 'Marcos', 65000);
INSERT INTO funcionarios VALUES (3, 'Helio', 120000);
INSERT INTO funcionarios VALUES (4, 'Lazaro', 89000);
INSERT INTO funcionarios VALUES (5, 'Lucas', 49000);
INSERT INTO funcionarios VALUES (6, 'Gabriel', 79000);
INSERT INTO funcionarios VALUES (7, 'Josias', 8000);
INSERT INTO funcionarios VALUES (8, 'Jorge', 4000);
INSERT INTO funcionarios VALUES (9, 'Mario', 7000);
INSERT INTO funcionarios VALUES (10, 'Maria', 6000);
INSERT INTO funcionarios VALUES (11, 'Patricia', 5000);
INSERT INTO funcionarios VALUES (12, 'Patrick', 9000);
INSERT INTO funcionarios VALUES (13, 'Sabrina', 3000);
INSERT INTO funcionarios VALUES (14, 'Joniel', 8500);
INSERT INTO funcionarios VALUES (15, 'Fabio', 6500);
INSERT INTO funcionarios VALUES (16, 'Rafael', 5500);
INSERT INTO funcionarios VALUES (17, 'Irlan', 4000);
INSERT INTO funcionarios VALUES (18, 'Willkerson', 4500);
INSERT INTO funcionarios VALUES (19, 'Mateus', 45000);
INSERT INTO funcionarios VALUES (20, 'Alan', 5900);
INSERT INTO funcionarios VALUES (21, 'Victor', 7800);
INSERT INTO funcionarios VALUES (22, 'Paloma', 85000);
INSERT INTO funcionarios VALUES (23, 'Tiago o Mago', 82000);
INSERT INTO funcionarios VALUES (24, 'Sergio', 110000);

-- Inserindo pilotos

INSERT INTO certificado VALUES (1, 1);
INSERT INTO certificado VALUES (2, 2);
INSERT INTO certificado VALUES (3, 3);
INSERT INTO certificado VALUES (4, 4);
INSERT INTO certificado VALUES (5, 5);
INSERT INTO certificado VALUES (6, 6);
INSERT INTO certificado VALUES (1, 7);
INSERT INTO certificado VALUES (2, 8);
INSERT INTO certificado VALUES (19, 1);
INSERT INTO certificado VALUES (20, 1);
INSERT INTO certificado VALUES (21, 1);
INSERT INTO certificado VALUES (21, 3);
INSERT INTO certificado VALUES (21, 4);
INSERT INTO certificado VALUES (21, 5);

INSERT INTO voos VALUES (1,
						'Los Angeles',
						'Honolulu',
						5000,
						TO_DATE('23-04-2018 14:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						TO_DATE('23-04-2018 20:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						40000);

INSERT INTO voos VALUES (2,
						'Los Angeles',
						'Honolulu',
						5000,
						TO_DATE('23-04-2018 14:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						TO_DATE('23-04-2018 22:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						2000);

INSERT INTO voos VALUES (3,
						'Los Angeles',
						'Chicago',
						4000,
						TO_DATE('25-04-2018 10:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						TO_DATE('25-04-2018 16:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						4000);

INSERT INTO voos VALUES (4,
						'Chicago',
						'Miami',
						2000,
						TO_DATE('23-04-2018 16:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						TO_DATE('23-04-2018 20:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						3000);

INSERT INTO voos VALUES (5,
						'Madison',
						'Chicago',
						2000,
						TO_DATE('28-05-2018 12:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						TO_DATE('28-05-2018 14:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						3000);

INSERT INTO voos VALUES (6,
						'Madison',
						'Miami',
						3000,
						TO_DATE('28-05-2018 12:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						TO_DATE('28-05-2018 14:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						3000);

INSERT INTO voos VALUES (7,
						'Chicago',
						'Nova York',
						2000,
						TO_DATE('28-05-2018 15:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						TO_DATE('28-05-2018 17:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						3500);

INSERT INTO voos VALUES (8,
						'Miami',
						'Nova York',
						3000,
						TO_DATE('28-05-2018 15:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						TO_DATE('28-05-2018 18:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						2000);

INSERT INTO voos VALUES (9,
						'Madison',
						'Southpark',
						1000,
						TO_DATE('28-05-2018 12:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						TO_DATE('28-05-2018 13:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						1000);

INSERT INTO voos VALUES (10,
						'Southpark',
						'Las Vegas',
						2000,
						TO_DATE('28-05-2018 14:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						TO_DATE('28-05-2018 15:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						3500);

INSERT INTO voos VALUES (11,
						'Las Vegas',
						'Nova York',
						3000,
						TO_DATE('28-05-2018 16:00:00', 'DD-MM-YYYY HH24:MI:SS'),
						TO_DATE('28-05-2018 17:30:00', 'DD-MM-YYYY HH24:MI:SS'),
						3000);

/* Respostas das consultas */

/* Letra a */

SELECT nomeaero FROM aeronave
WHERE idaero IN (
	SELECT a.idaero FROM aeronave a
	JOIN certificado c ON c.idaero = a.idaero
	JOIN funcionarios f ON f.idfuncion = c.idfuncion
	WHERE f.salario > 80000
);

/* Letra b */

SELECT c.idfuncion, MAX(a.distlimite) FROM certificado c
JOIN aeronave a ON a.idaero = c.idaero
WHERE c.idfuncion IN (
	SELECT idfuncion FROM certificado
	GROUP BY idfuncion
	HAVING COUNT(idaero) > 3
)
GROUP BY c.idfuncion
ORDER BY c.idfuncion;

/* Letra c */

SELECT DISTINCT f.nomefuncion FROM funcionarios f
JOIN certificado c ON c.idfuncion = f.idfuncion
WHERE f.salario < (
	SELECT MIN(preco) FROM voos
	WHERE de = 'Los Angeles' AND para = 'Honolulu'
);

/* Letra d */

SELECT a.nomeaero, AVG(f.salario) FROM aeronave a
JOIN certificado c ON c.idaero = a.idaero
JOIN funcionarios f ON f.idfuncion = c.idfuncion
WHERE a.distlimite > 1000
GROUP BY a.nomeaero
ORDER BY a.nomeaero;

/* Letra e */

SELECT f.nomefuncion FROM funcionarios f
JOIN certificado c ON c.idfuncion = f.idfuncion
JOIN aeronave a ON a.idaero = c.idaero
WHERE a.nomeaero LIKE 'Boeing%';

/* Letra f */

SELECT idaero FROM aeronave
WHERE distlimite > (
	SELECT MAX(distancia) FROM voos
	WHERE de = 'Los Angeles' AND para = 'Chicago'
);

/* Letra g */

SELECT UNIQUE v.de, v.para FROM voos v
WHERE v.distancia < (
	SELECT MAX(distlimite) FROM aeronave a
	JOIN certificado c ON c.idaero = a.idaero
	JOIN funcionarios f ON f.idfuncion = c.idfuncion
	WHERE f.salario > 100000
);

/* Letra h */

SELECT DISTINCT nomefuncion FROM funcionarios f
JOIN certificado c ON c.idfuncion = f.idfuncion
JOIN aeronave a ON a.idaero = c.idaero
WHERE c.idfuncion NOT IN (
	SELECT c.idaero FROM certificado c
	JOIN aeronave a ON a.idaero = c.idaero
	WHERE a.nomeaero LIKE 'Boeing%'
) AND a.distlimite > 3000;

/* Letra i */

-- Solução 1: Com Subconsultas

SELECT nrovoo, partida FROM voos
WHERE de = 'Madison' AND (
	(para = 'Nova York') AND chegada < TO_DATE('28-05-2018 18:00:00', 'DD-MM-YYYY HH24:MI:SS') -- Voo Direto
	OR (para IN (
		SELECT de FROM voos -- Primeira escala
		WHERE para = 'Nova York' AND chegada < TO_DATE('28-05-2018 18:00:00', 'DD-MM-YYYY HH24:MI:SS')
	))
	OR (para IN (
		SELECT de FROM voos -- Segunda Escala
		WHERE para IN (
			SELECT de FROM voos
			WHERE para = 'Nova York' AND chegada < TO_DATE('28-05-2018 18:00:00', 'DD-MM-YYYY HH24:MI:SS')
		)
	))
)
ORDER BY nrovoo;

-- Solução 2: Com Produto Cartesiano da tabela voos

SELECT UNIQUE v1.nrovoo, v1.partida FROM voos v1, voos v2, voos v3
WHERE v1.de = 'Madison' AND (
	(v1.para = v2.de AND v2.para = v3.de
		AND v3.para = 'Nova York' -- Voo Direto
		AND v3.chegada < TO_DATE('28-05-2018 18:00:00', 'DD-MM-YYYY HH24:MI:SS')
	)
	OR (v1.para = v2.de AND v2.para = 'Nova York' -- Primeira Escala
		AND v2.chegada < TO_DATE('28-05-2018 18:00:00', 'DD-MM-YYYY HH24:MI:SS')
	)
	OR (v1.para = 'Nova York' -- Segunda Escala
		AND v2.chegada < TO_DATE('28-05-2018 18:00:00', 'DD-MM-YYYY HH24:MI:SS')
	)
)
ORDER BY v1.nrovoo;

/* Letra j */

SELECT AVG(f2.salario) - AVG(f1.salario) FROM funcionarios f1, funcionarios f2
JOIN certificado c ON c.idfuncion = f2.idfuncion;

/* Letra k */

-- Solução 1: Somente Com Subconsultas

SELECT nomefuncion, salario FROM funcionarios
WHERE salario > (
	SELECT AVG(salario) FROM funcionarios
	WHERE idfuncion IN (
		SELECT idfuncion FROM certificado
	)
) AND idfuncion NOT IN (
	SELECT idfuncion FROM certificado
)
ORDER BY nomefuncion;

-- Solução 2: Mesclando Subconsultas com join

SELECT nomefuncion, salario FROM funcionarios
WHERE salario > (
	SELECT AVG(salario) FROM funcionarios f
	JOIN certificado c ON c.idfuncion = f.idfuncion
) AND idfuncion NOT IN (
	SELECT idfuncion FROM certificado
)
ORDER BY nomefuncion;
