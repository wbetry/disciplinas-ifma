package br.edu.ifma.bd1.conexao;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * 
 * @author jorgeluis
 */
public class Conexao {

    private static Connection db = null;

    private Conexao() {}

    /**
     * Método responsável por abrir uma conexão com o banco.
     * A conexão será construída a partir de um arquivo de texto contendo nas duas linhas iniciais o usuário e a senha.     * 
     * 
     * @return Connection
     * @throws SQLException 
     */
    public static Connection getConnection() throws SQLException {
        String usuario = "";
        String senha = "";
        
        try (Scanner file = new Scanner(new File("conf.txt"))) {
            
            if (!file.hasNext()) {
                throw new Exception("Usuario inválido");
            }
            
            usuario = file.nextLine();
            
            if (!file.hasNext()) {
                throw  new Exception("Senha inválida");
            }
            senha = file.nextLine();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
        if (Conexao.db == null) {
            System.out.println("Estabelecendo conexão com o banco de dados");
            Conexao.db = DriverManager.getConnection("jdbc:oracle:thin:@localhost", usuario, senha);
        }
        
        return Conexao.db;
    }

    public static void closeConnection() throws SQLException {
        if (Conexao.db != null) {
            System.out.println("Fechando a conexão com o banco de dados");
            Conexao.db.close();
        }
    }
}
