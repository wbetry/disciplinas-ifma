package br.edu.ifma.bd1.modelo;

/**
 * 
 * @author jorgeluis
 */
public class Aluno extends Modelo {
    
    public Aluno() {
        super();
        this.table = "aluno";
        this.primaryKey = "id";
        this.foreignKey = null;
    }

}
