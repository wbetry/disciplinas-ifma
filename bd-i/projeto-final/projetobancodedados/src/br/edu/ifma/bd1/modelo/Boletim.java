package br.edu.ifma.bd1.modelo;

/**
 *
 * @author jorgeluis
 */
public class Boletim  extends Modelo {
    public Boletim() {
        super();
        this.table = "boletim";
        this.primaryKey = "id";
        this.foreignKey = null;
    }
}
