package br.edu.ifma.bd1.modelo;

/**
 *
 * @author jorgeluis
 */
public class Curso extends Modelo {
    public Curso() {
        super();
        this.table = "curso";
        this.primaryKey = "id";
        this.foreignKey = null;
    }
}
