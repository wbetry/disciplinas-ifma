package br.edu.ifma.bd1.modelo;

/**
 * 
 * @author jorgeluis
 */
abstract public class Modelo {

    protected String table;
    protected String primaryKey;
    protected String foreignKey;

    public Modelo() {
    }

    public String getTable() {
        return this.table;
    }

    public String getPrimaryKey() {
        return this.primaryKey;
    }
    
    public String getForeignKey() {
        return this.primaryKey;
    }

}
