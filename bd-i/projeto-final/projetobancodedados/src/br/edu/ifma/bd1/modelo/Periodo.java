package br.edu.ifma.bd1.modelo;

/**
 *
 * @author jorgeluis
 */
public class Periodo  extends Modelo {
    public Periodo() {
        super();
        this.table = "periodo";
        this.primaryKey = "id";
        this.foreignKey = null;
    }
}
