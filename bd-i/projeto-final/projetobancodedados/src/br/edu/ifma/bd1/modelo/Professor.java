package br.edu.ifma.bd1.modelo;

/**
 *
 * @author jorgeluis
 */
public class Professor extends Modelo  {
    public Professor() {
        super();
        this.table = "professor";
        this.primaryKey = "id";
        this.foreignKey = null;
    }
}
