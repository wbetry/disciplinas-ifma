package br.edu.ifma.bd1.querybuilder;

/**
 * 
 * @author jorgeluis
 */
public class Condicoes {

    private String clausules;

    public Condicoes(QueryBuilder qb) {
        this.clausules = qb.toString();
    }

    private Condicoes(String clausules) {
        this.clausules = clausules;
    }

    public Condicoes and(String field, String comparator, String value) {
        return this.newClausule("AND", field, comparator, value);
    }

    public Condicoes or(String field, String comparator, String value) {
        return this.newClausule("OR", field, comparator, value);
    }

    private Condicoes newClausule(String operator, String field, String comparator, String value) {
        StringBuilder query = new StringBuilder(this.clausules);

        query.append(operator + " ");
        query.append(field + " ");
        query.append(comparator + " ");
        query.append(value + " ");

        return new Condicoes(query.toString());
    }

    @Override
    public String toString() {
        return this.clausules;
    }

    public QueryBuilder toQueryBuilder() {
        return new QueryBuilder(this.clausules);
    }
}
