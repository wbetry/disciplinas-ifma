package br.edu.ifma.bd1.querybuilder;

import br.edu.ifma.bd1.modelo.Modelo;
import java.util.List;

/**
 * 
 * @author jorgeluis
 */
public class Mapeamento {

    private Modelo model;
    private List<String> fields;

    public Mapeamento(Modelo model, List<String> fields) {
        this.model = model;
        this.fields = fields;
    }

    public Modelo getModel() {
        return model;
    }

    public List<String> getFields() {
        return fields;
    }

}
