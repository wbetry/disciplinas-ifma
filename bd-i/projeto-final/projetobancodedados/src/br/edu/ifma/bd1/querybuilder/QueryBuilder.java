package br.edu.ifma.bd1.querybuilder;

import br.edu.ifma.bd1.modelo.Modelo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author jorgeluis
 */
public class QueryBuilder {

    public String query;

    public QueryBuilder() {
        this.query = "";
    }

    public QueryBuilder(String query) {
        this.query = query;
    }

    public QueryBuilder select(Modelo model) {
        return new QueryBuilder(this.select() + this.fields() + this.from(model));
    }

    public QueryBuilder select(Modelo model, String[] fieldSet) {
        List<String> list = new ArrayList<>();

        list.addAll(Arrays.asList(fieldSet));

        return this.select(model, list);
    }

    public QueryBuilder select(Modelo model, List<String> fieldSet) {
        return new QueryBuilder(this.select() + this.fields(model, fieldSet) + this.from(model));
    }

    public QueryBuilder select(Mapeamento argument) {
        return this.select(argument.getModel(), argument.getFields());
    }

    public QueryBuilder select(List<Mapeamento> arguments) throws Exception {
        if (arguments.isEmpty()) {
            throw new Exception("Lista inválida Baby");
        }

        Iterator<Mapeamento> iterForFieldsInSelect = arguments.iterator();

        StringBuilder query = new StringBuilder("SELECT ");

        for (; iterForFieldsInSelect.hasNext();) {
            Mapeamento map = iterForFieldsInSelect.next();

            Iterator<String> iterMap = map.getFields().iterator();

            for (; iterMap.hasNext();) {
                query.append(map.getModel().getTable()).append(".").append(iterMap.next());
                if (iterMap.hasNext()) {
                    query.append(", ");
                }
            }

            if (iterForFieldsInSelect.hasNext()) {
                query.append(", ");
            }
        }

        query.append(" FROM ").append(arguments.get(0).getModel().getTable()).append("\n");

        Iterator<Mapeamento> iterForJoins = arguments.iterator();
        iterForJoins.next();
        Iterator<Mapeamento> iterForForeignKey = arguments.iterator();

        for (; iterForJoins.hasNext();) {
            Mapeamento map = iterForJoins.next();
            String pktable = map.getModel().getTable();
            String pk = map.getModel().getPrimaryKey();
            Modelo fkModel = iterForForeignKey.next().getModel();
            String fktable = fkModel.getTable();
            String fk = fkModel.getPrimaryKey();
            query.append("JOIN ").append(pktable).append(" ON ").append(pktable).append(".").append(pk).append(" = ").append(fktable).append(".").append(fk);
            query.append("\n");
        }

        System.out.println(query);

        //query.join(iter);
        return new QueryBuilder();
    }

    public Condicoes where(Modelo model, String field, String comparator, String value) {
        StringBuilder query = new StringBuilder(this.query);

        query.append("WHERE ");
        query.append(model.getTable() + "." + field + " ");
        query.append(comparator + " ");
        query.append(value + " ");

        return new Condicoes(new QueryBuilder(query.toString()));
    }

    public QueryBuilder join(String table, String pk) {
        StringBuilder joinClausule = new StringBuilder(this.query);

        return new QueryBuilder("OK");
    }

    private String select() {
        return "SELECT ";
    }

    private String fields() {
        return this.query + "* ";
    }

    private String fields(Modelo model, List<String> fieldSet) {
        StringBuilder fields = new StringBuilder("");

        Iterator<String> iterator = fieldSet.iterator();

        for (; iterator.hasNext();) {
            fields.append(model.getTable() + "." + iterator.next());
            if (iterator.hasNext()) {
                fields.append(",");
            }
            fields.append(" ");
        }

        return this.query + fields.toString();
    }

    private String from(Modelo model) {
        return this.query + "FROM " + model.getTable() + " " + model.getTable() + " ";
    }

    /*public Mapeamento join(Mapeamento map) {
        return new Mapeamento(model, fields);
    }
     */
    public String join(List<Modelo> tables) {
        String query = "SELECT * FROM ";

        Iterator<Modelo> iterator = tables.iterator();

        Modelo model = null;

        if (iterator.hasNext()) {
            model = iterator.next();
            query += model.getTable() + " " + model.getTable() + " ";
        }

        for (; iterator.hasNext();) {
            Modelo joinClausule = iterator.next();

            query += "JOIN " + joinClausule.getTable() + " ON " + joinClausule.getTable() + ".id = " + model.getTable() + ".id ";
        }

        return query;
    }

    @Override
    public String toString() {
        return this.query.toString();
    }
}
