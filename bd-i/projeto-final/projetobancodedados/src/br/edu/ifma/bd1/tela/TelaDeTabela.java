package br.edu.ifma.bd1.tela;

import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

/**
 * 
 * @author jorgeluis
 */
public class TelaDeTabela extends JFrame {

    private JTable table;
    private JScrollPane panel;

    public TelaDeTabela(ResultSet set, String[] columns) throws HeadlessException, SQLException {
        this.setTitle("Tabela");
        this.setSize(800, 600);

        DefaultTableModel model = new DefaultTableModel(null, columns);

        this.table = new JTable();

        for (; set.next();) {
            String[] data = new String[columns.length];
            for (int i = 0; i < columns.length; i++) {
                data[i] = set.getString(columns[i]);
            }
            model.addRow(data);
        }

        this.table.setModel(model);

        this.panel = new JScrollPane(this.table);
        this.add(this.panel, BorderLayout.CENTER);
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

}
