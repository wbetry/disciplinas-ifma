package br.edu.ifma.bd1.testes;

import br.edu.ifma.bd1.modelo.Aluno;
import br.edu.ifma.bd1.modelo.Boletim;
import br.edu.ifma.bd1.modelo.Curso;
import br.edu.ifma.bd1.querybuilder.Mapeamento;
import br.edu.ifma.bd1.querybuilder.QueryBuilder;
import java.util.ArrayList;

/**
 * 
 * @author jorgeluis
 */
public class TesteQueryBuilder {
    
    public static void main(String[] args) throws Exception {
        
        QueryBuilder qb = new QueryBuilder();
        
        ArrayList<String> arr1 = new ArrayList<>();
        arr1.add("Id");
        arr1.add("Nome");
        arr1.add("Email");
        
        ArrayList<String> arr2 = new ArrayList<>();
        arr2.add("N1");
        arr2.add("N2");
        arr2.add("N3");
        
        ArrayList<String> arr3 = new ArrayList<>();
        arr3.add("Nome");
     
        ArrayList<Mapeamento> joins = new ArrayList<>();
        joins.add(new Mapeamento(new Aluno(), arr1));
        joins.add(new Mapeamento(new Boletim(), arr2));
        joins.add(new Mapeamento(new Curso(), arr3));
        
        System.out.println(qb.select(joins));
        
    }
    
}
