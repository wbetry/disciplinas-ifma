/*
 * Aluno: Jorge Luis Lopes Amorim
 * 
 * Conectando o Java no Oracle!
 */

DROP TABLE DISCIPLINA_PROFESSOR;
DROP TABLE BOLETIM;
DROP TABLE PROFESSOR;
DROP TABLE DISCIPLINA;
DROP TABLE PERIODO;
DROP TABLE ALUNO;

CREATE TABLE professor (
	id INT NOT NULL,
	nome VARCHAR(100) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE disciplina (
	id INT NOT NULL,
	nome VARCHAR(64) NOT NULL,
	carga_horaria INT NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE periodo (
	id INT NOT NULL,
	semestre VARCHAR(6) NOT NULL UNIQUE,
	PRIMARY KEY (id)
);

CREATE TABLE disciplina_professor (
	id_disciplina INT NOT NULL,
	id_periodo INT NOT NULL,
	id_professor INT NOT NULL,
	PRIMARY KEY (id_disciplina, id_periodo),
	FOREIGN KEY (id_disciplina) REFERENCES disciplina(id),
	FOREIGN KEY (id_periodo) REFERENCES periodo(id),
	FOREIGN KEY (id_professor) REFERENCES professor(id)
);

CREATE TABLE aluno (
	id INT NOT NULL,
	nome VARCHAR(100) NOT NULL,
	matricula VARCHAR(64) NOT NULL UNIQUE,
	email VARCHAR(64) NOT NULL UNIQUE,
	senha VARCHAR(255) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE boletim (
	id_aluno INT NOT NULL,
	id_disciplina INT NOT NULL,
	id_periodo INT NOT NULL,
	n1 NUMBER(4, 2),
	n2 NUMBER(4, 2),
	n3 NUMBER(4, 2),
	repo NUMBER(4, 2),
	pf NUMBER(4, 2),
	faltas INT,
	PRIMARY KEY (id_aluno, id_disciplina, id_periodo),
	FOREIGN KEY (id_aluno) REFERENCES aluno(id),
	FOREIGN KEY (id_disciplina) REFERENCES disciplina(id),
	FOREIGN KEY (id_periodo) REFERENCES periodo(id),
	CHECK (n1 BETWEEN 0 AND 10),
	CHECK (n2 BETWEEN 0 AND 10),
	CHECK (n3 BETWEEN 0 AND 10),
	CHECK (repo BETWEEN 0 AND 10),
	CHECK (pf BETWEEN 0 AND 10)
);
