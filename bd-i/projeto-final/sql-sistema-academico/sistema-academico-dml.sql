/*
 * Aluno: Jorge Luis Lopes Amorim
 * 
 * Conectando o Java no Oracle!
 */

INSERT INTO professor VALUES (1, 'Afranio');
INSERT INTO professor VALUES (2, 'Beto');
INSERT INTO professor VALUES (3, 'Mauricio');
INSERT INTO professor VALUES (4, 'Rafaela');
INSERT INTO professor VALUES (5, 'Pamela');
INSERT INTO professor VALUES (6, 'James');

INSERT INTO disciplina VALUES (1, 'Introdução à Ciência da Computação', 90);
INSERT INTO disciplina VALUES (2, 'Linguagem de Programação I', 60);
INSERT INTO disciplina VALUES (3, 'Linguagem de Programação II', 90);
INSERT INTO disciplina VALUES (4, 'Linguagem de Programação III', 60);
INSERT INTO disciplina VALUES (5, 'Estrutura de Dados I', 90);
INSERT INTO disciplina VALUES (6, 'Estrutura de Dados II', 60);
INSERT INTO disciplina VALUES (7, 'Banco de Dados I', 60);
INSERT INTO disciplina VALUES (8, 'Banco de Dados II', 60);

INSERT INTO periodo VALUES (1, '2016/1');
INSERT INTO periodo VALUES (2, '2016/2');
INSERT INTO periodo VALUES (3, '2017/1');
INSERT INTO periodo VALUES (4, '2017/2');
INSERT INTO periodo VALUES (5, '2018/1');

INSERT INTO disciplina_professor VALUES (1, 1, 1);
INSERT INTO disciplina_professor VALUES (2, 1, 1);
INSERT INTO disciplina_professor VALUES (7, 1, 1);
INSERT INTO disciplina_professor VALUES (2, 3, 2);
INSERT INTO disciplina_professor VALUES (3, 1, 2);
INSERT INTO disciplina_professor VALUES (4, 1, 3);
INSERT INTO disciplina_professor VALUES (5, 1, 4);
INSERT INTO disciplina_professor VALUES (6, 1, 5);
INSERT INTO disciplina_professor VALUES (8, 1, 6);
INSERT INTO disciplina_professor VALUES (1, 2, 1);
INSERT INTO disciplina_professor VALUES (1, 3, 2);
INSERT INTO disciplina_professor VALUES (2, 2, 3);
INSERT INTO disciplina_professor VALUES (3, 2, 4);
INSERT INTO disciplina_professor VALUES (4, 2, 5);
INSERT INTO disciplina_professor VALUES (5, 2, 6);

INSERT INTO aluno VALUES (1, 'Alvaro', 'SI001', 'aluno1@email.com', '123456');
INSERT INTO aluno VALUES (2, 'Manuela', 'SI002', 'aluno2@email.com', '223456');
INSERT INTO aluno VALUES (3, 'Jim', 'SI003', 'aluno3@email.com', '323456');
INSERT INTO aluno VALUES (4, 'Carlos', 'SI004', 'aluno4@email.com', '423456');
INSERT INTO aluno VALUES (5, 'Alberto', 'SI005', 'aluno5@email.com', '523456');
INSERT INTO aluno VALUES (6, 'Maria', 'SI006', 'aluno6@email.com', '623456');
INSERT INTO aluno VALUES (7, 'Giovana', 'SI007', 'aluno7@email.com', '723456');
INSERT INTO aluno VALUES (8, 'Luana', 'SI008', 'aluno8@email.com', '823456');
INSERT INTO aluno VALUES (9, 'Juvenal', 'SI009', 'aluno9@email.com', '923456');
INSERT INTO aluno VALUES (10, 'Lana', 'SI0010', 'aluno10@email.com', '1023456');
INSERT INTO aluno VALUES (11, 'Paloma', 'SI0011', 'aluno1@e1mail.com', '1234156');
INSERT INTO aluno VALUES (12, 'Anísio', 'SI0012', 'aluno1@ema2il.com', '123456');
INSERT INTO aluno VALUES (13, 'Amanda', 'SI0013', 'aluno1@email3.com', '123456');
INSERT INTO aluno VALUES (14, 'Larissa', 'SI0014', '4aluno1@email.c4om', '123456');
INSERT INTO aluno VALUES (15, 'Silvio', 'SI0015', 'a5luno1@email.com5', '123456');
INSERT INTO aluno VALUES (16, 'Mauro', 'SI0016', 'al6uno1@email.com', '123456');
INSERT INTO aluno VALUES (17, 'Sandro', 'SI0017', 'alu7no1@email.com', '7123456');
INSERT INTO aluno VALUES (18, 'Emerson', 'SI0018', 'alun8o1@email.com', '1283456');

INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (1, 1, 1, 7, 7, 7, 0);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (1, 2, 2, 9.1, 10, 7, 0);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (2, 2, 1, 8.4, 10, 8, 3);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (2, 3, 2, 5.7, 10, 7, 0);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (3, 3, 2, 6.8, 9, 7.8, 1);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (3, 1, 1, 7.5, 7, 7, 5);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (3, 7, 2, 7.2, 9.8, 7.8, 3);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (3, 5, 2, 7.1, 8, 7.8, 7);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (4, 1, 1, 7.2, 7, 7, 0);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (5, 1, 2, 10, 10, 10, 10);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (6, 5, 1, 8.2, 7.1, 7, 7);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (7, 6, 1, 7, 8, 7, 0);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (8, 7, 1, 7.4, 7, 7.9, 8);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (9, 1, 2, 10, 1, 10, 0);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (10, 8, 2, 9.7, 8.8, 7.1, 0);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (11, 5, 2, 7, 7, 9.2, 2);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (12, 4, 2, 7, 7.1, 7, 0);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (13, 5, 1, 6, 7.2, 7, 10);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (10, 6, 1, 7, 8, 7, 0);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (11, 7, 3, 7.4, 7, 7.9, 4);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (12, 1, 2, 9.7, 1.5, 9.9, 0);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (13, 5, 3, 9.7, 8.8, 7.1, 5);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (14, 5, 2, 7.8, 7, 9.2, 0);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (15, 4, 2, 7, 9, 8, 1);
INSERT INTO boletim (id_aluno, id_disciplina, id_periodo, n1, n2, n3, faltas) VALUES (16, 1, 1, 6.3, 9, 7, 8);
