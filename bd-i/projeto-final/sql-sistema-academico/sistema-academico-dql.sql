/*
 * Aluno: Jorge Luis Lopes Amorim
 * 
 * Conectando o Java no Oracle!
 */

/* 
 * Listar todos os alunos que possuem média maior ou igual a 7 no disciplina de Introdução à Ciencia da computação
 */
SELECT DISTINCT a.nome AS "Nome do Aluno" FROM aluno a
JOIN boletim b ON b.id_aluno = a.id
JOIN disciplina c ON c.id = b.id_disciplina
WHERE (b.n1 + b.n2 + b.n3) / 3 >= 7 AND c.nome = 'Introdução à Ciência da Computação'
ORDER BY a.nome;

/*
 * Listar os professores e a quantidade de disciplinas que eles ministraram
 */
SELECT p.nome AS "Nome do Professor", COUNT(*) AS "Disciplinas Ministradas" FROM professor p
JOIN disciplina_professor cp ON cp.id_professor = p.id
JOIN disciplina d ON d.id = cp.id_disciplina
GROUP BY p.nome
ORDER BY p.nome;

/*
 * Listar todos os alunos que tiveram disciplinas no semestre 2016/2
 */
SELECT DISTINCT a.nome AS "Nome do Aluno" FROM aluno a
JOIN boletim b ON b.id_aluno = a.id
JOIN periodo p ON p.id = b.id_periodo
WHERE p.semestre = '2016/2'
ORDER BY a.nome;

/*
 * Listar a média geral de cada disciplina
 */
SELECT d.nome AS "Nome da Disciplina", AVG((b.n1 + b.n2 + b.n3) / 3) AS "Média da Disciplina" FROM disciplina d
JOIN boletim b ON b.id_disciplina = d.id
GROUP BY d.nome
ORDER BY d.nome;

/*
 * O aluno mais faltoso na disciplina de Estrutura de Dados I mininstrada no periodo 2016/2 
 */
SELECT a.nome AS "Nome do Aluno" FROM aluno a
JOIN boletim b ON b.id_aluno = a.id
JOIN disciplina c ON c.id = b.id_disciplina
WHERE b.faltas = (
	SELECT MAX(b.faltas) FROM disciplina c
	JOIN boletim b ON b.id_disciplina = c.id
	JOIN periodo p ON p.id = b.id_periodo
	WHERE p.semestre = '2016/2'
) AND c.nome = 'Estrutura de Dados I';


/* 
 * A lista dos professores que ministraram a disciplina Linguagem de Programação II com o semestre
 */
SELECT pr.nome AS "Professor", pe.semestre AS "Semestre" FROM professor pr
JOIN disciplina_professor cp ON cp.id_professor = pr.id
JOIN disciplina c ON c.id = cp.id_disciplina
JOIN periodo pe ON pe.id = cp.id_periodo
WHERE c.nome = 'Linguagem de Programação II';

/*
 * Gerar o boletim do aluno Jim no periodo 2016/2
 */
SELECT d.nome AS "Disciplina", b.faltas AS "F", b.n1 AS "N1", b.n2 AS "N2", b.n3 AS "N3", (b.n1 + b.n2 + b.n3) / 3 AS "Média" FROM aluno a
JOIN boletim b ON b.id_aluno = a.id
JOIN disciplina d ON d.id = b.id_disciplina
JOIN periodo pe ON pe.id = b.id_periodo
WHERE a.nome = 'Jim' AND pe.semestre = '2016/2';