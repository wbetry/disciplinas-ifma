package gestaodecaixaeletronico;

import gestaodecontas.CadastroContas;
import gestaodecontas.Conta;
import gestaodeexcessoes.ContaInexistenteException;
import gestaodeexcessoes.SaldoInsuficienteException;
import gestaodeexcessoes.SenhaInvalidaException;
import gestaodeexcessoes.ValorIncompativelException;

import java.util.ArrayList;

public class Caixa {

	private Terminal meuCaixaEletronico;
	private CadastroContas bdContas;
	private double saldoCaixa;

	public Caixa(Terminal terminal, CadastroContas bd) {
		this.meuCaixaEletronico = terminal;
		this.bdContas = bd;
	}

	public double consultaSaldo(int numeroDaConta, int senha) throws ContaInexistenteException, SenhaInvalidaException {
		Conta conta = this.bdContas.buscaConta(numeroDaConta);

		if (conta == null) {
			throw new ContaInexistenteException();
		}

		return conta.getSaldo(senha);
	}

	public void efetuaSaque(int numeroDaConta, int senha, double valor)
			throws ValorIncompativelException, ContaInexistenteException, SenhaInvalidaException, SaldoInsuficienteException {
		if (valor < 0 || (valor % 50) != 0 || valor > 500 || valor > this.saldoCaixa) {
			throw new ValorIncompativelException();
		}

		Conta conta = this.bdContas.buscaConta(numeroDaConta);

		if (conta == null) {
			throw new ContaInexistenteException();
		}

		conta.debitaValor(senha, valor, "Saque Automatico");

		this.liberaNotas((int) valor / 50);
		this.saldoCaixa -= valor;

		if (this.saldoCaixa < 500) {
			this.meuCaixaEletronico.setModo(0);
		}
	}

	public void efetuaDepositoEmDinheiro(int numeroDaConta, double valor)
			throws ContaInexistenteException, ValorIncompativelException {
		Conta conta = this.bdContas.buscaConta(numeroDaConta);

		if (conta == null) {
			throw new ContaInexistenteException();
		}

		conta.creditaValor(valor, "Deposito em Dinheiro");
	}

	public void efetuaDepositoEmCheque(int numeroDaConta, double valor)
			throws ValorIncompativelException, ContaInexistenteException {
		Conta conta = this.bdContas.buscaConta(numeroDaConta);

		if (conta == null) {
			throw new ContaInexistenteException();
		}

		conta.creditaValor(valor, "Deposito em Cheque");
	}

	public void efetuaTransferencia(int numeroDaConta, int numeroDaContaDeDestino, int senha, double valor)
			throws ContaInexistenteException, ValorIncompativelException, SenhaInvalidaException, SaldoInsuficienteException {
		
		Conta contaOrigem = this.bdContas.buscaConta(numeroDaConta);
		Conta contaDestino = this.bdContas.buscaConta(numeroDaContaDeDestino);

		if (contaOrigem == null || contaDestino == null) {
			throw new ContaInexistenteException();
		}

		contaOrigem.debitaValor(senha, valor, "Transferencia");

		contaDestino.creditaValor(valor, "Transferencia");
	}

	public void geraExtrato(int numeroDaConta, int senha) throws SenhaInvalidaException {
		Conta conta = this.bdContas.buscaConta(numeroDaConta);
		if (conta == null) {
			System.out.println("Conta Inexistente");
		} else {
			ArrayList<Lancamento> historico = conta.getHistorico(senha);
			double saldoAtual = conta.getSaldo(senha);
			System.out.println("------ Extrato Bancario ------");
			System.out.println("Conta: " + conta.getNumero());
			for (Lancamento lancamento : historico) {
				System.out.println(lancamento.getDescricao() + " = " + lancamento.getValor());
			}
			System.out.println("------------------------------");
			System.out.println("Saldo Atual = " + saldoAtual);
			System.out.println("------------------------------");
		}
	}

	public void recarrega() {
		this.saldoCaixa = 1000;
		this.meuCaixaEletronico.setModo(1);
	}

	private void liberaNotas(int quantidade) {

		while (quantidade-- > 0) {
			System.out.println("===/ R$50,00 /===");
		}

	}

}
