package gestaodecaixaeletronico;

import gestaodecontas.CadastroContas;
import gestaodeexcessoes.ContaInexistenteException;
import gestaodeexcessoes.SaldoInsuficienteException;
import gestaodeexcessoes.SenhaInvalidaException;
import gestaodeexcessoes.ValorIncompativelException;

import java.util.Scanner;

public class Terminal {

	private Caixa meuCaixa;
	private int modoAtual;

	public Terminal(CadastroContas bd) {
		this.meuCaixa = new Caixa(this, bd);
	}

	public void setModo(int modo) {
		if (modo == 0 || modo == 1) {
			this.modoAtual = modo;
		}
	}

	public void iniciaOperacao() {

		int opcao;
		opcao = this.getOpcao();

		while (opcao != 8) {
			switch (opcao) {
			case 1:
				double saldo;
				try {
					saldo = this.meuCaixa.consultaSaldo(this.getInt("Numero da Conta"), this.getInt("Senha"));
					System.out.println("Saldo Atual: " + saldo);
				} catch (ContaInexistenteException e) {
					System.err.println("Conta Inexistente");
				} catch (SenhaInvalidaException e) {
					System.err.println("Senha Invalida");
				}
				break;
			case 2:
				try {
					this.meuCaixa.efetuaSaque(this.getInt("Numero da Conta"), this.getInt("Senha"),
							(double) this.getInt("Valor"));
					System.out.println("Retire o dinheiro");
				} catch (ValorIncompativelException e) {
					System.err.println("Valor Inconsistente");
				} catch (ContaInexistenteException e) {
					System.err.println("Conta Inexistente");
				} catch (SenhaInvalidaException e) {
					System.err.println("Senha Invalida");
				} catch (SaldoInsuficienteException e) {
					System.err.println("Saldo Insuficiente");
				}
				break;
			case 3:
				try {
					this.meuCaixa.efetuaDepositoEmDinheiro(this.getInt("Numero da Conta"),
							(double) this.getInt("Valor"));
					System.out.println("Deposito Realizado com Sucesso");
				} catch (ContaInexistenteException e) {
					System.err.println("Conta Inexistente");
				} catch (ValorIncompativelException e) {
					System.err.println("Voce nao pode depositar valor menor que zero");
				}
				break;
			case 4:
				try {
					this.meuCaixa.efetuaDepositoEmCheque(this.getInt("Numero da Conta"), (double) this.getInt("Valor"));
					System.out.println("Deposito Realizado com Sucesso");
				} catch (ValorIncompativelException e) {
					System.err.println("Voce nao pode depositar valor menor que zero");
				} catch (ContaInexistenteException e) {
					System.err.println("Conta Inexistente");
				}
				break;
			case 5:
				try {
					this.meuCaixa.efetuaTransferencia(this.getInt("Numero da Conta de Origem"),
							this.getInt("Numero da Conta de Destino"), this.getInt("Senha"),
							(double) this.getInt("Valor"));
					System.out.println("Transferencia Realizada com Sucesso");
				} catch (ContaInexistenteException e) {
					System.err.println("Conta Origem/Destino nao existe");
				} catch (ValorIncompativelException e) {
					System.err.println("Voce nao pode transferir valor menor que zero");
				} catch (SenhaInvalidaException e) {
					System.err.println("Senha Invalida");
				} catch (SaldoInsuficienteException e) {
					System.err.println("Saldo conta origem Insuficiente");
				}
				break;
			case 6:
				try {
					this.meuCaixa.geraExtrato(this.getInt("Numero da Conta"), this.getInt("Senha"));
				} catch (SenhaInvalidaException e) {
					System.err.println("Senha Invalida");
				}
				break;
			case 7:
				this.meuCaixa.recarrega();
				break;
			}

			opcao = this.getOpcao();
		}

	}

	private int getOpcao() {
		int opcao;

		do {

			if (this.modoAtual == 1) {
				opcao = this.getInt("Opcao: 1 - Consulta Saldo, 2 - Saque, 3 - Deposito em Dinheiro, " + "\n"
						+ "4 - Deposito em Cheque, 5 - Transferencia, 6 - Extrato Bancario, 8 - Sair");
				if ((opcao < 1 & opcao > 6) | opcao == 7) {
					opcao = 0;
				}
			} else {
				opcao = this.getInt("Opcao: 7 - Recarrega, 8 - Sair");
				if (opcao != 7 & opcao != 7) {
					opcao = 0;
				}
			}

		} while (opcao == 0);

		return opcao;

	}

	@SuppressWarnings({ "resource", "unused" })
	private int getInt(String string) {

		Scanner r = new Scanner(System.in);
		System.out.println("Entre com " + string);

		if (r.hasNextInt()) {
			return r.nextInt();
		}

		String st = r.next();
		System.out.println("Erro na Leiura de Dados");

		return 0;

	}

}
