package gestaodecontas;

import java.util.ArrayList;

public class CadastroContas {

	private ArrayList<Conta> contas;

	public CadastroContas() {
		this.contas = new ArrayList<>();
	}

	public boolean insereConta(Conta conta) {
		if (this.buscaConta(conta.getNumero()) == null) {
			this.contas.add(conta);
			return true;
		}
		
		return false;
	}

	public Conta buscaConta(int numeroDaConta) {
		for (Conta conta : this.contas) {
			if (numeroDaConta == conta.getNumero()) {
				return conta;
			}
		}
		
		return null;
	}

}