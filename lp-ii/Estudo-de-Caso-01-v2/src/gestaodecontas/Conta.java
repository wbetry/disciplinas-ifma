package gestaodecontas;

import gestaodecaixaeletronico.Lancamento;
import gestaodeexcessoes.SaldoInsuficienteException;
import gestaodeexcessoes.SenhaInvalidaException;
import gestaodeexcessoes.ValorIncompativelException;

import java.util.ArrayList;

public class Conta {

	private int numero;
	private int senha;
	private double saldo;
	private ArrayList<Lancamento> historico;

	public Conta(int numero, int senha, double saldo) {
		this.numero = numero;
		this.senha = senha;
		this.saldo = saldo;
		this.historico = new ArrayList<>();
	}

	protected final boolean isSenha(int senha) {
		return this.senha == senha;
	}

	public int getNumero() {
		return this.numero;
	}

	protected final double getSaldo() {
		return this.saldo;
	}

	protected final void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public double getSaldo(int senha) throws SenhaInvalidaException {
		if (this.senha != senha) {
			throw new SenhaInvalidaException();
		}

		return this.saldo;
	}

	public ArrayList<Lancamento> getHistorico(int senha) throws SenhaInvalidaException {
		if (this.senha != senha) {
			throw new SenhaInvalidaException();
		}

		return this.historico;
	}

	public void debitaValor(int senha, double valor, String operacao)
			throws ValorIncompativelException, SenhaInvalidaException, SaldoInsuficienteException {
		if (valor < 0) {
			throw new ValorIncompativelException();
		}

		if (this.saldo < valor) {
			throw new SaldoInsuficienteException();
		}

		if (this.senha != senha) {
			throw new SenhaInvalidaException();
		}

		this.registraLancamento(operacao, valor);
		this.saldo -= valor;
	}

	public void creditaValor(double valor, String operacao) throws ValorIncompativelException {
		if (valor < 0) {
			throw new ValorIncompativelException();
		}

		this.registraLancamento(operacao, valor);
		this.saldo += valor;
	}

	protected final void registraLancamento(String operacao, double valor) {
		this.historico.add(new Lancamento(operacao, valor));
	}

}
