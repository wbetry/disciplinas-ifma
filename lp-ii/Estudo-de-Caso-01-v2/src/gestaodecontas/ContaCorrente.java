package gestaodecontas;

import gestaodeexcessoes.SenhaInvalidaException;
import gestaodeexcessoes.ValorIncompativelException;

public class ContaCorrente extends Conta {

	private double limite;

	public ContaCorrente(int numero, int senha, double saldo) {
		super(numero, senha, saldo);

		this.limite = 300;
	}

	public double getLimite() {
		return limite;
	}

	@Override
	public void debitaValor(int senha, double valor, String operacao)
			throws SenhaInvalidaException, ValorIncompativelException {
		if (!super.isSenha(senha)) {
			throw new SenhaInvalidaException();
		}

		if (valor < 0 || super.getSaldo(senha) - this.limite < -this.limite) {
			throw new ValorIncompativelException();
		}

		super.registraLancamento(operacao, valor);
		super.setSaldo(super.getSaldo(senha) - valor);
	}

}
