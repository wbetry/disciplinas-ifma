package gestaodecontas;

import gestaodeexcessoes.ValorIncompativelException;

public class ContaPoupaca extends Conta {

	public ContaPoupaca(int numero, int senha, double saldo) {
		super(numero, senha, saldo);
	}

	public void aplicaRendimento(double taxa) throws ValorIncompativelException {
		if (taxa < 0) {
			throw new ValorIncompativelException();
		}
		
		super.creditaValor((taxa / 100) * super.getSaldo(), "RENDIMENTO");
	}
}
