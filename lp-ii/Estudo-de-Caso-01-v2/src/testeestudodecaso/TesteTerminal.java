package testeestudodecaso;

import gestaodecaixaeletronico.Terminal;
import gestaodecontas.CadastroContas;
import gestaodecontas.Conta;

public class TesteTerminal {

	public static void main(String[] args) {

		CadastroContas bd = new CadastroContas();

		bd.insereConta(new Conta(1, 123, 500));
		bd.insereConta(new Conta(2, 123, 400));
		bd.insereConta(new Conta(3, 123, 100));
		bd.insereConta(new Conta(4, 123, 400));
		bd.insereConta(new Conta(5, 123, 100));
		bd.insereConta(new Conta(6, 123, 400));
		bd.insereConta(new Conta(7, 123, 100));
		bd.insereConta(new Conta(8, 123, 400));

		(new Terminal(bd)).iniciaOperacao();

	}

}
