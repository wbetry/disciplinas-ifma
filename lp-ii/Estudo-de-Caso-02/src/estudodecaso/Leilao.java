package estudodecaso;

import java.util.ArrayList;
import java.util.Iterator;

public class Leilao {
	
	private ArrayList<Lote> lotes;
	private int numeroProxLote;

	public Leilao() {
		this.lotes = new ArrayList<>();
		this.numeroProxLote = 1;
	}

	public void adicionaLote(String descricao) {
		this.lotes.add(new Lote(this.numeroProxLote, descricao));
		this.numeroProxLote++;
	}
	
	public Lote getLote(int numero) {
		if ((numero >= 1) && (numero < this.numeroProxLote)) {
			Lote loteSelecionado = (Lote) this.lotes.get(numero - 1);

			if (loteSelecionado.getNumero() != numero) {
				System.out.println("Erro!!");
			}

			return loteSelecionado;
		}

		System.out.println("Lote numero " + numero + "não existe");
		return null;
	}

	public void mostraLotes() {
		Iterator<Lote> it = this.lotes.iterator();

		while (it.hasNext()) {
			Lote lote = (Lote) it.next();

			System.out.println(lote.getNumero() + ": " + lote.getDescricao());

			Lance melhorLance = lote.getMaiorLance();

			String mensagem = (melhorLance != null) 
					? "Lance:" + melhorLance.getValor() 
					: "(Nenhum Lance)";

			System.out.println(mensagem);
		}
	}
	
	public ArrayList<Lote> getNaoVendidos() {
		ArrayList<Lote> lotesNaoVendidos = new ArrayList<>();
		Iterator<Lote> it = this.lotes.iterator();

		while (it.hasNext()) {
			Lote lote = (Lote) it.next();
			
			if (lote.getMaiorLance() == null) {
				lotesNaoVendidos.add(lote);
			}
		}
		
		return lotesNaoVendidos;
	}
	
	public Lote removeLote(int numero) {
		if (numero < 1) {
			return null;
		}
		
		Iterator<Lote> it = this.lotes.iterator();

		while (it.hasNext()) {
			Lote lote = (Lote) it.next();
			
			if (lote.getNumero() == numero) {
				this.lotes.remove(lote);
				
				return lote;
			}
		}
		
		return null;
	}

	public void close() {
		Iterator<Lote> it = this.lotes.iterator();

		while (it.hasNext()) {
			Lote lote = (Lote) it.next();

			System.out.println(lote.getNumero() + ": " + lote.getDescricao());

			Lance melhorLance = lote.getMaiorLance();

			String mensagem = "Lote " + lote.getNumero() + "\n";
			mensagem += (melhorLance != null)
					? "Nome do arrematador: " + melhorLance.getLicitante() + "\nValor arrematado: "
							+ melhorLance.getValor()
					: "Lote não foi vendido";

			System.out.println(mensagem);
		}

	}

}
