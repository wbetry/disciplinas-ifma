package estudodecaso;

import java.util.Scanner;

public class Teclado {
	private static Scanner teclado = new Scanner(System.in);
	
	public static int getInt(String mensagem) {

		System.out.print(mensagem);

		if (teclado.hasNextInt()) {
			return teclado.nextInt();
		}

		String lixo = teclado.next();
		System.out.println("Erro na Leitura de Dados");

		return 0;
	}
	
	public static String getString(String mensagem) {

		System.out.print(mensagem);
		
		return teclado.nextLine();		
	}
	
}
