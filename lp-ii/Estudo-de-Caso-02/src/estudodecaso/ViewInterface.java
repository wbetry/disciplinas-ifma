package estudodecaso;

public class ViewInterface {
	
	Leilao leilao;
	
	public ViewInterface() {
		this.leilao = new Leilao();
	}
	
	public void iniciaLelao() {
		int opcao;
		
		opcao = Teclado.getInt("");
		
		do {
			
			switch (opcao) {			
			case 1:
				leilao.adicionaLote(Teclado.getString("Entre com a descricao: "));
				break;
			case 2:
				leilao.removeLote(Teclado.getInt("Entre com o numero: "));
				break;
			case 3:
				Lote lote = leilao.getLote(Teclado.getInt("Entre com o numero: "));
				
				if (lote == null) continue;
				Teclado.getString("Entre com o nome: ");
				
				lote.lancePara(, "Entre com o lance: ");
				break;
			
				
			}
			
		} while(opcao != 0);
	}
	
	private void getOpcao() {
		String mensagem;
		
		mensagem = "[1] - Adicionar Lote\n"
				+ "[2] - Remove Lote\n"
				+ "[3] - Dar Lance\n"
				+ "[4] - Mostrar Lotes"
				+ "[0] - Encerrar\n";
		
		Teclado.getInt(mensagem);
	}
	
}
