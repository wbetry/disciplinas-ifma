package acao;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import modelo.usuario.Usuario;

public class Acao {

	private Usuario usuario;
	private Date inicio;
	private Date termino;

	public Acao(Usuario usuario, int prazo) {
		this.usuario = usuario;
		this.inicio = new Date();
		this.termino = calculaRetorno(prazo);
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public Date getInicio() {
		return this.inicio;
	}

	public Date getTermino() {
		return this.termino;
	}

	public boolean isExpirado() {
		Date hoje = new Date();
		return this.termino.before(hoje);
	}

	private Date calculaRetorno(int prazo) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DATE, prazo);
		return calendar.getTime();
	}

}
