package modelo.item;

import modelo.usuario.Usuario;
import modelo.usuario.UsuarioAluno;
import modelo.usuario.UsuarioProfessor;

public class DVD extends Item {

	private int privilegio;
	public static final int TODOS_USUARIOS = 0;
	public static final int USUARIO_ALUNO = 1;
	public static final int USUARIO_PROFESSOR = 2;

	public DVD(String titulo) {
		super(titulo);
	}

	public DVD(String titulo, int privilegio) {
		super(titulo);
		this.setPrivilegio(privilegio);
	}

	public void setPrivilegio(int privilegio) {
		this.privilegio = (privilegio > DVD.USUARIO_PROFESSOR || privilegio < DVD.TODOS_USUARIOS) 
				? DVD.TODOS_USUARIOS
				: privilegio;
	}

	@Override
	public boolean empresta(Usuario usuario, int prazo) {
		if (this.privilegio == DVD.USUARIO_PROFESSOR && !(usuario instanceof UsuarioProfessor)) {
			return false;
		}

		if (this.privilegio == DVD.USUARIO_ALUNO
				&& !(usuario instanceof UsuarioAluno || usuario instanceof UsuarioProfessor)) {
			return false;
		}

		return super.empresta(usuario, 2);
	}

	@Override
	public boolean isDisponivel() {
		return this.getDadosEmprestimo() == null;
	}

}
