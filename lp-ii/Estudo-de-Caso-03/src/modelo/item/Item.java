package modelo.item;

import acao.Emprestimo;
import modelo.usuario.Usuario;

abstract public class Item {

	private String titulo;
	private Emprestimo dadosEmprestimo;

	public Item(String titulo) {
		this.titulo = titulo;
	}

	public String getTitulo() {
		return titulo;
	}

	public Emprestimo getDadosEmprestimo() {
		return this.dadosEmprestimo;
	}

	public boolean empresta(Usuario usuario, int prazo) {
		if (this.isDisponivel()) {
			this.dadosEmprestimo = new Emprestimo(usuario, prazo);
			return true;
		}

		return false;
	}

	public boolean retorna(Usuario usuario) {
		if (this.isEmprestado()) {
			if (usuario.equals(this.dadosEmprestimo.getUsuario())) {
				this.dadosEmprestimo = null;
				return true;
			}
		}

		return false;
	}

	public boolean isEmAtraso() {
		return this.dadosEmprestimo.isExpirado();
	}

	protected boolean isEmprestado() {
		return this.dadosEmprestimo != null;
	}

	abstract public boolean isDisponivel();

}
