package modelo.item;

import acao.Bloqueio;
import modelo.usuario.Usuario;

abstract public class Livro extends Item {

	private Bloqueio dadosBloqueio;
	
	public Livro(String titulo) {
		super(titulo);
	}
	
	public boolean bloqueio(Usuario usuario, int prazo) {
		if (this.isDisponivel()) {
			this.dadosBloqueio = new Bloqueio(usuario, prazo);
			return true;
		}
		
		return false;
	}
	
	public  boolean desbloqueio(Usuario usuario) {
		if (this.isBloqueado()) {
			if (usuario.equals(dadosBloqueio.getUsuario())) {
				this.dadosBloqueio = null;
				return true;
			}
		}
		
		return false;
	}
	
	public boolean isDisponivel() {
		return this.getDadosEmprestimo() == null && this.dadosBloqueio == null;
	}
	
	@Override
	public boolean isEmAtraso() {
		if (this.dadosBloqueio != null) {
			return this.dadosBloqueio.isExpirado();
		}
		
		return super.isEmAtraso();
	}
	
	protected boolean isBloqueado() {
		return this.dadosBloqueio != null;
	}
	
}
