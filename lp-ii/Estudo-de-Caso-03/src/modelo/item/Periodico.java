package modelo.item;

import modelo.usuario.Usuario;

public class Periodico extends Livro {

	public Periodico(String titulo) {
		super(titulo);
	}
	
	@Override
	public boolean empresta(Usuario usuario, int prazo) {
		if (usuario.isProfessor()) {
			return super.empresta(usuario, prazo);
		}
		
		return false;
	}
	
}
