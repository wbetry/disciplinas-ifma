package modelo.usuario;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import modelo.item.Item;

public class Usuario {

	private String nome;
	private List<Item> itensRetirados;

	public Usuario(String nome) {
		this.nome = nome;
		this.itensRetirados = new ArrayList<>();
	}

	public String getNome() {
		return this.nome;
	}

	public int getCotaMaxima() {
		return 2;
	}

	public int getPrazoMaximo() {
		return 4;
	}

	public boolean isProfessor() {
		return false;
	}

	public void retiraItem(Item item) {
		if (this.isAptoARetirar()) {
			if (item.empresta(this, this.getPrazoMaximo())) {
				this.itensRetirados.add(item);
			}
		}
	}

	public void devolveItem(Item item) {
		if (item.retorna(this)) {
			this.itensRetirados.remove(item);
		}
	}

	private boolean isAptoARetirar() {
		return this.itensRetirados.size() <= this.getCotaMaxima() && !this.temPrazoVencido();
	}

	private boolean temPrazoVencido() {
		Iterator<Item> iter = itensRetirados.iterator();

		while (iter.hasNext()) {
			Item item = iter.next();
			if (item.isEmAtraso()) {
				return true;
			}
		}

		return false;
	}

}
