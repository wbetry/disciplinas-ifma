package modelo.usuario;

import java.util.Date;

public class UsuarioAluno extends Usuario {

	private Date dataExpiracao;

	public UsuarioAluno(String nome) {
		super(nome);
		this.dataExpiracao = new Date();
	}

	public UsuarioAluno(String nome, Date expiracao) {
		super(nome);
		this.dataExpiracao = expiracao;
	}

	public int getCotaMaxima() {
		return this.isCadastroExpirado() ? super.getCotaMaxima() : 3;
	}

	public int getPrazoMaximo() {
		return this.isCadastroExpirado() ? super.getPrazoMaximo() : 7;
	}

	public void renovarCadastro(Date expiracao) {
		this.dataExpiracao = expiracao;
	}

	public boolean isCadastroExpirado() {
		Date hoje = new Date();
		return this.dataExpiracao.after(hoje);
	}

}
