package modelo.usuario;

import modelo.item.Livro;

public class UsuarioProfessor extends Usuario {

	public UsuarioProfessor(String nome) {
		super(nome);
	}
	
	public int getCotaMaxima() {
		return 5;
	}

	public int getPrazoMaximo() {
		return 14;
	}
	
	public boolean isProfessor() {
		return true;
	}
	
	public void bloqueio(Livro livro, int prazo) {
		livro.bloqueio(this, prazo);
	}
	
	public void desbloqueio(Livro livro) {
		livro.desbloqueio(this);
	}

}
