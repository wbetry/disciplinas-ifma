package folhasdepagamento;

import java.util.Date;

public interface InterfaceFolha {
	
	Date getDataInicial() throws Exception;
	
	Date getDataFinal() throws Exception;
	
	void incluiDebito(String descricao, double valor) throws Exception;
	
	void incluiCredito(String descricao, double valor) throws Exception;
	
}
