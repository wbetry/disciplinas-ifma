package folhasdepagamento.demonstrativo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import folhasdepagamento.InterfaceFolha;
import folhasdepagamento.funcionario.Funcionario;

public class Demonstrativo implements InterfaceFolha {

	private Funcionario funcionario;
	private Date dataInicial;
	private Date dataFinal;
	private List<Lancamento> lancamentos;
	
	public Demonstrativo(Funcionario funcionario, Date inicio, Date fim) {
		this.funcionario = funcionario;
		this.dataInicial = inicio;
		this.dataFinal = fim;
		this.lancamentos = new ArrayList<>();
	}
	
	public Funcionario getFuncionario() {
		return funcionario;
	}
	
	@Override
	public Date getDataInicial() throws Exception {
		return this.dataInicial;
	}

	@Override
	public Date getDataFinal() throws Exception {
		return this.dataFinal;
	}
	
	@Override
	public void incluiCredito(String historico, double valor) throws Exception {
		this.lancamentos.add(new Lancamento(historico, valor));
	}

	@Override
	public void incluiDebito(String historico, double valor) throws Exception {
		this.lancamentos.add(new Lancamento(historico, -valor));
	}

	public void imprime() {
		float total = 0;
		
		System.out.println(this);
		
		for (Lancamento lancamento : this.lancamentos) {
			System.out.println(lancamento);
			total += lancamento.getValor() ;
		}
		
		System.out.println("Total a pagar: " + total);
		
	}
	
}
