package folhasdepagamento.demonstrativo;

public class Lancamento {

	private String historico;
	private double valor;
	
	public Lancamento(String historico, double valor) {
		this.historico = historico;
		this.valor = valor;
	}
	
	public String getHistorico() {
		return this.historico;
	}

	public double getValor() {
		return this.valor;
	}

	@Override
	public String toString() {
		return "Lancamento [historico=" + historico + ", valor=" + valor + "]";
	}

}
