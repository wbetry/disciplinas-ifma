package folhasdepagamento.evento;

import java.util.Date;

import folhasdepagamento.util.FormataData;

abstract public class Evento {

	private Date data;
	private double valor;
	
	public Evento(Date data) {
		this.data = data;
	}
	
	public Evento(Date data, double valor) {
		this.data = data;
		this.valor = valor;
	}

	public Date getDataEvento() {
		return data;
	}

	public double getValorEvento() {
		return valor;
	}

	public String getTipoEvento() {
		return (this.getClass()).getName();
	}

	public boolean isTipo(String tipo) {
		return tipo.equals(this.getTipoEvento());
	}

	public boolean equals(Evento obj) {
		return this.getTipoEvento().equals(obj.getTipoEvento());
	}

	@Override
	public String toString() {
		return String.format("%s em %s", this.getTipoEvento(), FormataData.format(this.data));
	}

}
