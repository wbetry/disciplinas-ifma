package folhasdepagamento.evento;

import java.util.Date;

import folhasdepagamento.excessao.FolhaException;

public class EventoAtraso extends Evento {

	public EventoAtraso(Date data, double valor) throws FolhaException {
		super(data, valor);
		
		if (valor > 2) {
			throw new FolhaException("Atraso maior que duas horas");
		}
	}

}
