package folhasdepagamento.evento;

import java.util.Date;

import folhasdepagamento.excessao.FolhaException;

public class EventoComissao extends Evento {

	public EventoComissao(Date data, double valor) throws FolhaException {
		super(data, valor);
		
		if (valor < 0) {
			throw new FolhaException("Comissao menor que zero");
		}
	}

}
