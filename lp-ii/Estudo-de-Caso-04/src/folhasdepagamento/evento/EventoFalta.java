package folhasdepagamento.evento;

import java.util.Date;

public class EventoFalta extends Evento {

	public EventoFalta(Date data) {
		super(data);
	}

	@Override
	public String toString() {
		return super.getTipoEvento() + " em " + super.getDataEvento();
	}
}
