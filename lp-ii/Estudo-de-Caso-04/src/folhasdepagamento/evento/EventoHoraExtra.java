package folhasdepagamento.evento;

import java.util.Date;

import folhasdepagamento.excessao.FolhaException;

public class EventoHoraExtra extends Evento {

	public EventoHoraExtra(Date data, double horas) throws FolhaException {
		super(data, horas);

		if (horas > 4) {
			throw new FolhaException("Horas com quantidade maior que 4");
		}
	}

}
