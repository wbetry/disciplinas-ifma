package folhasdepagamento.evento;

import java.util.Date;

import folhasdepagamento.excessao.FolhaException;

public class EventoReajuste extends Evento {

	public EventoReajuste(Date data, double valor) throws FolhaException {
		super(data, valor);

		if (valor < 300) {
			throw new FolhaException("Reajuste menor que 300");
		}
	}

}
