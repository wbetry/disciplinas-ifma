package folhasdepagamento.evento;

import java.util.Date;

import folhasdepagamento.excessao.FolhaException;

public class EventoRecisao extends Evento {

	private int motivo;
	public static final int SE_DEMITIU = 1;
	public static final int FOI_DEMITIDO = 2;
	public static final int APOSENTADO = 3;
	private boolean avisoPrevio;
	public static boolean CUMPRIU_AVISO = true;
	public static boolean AVISO_IDENIZADO = false;

	public EventoRecisao(Date data, int motivo, boolean aviso) throws FolhaException {
		super(data);

		if (!(motivo == SE_DEMITIU || motivo == FOI_DEMITIDO || motivo == APOSENTADO)) {
			throw new FolhaException("Codigo de recisao invalido");
		}

		this.motivo = motivo;
		this.avisoPrevio = aviso;
	}

	public int getMotivo() {
		return motivo;
	}

	public boolean isAvisoPrevio() {
		return avisoPrevio;
	}

	@Override
	public String toString() {
		String mensagem = String.format("%s\nMotivo: \nAviso Previo: ", super.toString(), this.motivo,
				this.avisoPrevio);

		return mensagem;
	}

}
