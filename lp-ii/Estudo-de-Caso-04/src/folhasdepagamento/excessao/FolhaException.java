package folhasdepagamento.excessao;

import folhasdepagamento.evento.Evento;
import folhasdepagamento.funcionario.Funcionario;

public class FolhaException extends Exception {
	
	private static final long serialVersionUID = -4158500773702713215L;
	private String descricao;
	private Funcionario funcionario;
	private Evento evento;
	private Exception excessao;

	public FolhaException(String descricao) {
		this.descricao = descricao;
	}

	public FolhaException(String descricao, Funcionario funcionario) {
		this(descricao);
		this.funcionario = funcionario;
	}

	public FolhaException(String descricao, Funcionario funcionario, Evento evento) {
		this(descricao, funcionario);
		this.evento = evento;
	}

	public FolhaException(String descricao, Funcionario funcionario, Exception excessao) {
		this(descricao, funcionario);
		this.excessao = excessao;
	}

	@Override
	public String toString() {
		String mensagem = String.format(
				"FolhaException: \nDescricao: %s\nFuncionario: %s\nEvento: %s\nExcessao: %s\n\n",
				this.descricao,
				(this.funcionario == null ? " " : this.funcionario),
				(this.evento == null ? " " : this.evento),
				(this.excessao == null ? " " : this.excessao)
		);

		return mensagem;
	}

}
