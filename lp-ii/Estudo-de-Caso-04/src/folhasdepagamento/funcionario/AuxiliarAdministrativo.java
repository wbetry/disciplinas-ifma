package folhasdepagamento.funcionario;

import java.util.Date;

public class AuxiliarAdministrativo extends FuncionarioMensalista {

	public AuxiliarAdministrativo(String nome, Date dataAdmissao, double salarioBase) {
		super(nome, dataAdmissao, salarioBase);
	}

}
