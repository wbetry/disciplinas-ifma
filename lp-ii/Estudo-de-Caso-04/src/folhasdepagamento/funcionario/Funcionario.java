package folhasdepagamento.funcionario;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

import folhasdepagamento.demonstrativo.Demonstrativo;
import folhasdepagamento.evento.Evento;
import folhasdepagamento.evento.EventoReajuste;
import folhasdepagamento.evento.EventoRecisao;
import folhasdepagamento.excessao.FolhaException;
import folhasdepagamento.integracaoexterna.InterfaceConvenio;

abstract public class Funcionario {

	private String nome;
	private double salarioBase;
	private Date dataAdmissao;
	private Date dataSalario;
	private Date dataFechamento;
	private Date dataRecisao;
	private List<Evento> eventos;
	private List<Demonstrativo> demonstrativos;
	private InterfaceConvenio convenio;
	private String idContratoConvenio;

	public Funcionario(String nome, Date dataAdmissao, double salarioBase) {
		this.nome = nome;
		this.dataAdmissao = dataAdmissao;
		this.dataSalario = dataAdmissao;
		this.dataFechamento = dataAdmissao;
		this.salarioBase = salarioBase;
		this.eventos = new ArrayList<>();
		this.demonstrativos = new ArrayList<>();
	}

	public String getNome() {
		return nome;
	}

	public double getSalarioBase() {
		return salarioBase;
	}

	public void registraConvenio(InterfaceConvenio convenio, String id) {
		this.convenio = convenio;
		this.idContratoConvenio = id;
	}

	public void registraEvento(Evento evento) throws FolhaException {
		Date hoje = new Date();
		if (this.dataRecisao != null) {
			throw new FolhaException("Evento para funcioario desligado");
		}

		if (!evento.getDataEvento().after(this.dataFechamento)) {
			throw new FolhaException("Evento com data anterior aa do fechamento");
		}

		if (evento.getDataEvento().after(hoje)) {
			throw new FolhaException("Evento com data futura", this, evento);
		}

		if (this.eventoDuplicado(evento)) {
			throw new FolhaException("Evento duplicado para o funcionario", this, evento);
		}

		this.eventos.add(evento);
	}

	public void geraDemonstrativo() throws Exception {
		Evento evento;
		Demonstrativo demonstrativo;
		Date hoje = new Date();

		Iterator<Evento> lista = this.eventos.iterator();
		while (lista.hasNext()) {
			evento = (Evento) lista.next();

			if (evento.getDataEvento().after(this.dataFechamento)) {
				if (!eventoComum(evento)) {
					try {
						this.processaEvento(evento);
					} catch (FolhaException e) {
						System.err.println(e);
					}
				}
			}
		}

		demonstrativo = new Demonstrativo(this, dataFechamento, hoje);
		try {
			demonstrativo.incluiCredito("Salario Base", this.salarioBase);
		} catch (Exception e) {
			throw new FolhaException("Erro ao Lancar Salario Base", this, e);
		}

		geraLancamentos(demonstrativo);

		if (this.idContratoConvenio != null) {
			try {
				convenio.processaContrato(idContratoConvenio, demonstrativo);
			} catch (Exception e) {
				System.err.println("Erro ao lancar convenio");
			}
		}

		this.demonstrativos.add(demonstrativo);
		this.dataFechamento = hoje;
		demonstrativo.imprime();
	}

	private boolean eventoDuplicado(Evento evento) {
		Iterator<Evento> lista = this.eventos.iterator();

		while (lista.hasNext()) {
			if (evento.equals(lista.next()))
				return true;
		}

		return false;
	}

	private boolean eventoComum(Evento evento) {
		if (evento instanceof EventoReajuste) {
			this.dataSalario = evento.getDataEvento();
			this.salarioBase = evento.getValorEvento();
			return true;
		}

		if (evento instanceof EventoRecisao) {
			this.dataRecisao = evento.getDataEvento();
			return true;
		}

		return false;
	}

	@Override
	public String toString() {
		return super.toString();
	}

	abstract public void geraLancamentos(Demonstrativo demonstrativo) throws Exception;

	abstract public void processaEvento(Evento evento) throws Exception;

}
