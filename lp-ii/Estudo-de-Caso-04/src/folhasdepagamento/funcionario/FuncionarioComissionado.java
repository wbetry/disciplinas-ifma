package folhasdepagamento.funcionario;

import java.util.Date;

import folhasdepagamento.demonstrativo.Demonstrativo;
import folhasdepagamento.evento.Evento;
import folhasdepagamento.evento.EventoComissao;
import folhasdepagamento.excessao.FolhaException;

abstract public class FuncionarioComissionado extends Funcionario {

	private double comissoes;

	public FuncionarioComissionado(String nome, Date dataAdmissao, double salarioBase) {
		super(nome, dataAdmissao, salarioBase);
	}

	@Override
	public void processaEvento(Evento evento) throws FolhaException {
		if (!(evento instanceof EventoComissao)) {
			throw new FolhaException("Evento Invalido para Funcionario comissionado", this, evento);
		}

		this.comissoes += evento.getValorEvento();
	}

	@Override
	public void geraLancamentos(Demonstrativo demonstrativo) throws FolhaException {
		try {
			if (comissoes > 0) {
				demonstrativo.incluiCredito("Comissoes", this.comissoes);
			}
		} catch (Exception e) {
			throw new FolhaException("Erro ao gerar lancamentos de comissoes", this, e);
		}
	}

}
