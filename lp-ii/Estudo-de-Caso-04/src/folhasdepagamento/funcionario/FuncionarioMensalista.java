package folhasdepagamento.funcionario;

import java.util.Date;

import folhasdepagamento.demonstrativo.Demonstrativo;
import folhasdepagamento.evento.Evento;
import folhasdepagamento.evento.EventoAtraso;
import folhasdepagamento.evento.EventoFalta;
import folhasdepagamento.evento.EventoHoraExtra;
import folhasdepagamento.excessao.FolhaException;

abstract public class FuncionarioMensalista extends Funcionario {

	private int horasExtra;
	private int faltas;
	private int horasAtraso;

	public FuncionarioMensalista(String nome, Date dataAdmissao, double salarioBase) {
		super(nome, dataAdmissao, salarioBase);
	}

	@Override
	public void processaEvento(Evento evento) throws FolhaException {
		if (evento instanceof EventoHoraExtra) {
			this.horasExtra += evento.getValorEvento();
		} else if (evento instanceof EventoFalta) {
			this.faltas++;
		} else if (evento instanceof EventoAtraso) {
			this.horasAtraso += evento.getValorEvento();
		} else {
			throw new FolhaException("Evento invalido para mensalista", this, evento);
		}
	}

	@Override
	public void geraLancamentos(Demonstrativo demonstrativo) throws FolhaException {
		double valor;
		double salarioHora;

		try {
			salarioHora = super.getSalarioBase() / 176;

			if (this.horasExtra > 0) {
				valor = this.horasExtra * 2 * salarioHora;
				demonstrativo.incluiCredito("Horas Extras (" + this.horasExtra + " h)", valor);
			}

			if (this.faltas > 0) {
				valor = this.faltas * 8 * salarioHora;
				demonstrativo.incluiDebito("Faltas (" + this.faltas + " dias)", valor);
			}

			if (this.horasAtraso > 0) {
				valor = this.horasAtraso * salarioHora;
				demonstrativo.incluiDebito("Atrasos (" + this.horasAtraso + " h)", valor);
			}
		} catch (Exception e) {
			throw new FolhaException("Erro ao gerar lancamentos do ponto", this, e);
		}
	}

}
