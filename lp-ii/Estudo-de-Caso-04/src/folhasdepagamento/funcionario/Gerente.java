package folhasdepagamento.funcionario;

import java.util.Date;

public class Gerente extends FuncionarioComissionado {

	public Gerente(String nome, Date dataAdmissao, double salarioBase) {
		super(nome, dataAdmissao, salarioBase);
	}

}
