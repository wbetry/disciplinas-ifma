package folhasdepagamento.integracaoexterna;

import folhasdepagamento.InterfaceFolha;

public interface InterfaceConvenio {
	
	abstract public void processaContrato(String id, InterfaceFolha folhaPagamento) throws Exception;
	
}
