package folhasdepagamento.integracaoexterna;

import folhasdepagamento.InterfaceFolha;

public class MedGrupo implements InterfaceConvenio {

	@Override
	public void processaContrato(String id, InterfaceFolha folhaPagamento) throws Exception {
		if (id.equals("1")) {
			folhaPagamento.incluiDebito("Convenio MedGrupo", 30);
		} else {
			throw new Exception();
		}
	}
	
	
	
}
