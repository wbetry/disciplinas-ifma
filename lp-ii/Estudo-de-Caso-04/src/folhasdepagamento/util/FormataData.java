package folhasdepagamento.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class FormataData {
	
	public static String format(Date data) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(data);		
		
		return String.format("%d/%d/%d", calendar.DATE, calendar.MONTH, calendar.YEAR);
	}
	
}
