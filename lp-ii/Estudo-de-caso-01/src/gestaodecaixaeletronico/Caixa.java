package gestaodecaixaeletronico;

import gestaodecontas.CadastroContas;
import gestaodecontas.Conta;

public class Caixa {

	private Terminal meuCaixaEletronico;
	private CadastroContas bdContas;
	private double saldoCaixa;

	public Caixa(Terminal terminal, CadastroContas bd) {
		this.meuCaixaEletronico = terminal;
		this.bdContas = bd;
	}

	public double consultaSaldo(int numeroDaConta, int senha) {
		Conta conta = this.bdContas.buscaConta(numeroDaConta);

		if (conta != null) {
			return conta.getSaldo(senha);
		}

		return -1;
	}

	public String consultaExtrato(int numeroDaConta, int senha) {
		Conta conta = this.bdContas.buscaConta(numeroDaConta);

		if (conta != null) {
			return conta.geraExtrato(senha);
		}

		return null;
	}

	public boolean efetuaSaque(int numeroDaConta, double valor, int senha) {
		if (valor < 0 || (valor % 50) != 0 || valor > 500 || valor > this.saldoCaixa) {
			return false;
		}

		Conta conta = this.bdContas.buscaConta(numeroDaConta);

		if (conta == null || conta.debitaValor(senha, valor, "Saque Automatico") == false) {
			return false;
		}

		this.liberaNotas((int) (valor / 50));
		this.saldoCaixa -= valor;

		if (this.saldoCaixa < 500) {
			this.meuCaixaEletronico.setModo(0);
		}

		return true;
	}

	public boolean efetuaDeposito(int numeroDaConta, double valor, String tipo) {
		if (valor < 0 || (valor % 50) != 0) {
			return false;
		}

		Conta conta = this.bdContas.buscaConta(numeroDaConta);

		String operacao = "Dep�sito em ";
		operacao += tipo;

		if (conta == null || conta.creditaValor(valor, operacao) == false) {
			return false;
		}

		this.saldoCaixa += valor;

		return true;
	}

	public boolean efetuaTransferencia(int numeroDaMinhaConta, int numeroContaDestino, double valor, int minhaSenha) {
		if (valor < 0 || valor > 500 || valor > this.saldoCaixa) {
			return false;
		}

		Conta minhaConta = this.bdContas.buscaConta(numeroDaMinhaConta);
		Conta contaDestino = this.bdContas.buscaConta(numeroContaDestino);

		if (minhaConta == null || contaDestino == null
				|| minhaConta.transfereValor(contaDestino, minhaSenha, valor) == false) {
			return false;
		}

		return true;
	}

	public void recarrega() {
		this.saldoCaixa = 1000;
		this.meuCaixaEletronico.setModo(1);
	}

	private void liberaNotas(int qtd) {
		while (qtd-- > 0) {
			System.out.println("===/ R$50,00 /===");
		}
	}

}
