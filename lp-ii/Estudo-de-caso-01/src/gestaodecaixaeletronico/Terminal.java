package gestaodecaixaeletronico;

import java.util.Scanner;

import gestaodecontas.CadastroContas;

public class Terminal {

	private Caixa meuCaixa;
	private int modoAtual;

	public Terminal(CadastroContas bd) {
		this.meuCaixa = new Caixa(this, bd);
	}

	public void iniciaOperacao() {
		int opcao;
		String mensagem;
		boolean operacaoEfetuada;

		opcao = this.getOpcao();

		while (opcao != 7) {
			switch (opcao) {
			case 1:
				double saldo = this.meuCaixa.consultaSaldo(this.getInt("Numero da conta: "), this.getInt("Senha: "));

				mensagem = (saldo != -1) ? String.format("Saldo Atual: R$%.2f", saldo) : "Conta/senha invalida";

				System.out.println(mensagem);
				break;

			case 2:
				String extrato = this.meuCaixa.consultaExtrato(this.getInt("Numero da conta: "),
						this.getInt("Senha: "));

				mensagem = (extrato != null) ? extrato : "Conta/senha invalida";

				System.out.println(mensagem);
				break;

			case 3:
				this.meuCaixa.recarrega();
				break;

			case 4:
				operacaoEfetuada = this.meuCaixa.efetuaSaque(this.getInt("Numero da Conta: "),
						(double) this.getInt("Valor: "), this.getInt("Senha: "));

				mensagem = (operacaoEfetuada) ? "Retire o dinheiro" : "Pedido de saque recusado";

				System.out.println(mensagem);
				break;

			case 5:
				int forma;
				String tipo = "";

				do {
					System.out.println("[1] - Dinheiro\n[2] - Cheque\n");
					forma = this.getInt("Forma: ");

					if (forma == 1) {
						tipo = "dinheiro";
						break;
					}

					if (forma == 2) {
						tipo = "cheque";
						break;
					}

					forma = 0;
					System.out.println("Opcao invalida! Tente novamente!");
				} while (forma == 0);

				operacaoEfetuada = this.meuCaixa.efetuaDeposito(this.getInt("Numero da Conta: "),
						(double) this.getInt("Valor: "), tipo);

				mensagem = (operacaoEfetuada) ? "Deposito em " + tipo + " efetuado com sucesso"
						: "Erro ao efetuar deposito";

				System.out.println(mensagem);
				break;

			case 6:
				operacaoEfetuada = this.meuCaixa.efetuaTransferencia(this.getInt("Numero da Minha Conta: "),
						this.getInt("Numero da Conta Destino: "), (double) this.getInt("Valor: "),
						this.getInt("Senha: "));

				mensagem = (operacaoEfetuada) ? "Transferencia efetuada com sucesso" : "Erro ao efetuar transferencia ";

				System.out.println(mensagem);
				break;
			}

			opcao = this.getOpcao();
		}
	}

	public void setModo(int modo) {
		if (modo == 0 || modo == 1) {
			this.modoAtual = modo;
		}
	}

	private int getOpcao() {
		int opcao;
		String mensagem;

		do {
			if (this.modoAtual == 1) {
				mensagem = "\n[1] - Consultar Saldo\n[2] - Consultar Extrato\n[4] - Saque\n[5] - Deposito\n[6] - Transferencia\n[7] - Sair\n";
				mensagem += "\nEscolha uma opcao: ";

				opcao = this.getInt(mensagem);

				if (opcao < 1 || opcao > 7) {
					opcao = 0;
				}
			} else {
				mensagem = "\n[3] - Recarrega\n[7] - Sair\n";
				mensagem += "\nEscolha uma opcao: ";

				opcao = this.getInt(mensagem);

				if (opcao != 3 && opcao != 7) {
					opcao = 0;
				}
			}
		} while (opcao == 0);

		return opcao;
	}

	private int getInt(String mensagem) {
		Scanner teclado = new Scanner(System.in);

		System.out.print("Entre com " + mensagem);

		if (teclado.hasNextInt()) {
			return teclado.nextInt();
		}

		String lixo = teclado.next();
		System.out.println("Erro na Leitura de Dados");

		return 0;
	}

}
