package gestaodecontas;

public class CadastroContas {

	private int numeroDeContas;
	private Conta[] contas;

	public CadastroContas(int numeroDeContas) {
		this.contas = new Conta[numeroDeContas + 1];
	}

	public boolean insereConta(Conta conta) {
		if (this.numeroDeContas >= this.contas.length) {
			return false;
		}

		for (int i = 1; i <= this.numeroDeContas; i++) {
			if (conta.getNumero() == this.contas[i].getNumero()) {
				return false;
			}
		}

		this.numeroDeContas++;
		this.contas[this.numeroDeContas] = conta;

		return true;

	}

	public Conta buscaConta(int numero) {
		for (int i = 1; i <= this.numeroDeContas; i++) {
			if (numero == this.contas[i].getNumero()) {
				return this.contas[i];
			}
		}

		return null;
	}

}
