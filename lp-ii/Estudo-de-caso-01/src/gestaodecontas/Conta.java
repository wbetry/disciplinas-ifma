package gestaodecontas;

public class Conta {

	private int numero;
	private int senha;
	private double saldo;
	private String[] historico;
	private double[] valorLancamento;
	private int ultimoLancamento;

	public Conta(int numero, int senha, double saldo) {
		this.numero = numero;
		this.senha = senha;
		this.saldo = saldo;
		this.historico = new String[11];
		this.valorLancamento = new double[11];
	}

	public int getNumero() {
		return this.numero;
	}

	public double getSaldo(int senha) {
		if (senha == this.senha) {
			return this.saldo;
		}

		return -1;
	}

	public String geraExtrato(int senha) {
		if (senha != this.senha) {
			return null;
		}

		String extrato = "=================================\n" + "=== Extrato da conta numero " + this.numero + " ==="
				+ "\n=================================";

		for (int index = 1; index <= this.ultimoLancamento; index++) {
			extrato += String.format("\n%s\tR$%.2f", this.historico[index], this.valorLancamento[index]);
		}

		extrato += "\n\nSaldo atual: " + this.saldo;
		extrato += "\n=================================\n";

		return extrato;
	}

	public boolean debitaValor(int senha, double valor, String operacao) {
		if (valor < 0 || this.senha != senha || this.saldo < valor) {
			return false;
		}

		this.manipulaFila();

		this.historico[this.ultimoLancamento] = operacao;
		this.valorLancamento[this.ultimoLancamento] = -valor;
		this.saldo -= valor;

		return true;
	}

	public boolean creditaValor(double valor, String operacao) {
		if (valor < 0) {
			return false;
		}

		this.manipulaFila();

		this.historico[this.ultimoLancamento] = operacao;
		this.valorLancamento[this.ultimoLancamento] = valor;
		this.saldo += valor;

		return true;
	}

	public boolean transfereValor(Conta contaDestino, int senha, double valor) {
		if (this.debitaValor(senha, valor, "Transferência")) {
			return contaDestino.creditaValor(valor, "Transferência");
		}

		return false;
	}

	/**
	 * Consome a fila de historico de operações
	 */
	private void manipulaFila() {
		if (this.ultimoLancamento == 10) {
			for (int i = 0; i < 10; i++) {
				this.historico[i] = this.historico[i + 1];
				this.valorLancamento[i] = this.valorLancamento[i + 1];
			}
		} else {
			this.ultimoLancamento++;
		}
	}

}
