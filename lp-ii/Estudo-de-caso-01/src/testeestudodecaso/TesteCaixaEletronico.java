package testeestudodecaso;

import gestaodecaixaeletronico.Terminal;
import gestaodecontas.CadastroContas;
import gestaodecontas.Conta;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 *
 */
public class TesteCaixaEletronico {

	public static void main(String[] args) {
		Conta c1 = new Conta(1, 4321, 0);
		Conta c2 = new Conta(2, 1234, -1); // Sei la, mais da certo
		Conta c3 = new Conta(3, 4321, 1000);
		Conta c4 = new Conta(4, 1234, 200);
		Conta c5 = new Conta(5, 123456, 3280);
		Conta c6 = new Conta(6, 654321, 1800);
		Conta c7 = new Conta(7, 123, -800); // Tambem da certo
		Conta c8 = new Conta(7, 321, 5700);

		CadastroContas cadastro = new CadastroContas(10);

		cadastro.insereConta(c1);
		cadastro.insereConta(c2);
		cadastro.insereConta(c3);
		cadastro.insereConta(c4);
		cadastro.insereConta(c5);
		cadastro.insereConta(c6);
		cadastro.insereConta(c7);
		cadastro.insereConta(c8);

		Terminal terminal = new Terminal(cadastro);

		terminal.iniciaOperacao();
	}

}
