package testeestudodecaso;

import gestaodecontas.Conta;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 *
 */
public class TesteConta {
	public static void main(String[] args) {
		Conta c1 = new Conta(1, 1, 0);
		Conta c2 = new Conta(2, 2, 0);

		c1.creditaValor(1000, "Deposito em dinheiro");
		c1.creditaValor(500, "Deposito em Cheque");
		c1.creditaValor(200, "Deposito em dinheiro");
		c1.debitaValor(1, 100, "Saque");
		c1.transfereValor(c2, 1, 400);

		System.out.println(c1.geraExtrato(1));
		System.out.println(c2.geraExtrato(2));
	}
}
