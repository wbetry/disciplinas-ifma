package tarefa2;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 *
 */
public class TestaCliente {

	public static void main(String[] args) {
		CartaoDeCredito cc1 = new CartaoDeCredito();
		cc1.numero = 1;
		cc1.dataDeValidade = "23/10/2017";
		
		CartaoDeCredito cc2 = new CartaoDeCredito();
		cc2.numero = 2;
		cc2.dataDeValidade = "10/10/2020";
		
		System.out.println("Dados do primeiro cart�o de cr�dito");
		System.out.println("N�mero: " + cc1.numero);
		System.out.println("Data de validade: " + cc1.dataDeValidade);
		
		System.out.println("-----------------------------");
		
		System.out.println("Dados do primeiro cart�o de cr�dito");
		System.out.println("N�mero: " + cc2.numero);
		System.out.println("Data de validade: " + cc2.dataDeValidade);
	}

}
