package tarefa3;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 *
 */
public class CadastroDeLivros {
	
	public static void main(String[] args) {
		Livro phpPoo = new Livro();
		
		/* exemplo real */
		phpPoo.nome = "PHP. Programando com Orientação a Objetos";
		phpPoo.descricao = "Um dos melhores livros para POO em PHP";
		phpPoo.autor = "Pablo Dall'Oglio";
		phpPoo.emailParaContato = "pablo@novatec.com";		// email ficticio
		phpPoo.valor = 95.00;
		phpPoo.isbn = "8575224654";
		
		phpPoo.darDesconto(50);
		
		phpPoo.mostraDetalhes();
		
	}
	
}
