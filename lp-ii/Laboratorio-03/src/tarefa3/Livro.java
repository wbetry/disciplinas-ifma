package tarefa3;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 *
 */
public class Livro {
	String nome;
	String autor;
	String emailParaContato;
	String descricao;
	double valor;
	String isbn;
	
	void darDesconto(double desconto) {
		this.valor -= ((desconto / 100) * valor);
	}
	
	void mostraDetalhes() {
		System.out.println("Detalhes do livro");
		
		System.out.println("Nome: " + this.nome);
		System.out.println("Autor: " + this.autor);
		System.out.println("Email para contato: " + this.emailParaContato);
		System.out.println("Descri��o: " + this.descricao);
		System.out.println("Valor: " + this.valor);
		System.out.println("ISBN: " + this.isbn);
	}
}
