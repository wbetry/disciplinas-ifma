package tarefa1;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 *
 */
public class Cliente {
	private String nome;
	private int codigo;
	
	public Cliente(String nome, int codigo) {
		this.nome = nome;
		this.codigo = codigo;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getCodigo() {
		return this.codigo;
	}	
	
}

/* 
 * Quest�o 1 - Letra C
 *
 * Pois um c�digo quando gerado n�o deve ser alterado. Se O mesmo for alterado
 * quebra o encapsulamento do c�digo, e este pode receber outro valor de
 * c�digo. Isto pode gerar inconsist�ncia no sitema.
 * 
 */