package tarefa1;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 *
 */
public class TestaCliente {
	public static void main(String[] args) {
		Cliente c1 = new Cliente("Debora", 1);		
		Cliente c2 = new Cliente("Jo�o", 2);
		
		System.out.println("Dados do primeiro cliente");
		System.out.println("Nome: " + c1.getNome());
		System.out.println("Codigo: " + c1.getCodigo());
		
		System.out.println("-----------------------------");
		
		System.out.println("Dados do primeiro cliente");
		System.out.println("Nome: " + c2.getNome());
		System.out.println("Codigo: " + c2.getCodigo());
	}

}

/*
 * Quest�o 1 - Letra D
 * 
 * Quebra o c�digo. As dependencias que TestaCliente tem de Cliente n�o s�o
 * mais acessiveis e o mesmo gera erros. 
 * 
 */