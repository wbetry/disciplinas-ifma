package tarefa2;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 *
 */
public class CartaoDeCredito {
	
	private int numero;
	private String dataDeValidade;
	
	public CartaoDeCredito(int numero, String dataDeValidade) {
		this.numero = numero;
		this.dataDeValidade = dataDeValidade;
	}
	
	public int getNumero() {
		return this.numero;
	}
	
	public String getDataDeValidade() {
		return this.dataDeValidade;
	}
}
