package tarefa2;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 *
 */
public class TestaCliente {

	public static void main(String[] args) {
		CartaoDeCredito cc1 = new CartaoDeCredito(1, "26/10/2017");		
		CartaoDeCredito cc2 = new CartaoDeCredito(2, "10/10/2020");
		
		System.out.println("Dados do primeiro cart�o de cr�dito");
		System.out.println("N�mero: " + cc1.getNumero());
		System.out.println("Data de validade: " + cc1.getDataDeValidade());
		
		System.out.println("-----------------------------");
		
		System.out.println("Dados do primeiro cart�o de cr�dito");
		System.out.println("N�mero: " + cc2.getNumero());
		System.out.println("Data de validade: " + cc2.getDataDeValidade());
	}

}
