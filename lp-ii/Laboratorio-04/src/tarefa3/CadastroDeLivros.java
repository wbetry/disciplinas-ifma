package tarefa3;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 *
 */
public class CadastroDeLivros {
	
	public static void main(String[] args) {
		Livro phpPoo = new Livro("PHP. Programando com Orientação a Objetos", "Pablo Dall'Oglio", "pablo@novatec.com", 95.00, "8575224654");
		
		phpPoo.setDescricao("Um dos melhores livros para POO em PHP");
		
		phpPoo.darDesconto(50);
		
		phpPoo.mostraDetalhes();
	}
}
