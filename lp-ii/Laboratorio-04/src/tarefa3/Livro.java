package tarefa3;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 *
 */
public class Livro {
	private String nome;
	private String autor;
	private String emailParaContato;
	private String descricao;
	private double valor;
	private String isbn;

	public Livro(String nome, String autor, String emailParaContato, double valor, String isbn) {
		this.nome = nome;
		this.autor = autor;
		this.emailParaContato = emailParaContato;
		this.valor = valor;
		this.isbn = isbn;
	}

	public Livro(String nome, String autor, String emailParaContato, double valor, String isbn, String descricao) {
		this.nome = nome;
		this.autor = autor;
		this.emailParaContato = emailParaContato;
		this.descricao = descricao;
		this.valor = valor;
		this.isbn = isbn;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getAutor() {
		return this.autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getEmailParaContato() {
		return this.emailParaContato;
	}

	public void setEmailParaContato(String emailParaContato) {
		this.emailParaContato = emailParaContato;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return this.valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public String getIsbn() {
		return this.isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	void darDesconto(double desconto) {
		this.valor -= ((desconto / 100) * valor);
	}
	
	void mostraDetalhes() {
		System.out.println("Detalhes do livro");
		
		System.out.println("Nome: " + this.nome);
		System.out.println("Autor: " + this.autor);
		System.out.println("Email para contato: " + this.emailParaContato);
		System.out.println("Descri��o: " + this.descricao);
		System.out.println("Valor: " + this.valor);
		System.out.println("ISBN: " + this.isbn);
	}
}

/*
 * Quest�o 3 - Letra B
 * 
 * Criar sobrecarga de construtor com e sem descri��o.
 * 
 */
