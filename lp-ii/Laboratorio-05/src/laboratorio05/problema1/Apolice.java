package laboratorio05.problema1;

public class Apolice {
	private String nomeSegurado;
	private double valorPremio;
	
	public Apolice(String nomeSegurado, double valorPremio) {
		this.nomeSegurado = nomeSegurado;
		this.valorPremio = valorPremio;
	}
	
	public String getNomeSegurado() {
		return nomeSegurado;
	}
	
	public void setNomeSegurado(String nomeSegurado) {
		this.nomeSegurado = nomeSegurado;
	}
	
	public void imprime() {
		System.out.println("Nome: " + this.nomeSegurado);
		System.out.println("Código: " + this.valorPremio);
	}
	
	public void calcularPremioApolice(int idade) {
		int percentagem;
		
		if(idade >= 18 && idade <= 25) {
			percentagem = 20;
		} else if(idade <= 36) {
			percentagem = 15;
		} else {
			percentagem = 10;
		}
		
		this.valorPremio += (this.valorPremio * percentagem) / 100;
	}
	
	public void oferecerDesconto(String cidade) {
		int desconto = 0;
		
		if (cidade.equals("Curitiba")) {
			desconto = 20;
		} else if(cidade.equals("Rio de Janeiro")) {
			desconto = 15;
		} else if(cidade.equals("São Paulo")) {
			desconto = 10;
		} else if(cidade.equals("Belo Horizonte")) {
			desconto = 5;
		}
		
		this.valorPremio -= ((double) desconto / 100) * valorPremio;
	}
	
}
