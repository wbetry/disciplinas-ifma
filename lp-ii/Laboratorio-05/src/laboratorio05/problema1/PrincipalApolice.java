package laboratorio05.problema1;

public class PrincipalApolice {
	public static void main(String[] args) {
		Apolice ap = new Apolice("Jorge Luis", 2000);
		
		ap.imprime();
		
		ap.calcularPremioApolice(19);
		
		ap.imprime();
		
		ap.oferecerDesconto("Curitiba");
		
		ap.imprime();
	}
}
