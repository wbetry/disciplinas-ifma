package laboratorio05.problema3;

public class Computador {
	public String marca;
	public String cor;
	public String modelo;
	public String numeroSerie;
	public double preco;
	
	public void imprimir() {
		System.out.println("Marca: " + this.marca);
		System.out.println("Cor: " + this.cor);
		System.out.println("Modelo: " + this.modelo);
		System.out.println("Número de Série: " + this.numeroSerie);
		System.out.println("Preço: " + this.preco);
	}
	
	public void calcularValor() {
		double acrescimo = 0;
		if (this.marca.equals("HP")) {
			acrescimo = 30;
		} else if(this.marca.equals("IBM")) {
			acrescimo = 50;
		}
		
		this.preco += (this.preco * acrescimo) / 100;
	}
	
	public int  alterarValor(double valor) {
		if (valor > 0) {
			this.preco = valor;
			return 1;
		}
		
		return 0;
	}
	
}
