package laboratorio05.problema3;

public class PrincipalComputador {
	public static void main(String[] args) {
		
		Computador c1 = new Computador();
		
		c1.marca = "HP";
		c1.cor = "prata";
		c1.modelo = "hh1";
		c1.numeroSerie = "112233";
		c1.preco = 2200;
		
		c1.imprimir();
		
		Computador c2 = new Computador();
		
		c2.marca = "IBM";
		c2.cor = "cinza";
		c2.modelo = "whatson";
		c2.numeroSerie = "445566";
		c2.preco = 50000;
		
		c2.imprimir();
		
		c2.calcularValor();
		
		c2.imprimir();
		
		int status = c2.alterarValor(20000);
		if (status == 1) {
			System.out.println("Valor Alterado");
		} else if(status ==0) {
			System.out.println("Valor Não Alterado");
		}
				
		int novoStatus = c2.alterarValor(-20000);
		if (novoStatus == 1) {
			System.out.println("Valor Alterado");
		} else if(novoStatus ==0) {
			System.out.println("Valor Não Alterado");
		}
		
		c2.imprimir();
	}
	
}
