package tarefa01;

import java.util.ArrayList;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 *
 */
public class GerenciadorDeLivraria {

	public static void main(String[] args) {
		Livraria maranata = new Livraria();

		Autor pablo = new Autor("Pablo Dall'Oglio", "pblo@gmail.com");
		Autor deitel = new Autor("Deitel", "deitel@gmail.com");
		Autor tenenbaum = new Autor("Andrew S. Tenenbaum", "tenenbaum@amoeba.org");

		Livro phpOO = new Livro("PHP Programando com orientação a objetos", pablo, "Livro de PHP", 95);
		Livro javacp = new Livro("Java: Como Programar", deitel, "Livro de Java", 200);
		Livro somod = new Livro("Sistemas Operacionais Modernos", tenenbaum, 150);
		Livro edc = new Livro("Estrutura de Dados Usando C", tenenbaum, "Livro de estrutura de dados em C", 220);

		maranata.adicionaLivro(phpOO);
		maranata.adicionaLivro(javacp);
		maranata.adicionaLivro(somod);
		maranata.adicionaLivro(edc);

		System.err.println("IMPRIMINDO INFORMAÇÕES DO LIVRO \"PHP Programando com orientação a objetos\"");
		System.out.println(maranata.buscaLivroPorTitulo("PHP Programando com orientação a objetos"));

		ArrayList<Livro> listaLivrosAutor;

		System.err.println("\n\nLISTANDO LIVROS DE Andrew S. Tenenbaum\n");
		listaLivrosAutor = maranata.buscaLivroPorAutor("andrew s. tenenbaum");

		for (Livro livro : listaLivrosAutor) {
			System.out.println(livro + "\n");
		}

		System.err.println("\n\nTODOS OS LIVROS\n");
		maranata.imprimeTodosLivros();

		System.out.println("removeu \"Java: Como programar\"? " + maranata.removeLivro("Java: Como programar"));

		System.err.println("\n\nTODOS OS LIVROS\n");
		maranata.imprimeTodosLivros();

		System.err.println("IMPRIMINDO INFORMAÇÕES DO LIVRO \"Java: Como programar\"");
		System.out.println(maranata.buscaLivroPorTitulo("Java: Como programar"));

	}

}
