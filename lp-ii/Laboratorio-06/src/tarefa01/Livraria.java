package tarefa01;

import java.util.ArrayList;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 *
 */
public class Livraria {
	
	private ArrayList<Livro> estoqueDeLivros;
	
	public Livraria() {
		this.estoqueDeLivros = new ArrayList<>();
	}
	
	public void adicionaLivro(Livro livro) {
		this.estoqueDeLivros.add(livro);
	}
	
	public Livro buscaLivroPorTitulo(String titulo) {
		for (Livro livro : this.estoqueDeLivros) {
			if (livro.getTitulo().equalsIgnoreCase(titulo)) {
				return livro;
			}
		}
		
		return null;
	}
	
	public ArrayList<Livro> buscaLivroPorAutor(String autor) {
		ArrayList<Livro> livrosAutor = new ArrayList<>();
		
		for (Livro livro : this.estoqueDeLivros) {
			if (livro.getAutor().getNome().equalsIgnoreCase(autor)) {
				livrosAutor.add(livro);
			}
		}
		
		if (livrosAutor.size() > 0) {
			return livrosAutor;
		}
		
		return null;
	}
	
	public boolean removeLivro(String titulo) {
		Livro livroRemovido = buscaLivroPorTitulo(titulo);
		
		if (livroRemovido != null) {
			this.estoqueDeLivros.remove(livroRemovido);
			return true;
		}
		
		return false;
	}
	
	/**
	 * Método para impressão de todos os livros da livraria 
	 */
	public void imprimeTodosLivros() {
		String informacoes = "";
		for (Livro livro : this.estoqueDeLivros) {
			informacoes += (livro + "\n\n");
		}
		
		System.out.println(informacoes);
	}
	
}
