package tarefa01;

public class Livro {

	private String titulo;
	private String descricao;
	private double valor;
	private String isbn;
	private Autor autor;

	public Livro(String titulo, Autor autor, double valor) {
		this.titulo = titulo;
		this.autor = autor;
		this.valor = valor;
	}

	public Livro(String titulo, Autor autor, String descricao, double valor) {
		this.titulo = titulo;
		this.descricao = descricao;
		this.autor = autor;
		this.valor = valor;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Autor getAutor() {
		return autor;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public void ofereceDescontoDe(double porcentagem) {
		this.valor -= this.valor * porcentagem / 100;
	}

	public String toString() {
		return "Titulo: " + this.titulo + "\nAutor: " + this.autor.getNome() + "\nDescrição: " + this.descricao
				+ "\nValor: R$ " + this.valor + "\nISBN: " + this.isbn;
	}

}