package laboratorio7;

public class Funcionario {

	private String nome;
	private double salario;
	private static double valeRefeicaoDiario;

	public Funcionario(String nome, double salario) {
		this.nome = nome;
		this.salario = salario;
	}

	public String getNome() {
		return this.nome;
	}

	public double getSalario() {
		return this.salario;
	}
	
	public void aumentaSalario(double aumento) {
		this.salario += this.salario * aumento;
	}
	
	public double valorValeRefeicaoDiario() {
		return Funcionario.valeRefeicaoDiario;
	}
	
	public static void reajustaValeRefeicaoDiario(double valor) {
		Funcionario.valeRefeicaoDiario = valor;
	}

	@Override
	public String toString() {
		return "Funcionario [\n\tnome = " + this.nome + 
				"\n\tsalario = " + this.salario + 
				"\n\tvalor refeicao = " + Funcionario.valeRefeicaoDiario + "\n]";
	}

}
