package laboratorio7;

import java.util.ArrayList;

public class TesteFuncionario {

	public static void main(String[] args) {
		
		Funcionario f1 = new Funcionario("Antonio", 1800);
		Funcionario f2 = new Funcionario("Mathias", 3200);
		Funcionario f3 = new Funcionario("Flavio", 2200);
		
		ArrayList<Funcionario> funcionarios = new ArrayList<>();
		funcionarios.add(f1);
		funcionarios.add(f2);
		funcionarios.add(f3);
		
		for (Funcionario funcionario : funcionarios) {
			System.out.println(funcionario.valorValeRefeicaoDiario());
		}
		
		Funcionario.reajustaValeRefeicaoDiario(50);
		
		funcionarios.get(0).reajustaValeRefeicaoDiario(200);
		
		for (Funcionario funcionario : funcionarios) {
			System.out.println(funcionario.valorValeRefeicaoDiario());
		}
		
		for (Funcionario funcionario : funcionarios) {
			System.out.println(funcionario);
		}
	}

}
