package tarefa01;

public class Pixel extends Ponto2D {
	private int cor;

	public Pixel(double x, double y, int cor) {
		super(x, y);
		this.cor = cor % 100;
	}

	public Pixel() {
		this(0, 0, 0);
	}
	
	public Pixel(int cor) {
		this(0, 0, cor);
	}

	public int getCor() {
		return this.cor;
	}

	public void setCor(int cor) {
		this.cor = cor;
	}
	
}
