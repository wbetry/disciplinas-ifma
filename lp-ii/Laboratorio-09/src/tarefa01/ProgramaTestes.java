package tarefa01;

public class ProgramaTestes {

	public static void main(String[] args) {
		Ponto2D p1 = new Ponto2D();
		Ponto3D p2 = new Ponto3D(1, 2, 3);
		Pixel px = new Pixel(1, 2, 5);
		
		p1.desloca(3, 4);
		
		Ponto3D p3 = p2.somaPonto(1, 3, 4);
		
		System.out.println("X = " + p1.getX() + "\tY = " + p1.getY());
		System.out.println("X = " + p2.getX() + "\tY = " + p2.getY() + "\tZ = " + p2.getZ());
		System.out.println("X = " + p3.getX() + "\tY = " + p3.getY() + "\tZ = " + p3.getZ());
		System.out.println("X = " + p3.getX() + "\tY = " + p3.getY() + "\tCor = " + px.getCor());
		
	}

}
