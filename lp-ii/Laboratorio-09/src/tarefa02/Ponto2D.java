package tarefa02;

public class Ponto2D {
	private double x;
	private double y;

	public Ponto2D(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public Ponto2D() {
		this(0, 0);
	}

	public Ponto2D(Ponto2D ponto) {
		this(ponto.getX(), ponto.getY());
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public void desloca(double x, double y) {
		this.x += x;
		this.y += y;
	}

	public Ponto2D somaPonto(double x, double y) {
		return new Ponto2D(this.x + x, this.y + y);
	}

	public Ponto2D somaPonto(Ponto2D p) {
		return new Ponto2D(this.x + p.getX(), this.y + p.getY());
	}

	public String toString() {
		return String.format("X = %.2f\nY= %.2f", this.x, this.y);
	}

	public Ponto2D clone() {
		return new Ponto2D(this);
	}
}
