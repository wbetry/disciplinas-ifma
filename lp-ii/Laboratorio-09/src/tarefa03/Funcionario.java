package tarefa03;

public class Funcionario {

	private String nome;
	private double salario;

	public Funcionario(String nome, double salario) {
		this.nome    = nome;
		this.salario = salario;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getSalario() {
		return this.salario;
	}
	
	protected void setSalario(double salario) {
		this.salario = salario;
	}
	
	public double calculaBonificacao() {
		return 0.1 * this.salario;
	}
	
	public void mostraDados() {
		System.out.println(this);
	}
	
	@Override
	public String toString() {
		return String.format("Nome: %s\nSalario: %.2f\nBonificacao: %.2f", this.nome, this.salario, this.calculaBonificacao());
	}
	
}
