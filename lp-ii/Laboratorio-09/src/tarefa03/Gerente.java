package tarefa03;

public class Gerente extends Funcionario {

	private String usuario;
	private String senha;
	
	public Gerente(String nome, double salario, String usuario, String senha) {
		super(nome, salario);
		this.usuario = usuario;
		this.senha = senha;
	}

	public String getUsuario() {
		return usuario;
	}
	
	public boolean isSenha(String senha) {
		return senha.equals(this.senha);
	}
	
	@Override
	public double calculaBonificacao() {
		return 500;
	}
	
	@Override
	public String toString() {
		return String.format("%s\nUsuario: %s\nSenha: %s", super.toString(), this.usuario, this.senha);
		
	}
	
}
