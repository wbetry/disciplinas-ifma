package tarefa03;

public class Secretaria extends Funcionario {

	private int ramal;

	public Secretaria(String nome, double salario, int ramal) {
		super(nome, salario);
		this.ramal = ramal;
	}

	public int getRamal() {
		return ramal;
	}

	public void setRamal(int ramal) {
		this.ramal = ramal;
	}
	
	@Override
	public String toString() {
		return String.format("%s\nRamal: %s", super.toString(), this.ramal);
	}
	
}
