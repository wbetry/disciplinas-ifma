package tarefa03;

public class Telefonista extends Funcionario{

	private int estacaoDeTrabalho;

	public Telefonista(String nome, double salario, int estacaoDeTrabalho) {
		super(nome, salario);
		this.setEstacaoDeTrabalho(estacaoDeTrabalho);
	}

	public int getEstacaoDeTrabalho() {
		return estacaoDeTrabalho;
	}

	public void setEstacaoDeTrabalho(int estacaoDeTrabalho) {
		this.estacaoDeTrabalho = estacaoDeTrabalho;
	}
	
	@Override
	public String toString() {
		return String.format("%s\nEstacao de trabalho: %d", super.toString(), this.estacaoDeTrabalho);
	}

}
