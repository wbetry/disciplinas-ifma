package tarefa03;

public class Testes {
	
	public static void main(String[] args) {
		
		Funcionario f1 = new Funcionario("Joaozinho", 2560.70);
		
		System.out.println("FUNCIONARIO 1:\n");
		f1.mostraDados();
		
		Funcionario f2 = new Gerente("Carlinhos", 6000, "carlinhos", "99%anjo1%vagabundo");
		
		System.out.println("\n\nFUNCIONARIO 2:\n");
		f2.mostraDados();
		
		Funcionario f3 = new Secretaria("Nadia", 3200.4, 2);
		
		System.out.println("\n\nFUNCIONARIO 3:\n");
		f3.mostraDados();
		
		
		Funcionario f4 = new Telefonista("Gisele", 2800, 2);
		System.out.println("\n\nFUNCIONARIO 4:\n");
		f4.mostraDados();
		
	}
	
}
