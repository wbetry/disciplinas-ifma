package tarefa1;

public interface ISimpleStack {

	abstract public void push(char character);

	abstract public char pop();

	abstract public boolean isEmpty();

	abstract public boolean isFull();

}
