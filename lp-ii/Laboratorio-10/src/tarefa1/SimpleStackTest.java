package tarefa1;

public class SimpleStackTest {

	public static void main(String[] args) {

		ISimpleStack s1 = new FixedSizeStack(4);
		ISimpleStack s2 = new DynamicStack();

		// PUSH S1
		for (int i = 0; i < 10; i++) {
			s1.push('a');
		}

		// POP S1
		for (int i = 0; i < 15; i++) {
			System.out.print(s1.pop() + ", ");
		}

		// PUSH S2
		for (int i = 0; i < 10; i++) {
			s2.push('e');
		}

		// POP S2
		for (int i = 0; i < 15; i++) {
			System.out.print(s2.pop() + ", ");
		}

	}

}
