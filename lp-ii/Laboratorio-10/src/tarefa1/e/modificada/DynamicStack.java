package tarefa1.e.modificada;

public class DynamicStack implements IStack {

	private char[] array;
	private int top;

	public DynamicStack() {
		array = new char[10];
	}

	@Override
	public void push(char character) {
		if (this.array.length == this.top) {
			char[] newArray = new char[2 * this.array.length];

			int len = this.array.length;
			for (int i = 0; i < len; i++) {
				newArray[i] = this.array[i];
			}

			this.array = newArray;
		}

		this.array[this.top++] = character;
	}

	@Override
	public char pop() {
		if (this.isEmpty()) {
			System.out.println("underflow");
			return 0;
		}

		return this.array[--this.top];
	}

	@Override
	public boolean isEmpty() {
		return this.top == 0;
	}

	@Override
	public boolean isFull() {
		return false;
	}

	@Override
	public void reset() {
		while (!this.isEmpty()) {
			this.pop();
		}
	}

	@Override
	public char peek() {
		if (this.isEmpty()) {
			System.out.println("underflow");
			return 0;
		}

		return this.array[this.top - 1];
	}

	@Override
	public int size() {
		return this.top;
	}
}
