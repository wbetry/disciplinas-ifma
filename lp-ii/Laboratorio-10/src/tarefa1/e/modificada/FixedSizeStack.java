package tarefa1.e.modificada;

public class FixedSizeStack implements IStack {

	private char[] array;
	private int top;

	public FixedSizeStack(int size) {
		array = new char[size];
	}

	@Override
	public void push(char character) {
		if (this.isFull()) {
			System.err.println("overflow");
			return;
		}

		this.array[this.top++] = character;
	}

	@Override
	public char pop() {
		if (this.isEmpty()) {
			System.err.println("underflow");
			return '0';
		}

		return this.array[--this.top];
	}

	@Override
	public boolean isEmpty() {
		return this.top == 0;
	}

	@Override
	public boolean isFull() {
		return this.array.length == this.top;
	}

	@Override
	public void reset() {
		while (!this.isEmpty()) {
			this.pop();
		}
	}

	@Override
	public char peek() {
		if (this.isEmpty()) {
			System.out.println("underflow");
			return 0;
		}

		return this.array[this.top - 1];
	}

	@Override
	public int size() {
		return this.top;
	}

}
