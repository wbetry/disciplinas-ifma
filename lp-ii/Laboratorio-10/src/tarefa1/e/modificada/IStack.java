package tarefa1.e.modificada;

public interface IStack extends ISimpleStack {

	abstract public void reset();

	abstract public char peek();

	abstract public int size();

}
