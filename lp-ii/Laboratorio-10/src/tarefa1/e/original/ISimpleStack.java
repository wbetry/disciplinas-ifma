package tarefa1.e.original;

public interface ISimpleStack {

	abstract public void push(char character);

	abstract public char pop();

	abstract public boolean isEmpty();

	abstract public boolean isFull();

	abstract public void reset();

	abstract public char peek();

	abstract public int size();

}
