package tarefa1.e.original;

public class SimpleStackTest {

	public static void main(String[] args) {

		ISimpleStack s1 = new FixedSizeStack(4);
		ISimpleStack s2 = new DynamicStack();

		// PUSH S1
		for (int i = 0; i < 10; i++) {
			s1.push('a');
		}

		s1.reset();

		// POP S1
		for (int i = 0; i < 15; i++) {
			System.out.print(s1.pop() + ", ");
		}

		// PUSH S2
		for (int i = 0; i < 10; i++) {
			s2.push('e');
		}

		System.out.println(s2.size());
		System.out.println(s2.peek());

		// POP S2
		for (int i = 0; i < 15; i++) {
			System.out.print(s2.pop() + ", ");
		}

	}

}
