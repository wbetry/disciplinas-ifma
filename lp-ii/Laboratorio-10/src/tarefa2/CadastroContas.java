package tarefa2;


import java.util.ArrayList;

public class CadastroContas {

	private ArrayList<Conta> contas;
	

	public CadastroContas() {
		this.contas = new ArrayList<Conta>();
	}

	public boolean insereConta(Conta conta) {
		return this.contas.add(conta);
	}

	public Conta buscaConta(int numcta) {

		for (Conta conta :  this.contas) {
			if (numcta == conta.getNumero()) {
				return conta;
			}
		}
		return null;
	}

}