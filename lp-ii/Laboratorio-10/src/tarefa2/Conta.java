package tarefa2;


public class Conta {

	private int numero;
	private Cliente titular;
	private double saldo;
	
	public Conta(int numero, Cliente titular, double valor){
		this.numero = numero;
		this.titular = titular;
		if(valor < 0){
		    this.saldo = 0;
		}
		else{
			this.saldo = valor;
		}
	}
	
	public Conta(int numero, Cliente titular){
		this.numero = numero;
		this.titular = titular;
	}
	
	public int getNumero() {
		return numero;
	}
	
	public void setTitular(Cliente titular) {
		this.titular = titular;
	}
	
	public Cliente getTitular() {
		return titular;
	}
	
	public boolean deposita(double valor){
		if(valor < 0){
			return false;
		}
		this.saldo += valor;
		return true;
	}
	
	public boolean saca(double valor){
		if(valor < 0 || valor > this.saldo){
			return false;
		}
		this.saldo -= valor;
		this.descontaTarifa();
		return true;
	}
	
	public double verificaSaldo(){
		return this.saldo;
	}
	
	public boolean transfere(Conta contaDestino, double valor){
		if(this.saca(valor)){
			return contaDestino.deposita(valor);
		}
		return false;
	}
	
	private void descontaTarifa(){
		this.saldo -= 0.15;
	}
}
