package tarefa2;

public class ContaCorrente extends Conta implements Tributavel {

	public ContaCorrente(int numero, Cliente titular) {
		super(numero, titular);
	}

	@Override
	public double calculaTributos() {
		return super.verificaSaldo() * 0.01;
	}

}
