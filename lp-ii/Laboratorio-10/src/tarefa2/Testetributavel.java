package tarefa2;

public class Testetributavel {

	public static void main(String[] args) {

		Tributavel t1 = new ContaCorrente(1, new Cliente("Jorge", "123"));
		Tributavel t2 = new SeguroDeVida();
		GerenciadorDeImpostoDeRenda g = new GerenciadorDeImpostoDeRenda();

		g.adiciona(t1);
		g.adiciona(t2);

		System.out.println(g.getTotal());

	}

}
