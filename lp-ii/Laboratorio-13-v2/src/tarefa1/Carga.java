package tarefa1;

public class Carga {

	private String codigo;
	private double precoKm;
	private double kms;
	private String codigoDoCliente;
	private String nomeDoProduto;
	
	public Carga(String codigo, double precoKm, double kms, String codigoDoCliente, String nomeDoProduto) {
		this.codigo = codigo;
		this.precoKm = precoKm;
		this.kms = kms;
		this.codigoDoCliente = codigoDoCliente;
		this.nomeDoProduto = nomeDoProduto;
	}
	
	public double getKms() {
		return this.kms;
	}

	public String getCliente() {
		return this.codigoDoCliente;
	}

	public String getCodigo() {
		return this.codigo;
	}
	
	public double calculaPreco() {
		return this.kms * this.precoKm;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Codigo da Carga: " + this.getCodigo() + "\n");
		sb.append("Codigo do Cliente: " + this.getCliente() + "\n");
		sb.append("Kms: " + this.getKms() + "\n");
		sb.append("Pre�o: " + this.calculaPreco() + "\n");
		return sb.toString();
		
	}
	
}
