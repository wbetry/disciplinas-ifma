package tarefa1;

import java.util.GregorianCalendar;
import java.util.HashSet;

public class Transporte {

	private String codigoDoTransporte;
	private String matricula;
	private GregorianCalendar data;
	private HashSet<Carga> cargasTransportadas;
	
	public Transporte(String codigoDoTransporte, String matricula, GregorianCalendar data) {
		this.codigoDoTransporte = codigoDoTransporte;
		this.matricula = matricula;
		this.data = data;
		this.cargasTransportadas = new HashSet<>();
	}
	
	public HashSet<Carga> getCargas(){
		HashSet<Carga> conjuntoDeCargas = new HashSet<>(this.cargasTransportadas);
		return conjuntoDeCargas;
	}
	
	public void adicionaCarga(Carga carga) {
		this.cargasTransportadas.add(carga);
	}
	
	public void adicionaCargas(HashSet<Carga> conjuntoDeCargas) {
		this.cargasTransportadas.addAll(conjuntoDeCargas);
	}
	
	public HashSet<String> getClientes(){
		HashSet<String> codigosDeClientes = new HashSet<>();
		
		for(Carga carga : this.cargasTransportadas) {
			codigosDeClientes.add(carga.getCliente());
		}
		
		return codigosDeClientes;
	}
	
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("Codigo Transporte: " + this.codigoDoTransporte + "\n");
		s.append("Matricula: " + this.matricula + "\n");
		s.append("Data: " + this.getGregCalToString() + "\n");
		s.append("------- Cargas -------");
		for(Carga carga : this.cargasTransportadas) {
			s.append(carga.toString());
		}
		return s.toString();
		
	}
	
	private String getGregCalToString() {
		StringBuilder s = new StringBuilder();
		s.append(data.get(GregorianCalendar.DAY_OF_MONTH) + "/ ");
		s.append(data.get(GregorianCalendar.MONTH) + "/ ");
		s.append(data.get(GregorianCalendar.YEAR) + "\n");
		return s.toString();
		
	}
}
