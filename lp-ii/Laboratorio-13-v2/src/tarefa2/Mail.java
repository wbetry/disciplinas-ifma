package tarefa2;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Mail {

	private String endereco;
	private String assunto;
	private GregorianCalendar dataDeEnvio;
	private GregorianCalendar dataDeRecebimento;
	private String texto;

	public Mail(String endereco, String assunto, GregorianCalendar dataDeEnvio, 
			GregorianCalendar dataDeRecebimento, String texto) {
		this.endereco = endereco;
		this.assunto = assunto;
		this.dataDeEnvio = dataDeEnvio;
		this.dataDeRecebimento = dataDeRecebimento;
		this.texto = texto;
	}
	
	
	public String toString() {
		
        StringBuilder s = new StringBuilder();
        s.append("De: " + this.endereco + "\n");
        s.append("Assunto: " + this.assunto + "\n");
        s.append("Enviado em: " +
                  this.dataDeEnvio.get(Calendar.DAY_OF_MONTH) + "/ " +
        		  this.dataDeEnvio.get(Calendar.MONTH) + "/ " +
                  this.dataDeEnvio.get(Calendar.YEAR) + "\n");
        s.append("Recebido em: " +
                  this.dataDeRecebimento.get(Calendar.DAY_OF_MONTH) + "/ " +
        		  this.dataDeRecebimento.get(Calendar.MONTH) + "/ " +
                  this.dataDeRecebimento.get(Calendar.YEAR) + "\n");
        s.append(this.texto + "\n");
        return s.toString();
        
	}


	public String getEndereco() {
		return this.endereco;
	}


	public Calendar getDataDeRecebimento() {
		return this.dataDeRecebimento;
	}


	public GregorianCalendar getDataDeEnvio() {
		return this.dataDeEnvio;
	}


	public String getAssunto() {
		return this.assunto;
	}

}
