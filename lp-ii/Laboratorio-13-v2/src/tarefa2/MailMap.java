package tarefa2;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;

public class MailMap {

	private TreeMap<String, ArrayList<Mail>> mailBox;
	
	public MailMap() {
		this.mailBox = new TreeMap<String, ArrayList<Mail>>();
	}
	
	public TreeMap<String, ArrayList<Mail>> getMailBox(){
		TreeMap<String, ArrayList<Mail>> copia = (TreeMap<String, ArrayList<Mail>>) this.mailBox.clone();
		return copia;
	}
	
	public int totalDeEnderecos() {
		return this.mailBox.keySet().size();
	}
	
	public ArrayList<Mail> emailsDe(String endereco){
		ArrayList<Mail> emails = new ArrayList<>();
		for(Mail email : this.mailBox.get(endereco)) {
			emails.add(email);
		}
		return emails;
	}
	
	public void guardaEmail(Mail email) {
		String endereco = email.getEndereco();
		if(this.mailBox.containsKey(endereco)) {
			this.mailBox.get(endereco).add(email);
		}
		else {
			ArrayList<Mail> emails = new ArrayList<>();
			emails.add(email);
			this.mailBox.put(endereco, emails);
		}
	}
	
	public void insereListaEmails(HashSet<Mail> emails) {
		for(Mail email : emails) {
			this.guardaEmail(email);
		}
	}
	
	public int emailsPorEndereco(String endereco) {
		if(this.mailBox.containsKey(endereco)) {
			return this.mailBox.get(endereco).size();
		}
		return 0;
	}
	
	public void eliminaEmailsAnterioresA(GregorianCalendar data) {
		Mail email = null;
		Iterator<Mail> itm = null;
		for(String endereco : this.mailBox.keySet()) {
			itm = this.mailBox.get(endereco).iterator();
			while(itm.hasNext()) {
				email = itm.next();
				if(email.getDataDeRecebimento().before(data)) {
					itm.remove();
				}
			}
		}
	}
	
	public TreeSet<String> enviaramEmailHoje(){
		GregorianCalendar hoje = new GregorianCalendar();
		String sHoje = this.getGregCalToString(hoje);
		TreeSet<String> enderecos = new TreeSet<>();
		for(ArrayList<Mail> listaDeEmails : this.mailBox.values()) {
			for(Mail email : listaDeEmails) {
				if(sHoje.equals(this.getGregCalToString(email.getDataDeEnvio()))) {
					enderecos.add(email.getEndereco());
				}
			}
		}
		return enderecos;
	}
	
	public TreeSet<String> enviadosDoBrasil(){
		TreeSet<String> enderecos = new TreeSet<>();
		for(ArrayList<Mail> listaDeEmails : this.mailBox.values()) {
			for(Mail email : listaDeEmails) {
				if(email.getEndereco().endsWith(".br")) {
					enderecos.add(email.getEndereco());
				}
			}
		}
		return enderecos;
	}
	
	public TreeSet<String> comPalavrasNoAssunto(HashSet<String> palavras){
		TreeSet<String> enderecos = new TreeSet<>();
		boolean existe = false;
		String palavra = "";
		for(ArrayList<Mail> listaDeEmails : this.mailBox.values()) {
			for(Mail email : listaDeEmails) {
				existe = false;
				Iterator<String> itPalavras = palavras.iterator();
				while(itPalavras.hasNext() && !existe) {
					palavra = itPalavras.next();
					if(email.getAssunto().contains(palavra)) {
						enderecos.add(email.getEndereco());
						existe = true;
					}
				}
			}
		}
		return enderecos;
	}
	
	public HashSet<Mail> emailsComPalavrasNoAssunto(HashSet<String> palavras){
		HashSet<Mail> emails = new HashSet<>();
		boolean existe = false;
		String palavra = "";
		for(ArrayList<Mail> listaDeEmails : this.mailBox.values()) {
			for(Mail email : listaDeEmails) {
				existe = false;
				Iterator<String> itPalavras = palavras.iterator();
				while(itPalavras.hasNext() && !existe) {
					palavra = itPalavras.next();
					if(email.getAssunto().contains(palavra)) {
						emails.add(email);
						existe = true;
					}
				}
			}
		}
		return emails;
	}
	
	public void spamComPalavrasNoAssunto(String endereco, HashSet<String> palavras) {
		TreeSet<String> enderecos = new TreeSet<>();
		boolean existe = false;
		String palavra = "";
		Mail email = null;
		Iterator<Mail> itEmails = mailBox.get(endereco).iterator();
		while(itEmails.hasNext()) {
			existe = false;
			email = itEmails.next();
			Iterator<String> itPalavras = palavras.iterator();
			while(itPalavras.hasNext() && !existe) {
				palavra = itPalavras.next();
				if(email.getAssunto().contains(palavra)) {
					existe = true;
				}
			}
			if(existe) {
				itEmails.remove();
			}
		}
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder("----- Mail MAP -----\n");
		for(String endereco : mailBox.keySet()) {
			sb.append("\tEndere�o: " + endereco + "\n");
			for(Mail email: mailBox.get(endereco)) {
				sb.append(email.toString());
			}
		}
		return sb.toString();
	}
	
	private String getGregCalToString(GregorianCalendar data) {
		StringBuilder s = new StringBuilder();
		s.append(data.get(GregorianCalendar.DAY_OF_MONTH) + "/ ");
		s.append(data.get(GregorianCalendar.MONTH) + "/ ");
		s.append(data.get(GregorianCalendar.YEAR) + "\n");
		return s.toString();
		
	}
	
}