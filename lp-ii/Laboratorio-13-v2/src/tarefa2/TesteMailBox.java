package tarefa2;

import java.util.GregorianCalendar;
import java.util.HashSet;

public class TesteMailBox {

	public static void main(String[] args) {

		Mail m1, m2, m3, m4, m5, m6 = null;
		MailMap mp = new MailMap();

		m1 = new Mail("a@ifma.edu.br", "Programa��o em Java", new GregorianCalendar(2018, 1, 18),
				new GregorianCalendar(2018, 1, 18), "Alguns exemplos...");

		m2 = new Mail("b@gmail.com", "Matem�tica Discreta", new GregorianCalendar(2018, 1, 18),
				new GregorianCalendar(2018, 1, 18), "Teremos aula sobre Indu��o Matem�tica...");

		m3 = new Mail("c@ifma.edu.br", "JSF Java", new GregorianCalendar(2018, 1, 16),
				new GregorianCalendar(2018, 1, 16), "Java Server Faces....");

		m4 = new Mail("d@yahoo.com", "free shop", new GregorianCalendar(2018, 1, 17),
				new GregorianCalendar(2018, 1, 17), "boas pr�ticas");

		m5 = new Mail("a@ifma.edu.br", "Envio de Relat�rio", new GregorianCalendar(2018, 1, 15),
				new GregorianCalendar(2018, 1, 15), "Ver Relat�rio");

		m6 = new Mail("a@hotmail.com", "compre seu imovel", new GregorianCalendar(2018, 1, 10),
				new GregorianCalendar(2018, 1, 10), "Acesse http://.....");

		mp.guardaEmail(m1);
		mp.guardaEmail(m2);
		mp.guardaEmail(m3);
		mp.guardaEmail(m4);
		mp.guardaEmail(m5);
		mp.guardaEmail(m6);

		System.out.println(mp.toString());
		
		HashSet<String> palavras = new HashSet<>();
		palavras.add("imovel");
		
		mp.spamComPalavrasNoAssunto("a@hotmail.com", palavras);
		
		System.out.println(mp.toString());

	}

}
