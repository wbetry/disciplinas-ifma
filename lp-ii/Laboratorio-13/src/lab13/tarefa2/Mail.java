package lab13.tarefa2;

import java.util.GregorianCalendar;

public class Mail {

	private String address;
	private String subject;
	private GregorianCalendar dateOfSend;
	private GregorianCalendar dateOfReceipt;
	private String text;

	public Mail(String address, String subject, GregorianCalendar dateOfSend, GregorianCalendar dateOfReceipt,
			String text) {
		this.address = address;
		this.subject = subject;
		this.dateOfSend = dateOfSend;
		this.dateOfReceipt = dateOfReceipt;
		this.text = text;
	}

	public String getSubject() {
		return subject;
	}
	
	public GregorianCalendar getDateOfReceipt() {
		return dateOfReceipt;
	}
	
}
