package lab13.tarefa2;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

public class MailMap {

	private TreeMap<String, ArrayList<Mail>> mailBox;

	public MailMap() {
		this.mailBox = new TreeMap<String, ArrayList<Mail>>();
	}
	
	public TreeMap<String, ArrayList<Mail>> getMailBox() {
		return (TreeMap<String, ArrayList<Mail>>) this.mailBox.clone();
	}
	
	public int getTotalAnddress() {
		return this.mailBox.keySet().size();
	}
	
	public void saveNewEmail(String address, Mail mail) {
		if (this.mailBox.containsKey(address)) {
			this.mailBox.get(address).add(mail);
			return;
		}
		
		ArrayList<Mail> listOfMails = new ArrayList<>();
		listOfMails.add(mail);
		
		this.mailBox.put(address, listOfMails);
	}
	
	public int emailsSameOrigin(String address) throws Exception {
		if (!this.mailBox.containsKey(address)) {
			throw new Exception("Endereco inexistente");
		}
	
		return this.mailBox.get(address).size();
	}
	
	public void insertSetOfEmails(String address, ArrayList<Mail> listOfMails) {
		for (Mail mail : listOfMails) {
			this.saveNewEmail(address, mail);
		}
	}
	
	public List<String> selectAddressesOfEmailContainingWordList(List<String> wordList) {
		List<String> listOfAddresses = new ArrayList<>();
		
		for (String address : this.mailBox.keySet()) {
			for (Mail email : this.mailBox.get(address)) {
				if (verifyIfAListContaisAString(wordList, email.getSubject())) {
					listOfAddresses.add(address);
				}
			}
		}
		
		return listOfAddresses;
	}
	
	private boolean verifyIfAListContaisAString(List<String> wordList, String string) {
		for (String word : wordList) {
			if (! string.contains(word)) {
				return false;
			}
		}
		
		return true;
	}
	
	public Set<Mail> selectEmailContainingWordList(List<String> wordList) {
		Set<Mail> setOfMails = new HashSet<>();
		
		for (String address : this.mailBox.keySet()) {
			for (Mail email : this.mailBox.get(address)) {
				if (verifyIfAListContaisAString(wordList, email.getSubject())) {
					setOfMails.add(email);
				}
			}
		}
		
		return setOfMails;
	}
	
	public void removeAllEmailsBefore(GregorianCalendar date) {
		for (String address : this.mailBox.keySet()) {
			Iterator<Mail> mails = this.mailBox.get(address).iterator();
			
			while (mails.hasNext()) {
				Mail mail = mails.next();
				
				if (mail.getDateOfReceipt().before(date)) {
					mails.remove();
				}
			}
		}
	}
	
	public List<String> addressesOfnow() {
		GregorianCalendar now = new GregorianCalendar();
		List<String> listOfAddresses = new ArrayList<>();
		
		for (String address : this.mailBox.keySet()) {
			for (Mail mail : this.mailBox.get(address)) {
				if (mail.getDateOfReceipt().equals(now)) {
					listOfAddresses.add(address);
				}
			}
		}
		
		return listOfAddresses;
	}
	
}
