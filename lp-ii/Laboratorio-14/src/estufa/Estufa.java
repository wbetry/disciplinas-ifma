package estufa;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 * 
 */
abstract public class Estufa {

	private String codigo;
	private double area;
	private double temperaturaMinima;
	private double temperaturaMaxima;
	private double temperaturaAtual;

	public double valorComercialFixo;

	public Estufa(String codigo, double area, double tempMin, double tempMax, double tempAtual, double valorFixo) {
		this.codigo = codigo;
		this.area = area;
		this.temperaturaMinima = tempMin;
		this.temperaturaMaxima = tempMax;
		this.temperaturaAtual = tempAtual;
		this.valorComercialFixo = valorFixo;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public double getArea() {
		return this.area;
	}

	public double getTemperaturaMinima() {
		return this.temperaturaMinima;
	}

	public double getTemperaturaMaxima() {
		return this.temperaturaMaxima;
	}

	public double getTemperaturaAtual() {
		return this.temperaturaAtual;
	}

	public double getValorComercialFixo() {
		return this.valorComercialFixo;
	}

	public void modificaTemperatura(double temperatura) {
		this.temperaturaAtual = temperatura;
	}

	public double calculaValorTotal() {
		return this.calculaValorComercial() + this.getValorComercialFixo();
	}

	abstract public double calculaValorComercial();

	@Override
	public String toString() {
		return "Estufa [\n\tCodigo: " + this.codigo + "\n\tArea: " + this.area + "\n\tTemperatura Atual: "
				+ this.temperaturaAtual + "\n]";
	}
}
