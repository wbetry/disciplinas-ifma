package estufa;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 * 
 */
public class EstufaFlor extends Estufa {

	private double precoUnitario;
	private int quantidadeProducao;

	public EstufaFlor(String codigo, double area, double tempMin, double tempMax, double tempAtual, double valorFixo,
			double precoProduto, int totalProducao) {
		super(codigo, area, tempMin, tempMax, tempAtual, valorFixo);
		this.precoUnitario = precoProduto;
		this.quantidadeProducao = totalProducao;
	}

	@Override
	public double calculaValorComercial() {
		return this.precoUnitario * this.quantidadeProducao;
	}

}