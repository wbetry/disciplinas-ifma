package estufa;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 * 
 */
public class EstufaFruta extends EstufaUmidade {

	private double precoUnitario;
	private int quantidadeProducao;

	public EstufaFruta(String codigo, double area, double tempMin, double tempMax, double tempAtual, int umidade,
			double valorFixo, double precoProduto, int totalProducao) {
		super(codigo, area, tempMin, tempMax, tempAtual, umidade, valorFixo);
		this.precoUnitario = precoProduto;
		this.quantidadeProducao = totalProducao;
	}

	@Override
	public double calculaValorComercial() {
		return this.precoUnitario * this.quantidadeProducao;
	}

}
