package estufa;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 * 
 */
public class EstufaLegume extends EstufaUmidade {

	private double precoPorQuilo;
	private int quilosProducao;

	public EstufaLegume(String codigo, double area, double tempMin, double tempMax, double tempAtual, int umidade,
			double valorFixo, double precoProduto, int totalProducao) {
		super(codigo, area, tempMin, tempMax, tempAtual, umidade, valorFixo);
		this.precoPorQuilo = precoProduto;
		this.quilosProducao = totalProducao;
	}

	@Override
	public double calculaValorComercial() {
		return this.precoPorQuilo * this.quilosProducao;
	}

}
