package estufa;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 * 
 */
abstract public class EstufaUmidade extends Estufa {

	private int umidade;

	public EstufaUmidade(String codigo, double area, double tempMin, double tempMax, double tempAtual, int umidade,
			double valorFixo) {
		super(codigo, area, tempMin, tempMax, tempAtual, valorFixo);
		this.umidade = umidade;
	}

	public int getUmidade() {
		return this.umidade;
	}

	public void modificaUmidade(int umidade) {
		this.umidade = umidade;
	}

	@Override
	public String toString() {
		return super.toString() + " EstufaUmidade [\n\tUmidade: " + this.umidade + "\n]";
	}

}
