package gerencia;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import estufa.Estufa;
import monitoramento.MonitorDeEstufas;
import registro.RegistroEstufa;
import registro.RegistroEstufaUmidade;
import tempo.Tempo;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 * 
 */
public class GereEstufa {

	private MonitorDeEstufas monitorEstufas;
	private TreeMap<String, Estufa> colecaoDeEstufas;

	public GereEstufa() {
		this.monitorEstufas = new MonitorDeEstufas();
		this.colecaoDeEstufas = new TreeMap<>();
	}

	public void insereEstufa(Estufa estufa) {
		if (this.colecaoDeEstufas.containsKey(estufa.getCodigo())) {
			return;
		}

		this.colecaoDeEstufas.put(estufa.getCodigo(), estufa);
	}

	public void insereEstufa(Set<Estufa> conjuntoEstufas) {
		for (Estufa estufa : conjuntoEstufas) {
			this.insereEstufa(estufa);
		}
	}

	public void insereRegistroEstufa(Tempo tempo, RegistroEstufa registro) {
		if (this.colecaoDeEstufasContemEstufa(registro.getEstufa()) == false) {
			return;
		}

		this.monitorEstufas.adiciona(tempo, registro);
	}

	public Set<RegistroEstufa> pegarRegistros(int hora) {
		if (hora < 0 || hora > 23) {
			// throw new Exception("Erro, hora invalida");
			return new HashSet<>();
		}

		return this.monitorEstufas.busca(hora);
	}

	public boolean todasEstufasTemRegistro(int hora) {
		Set<RegistroEstufa> conjuntoDeRegistrosEmUmaHora = this.pegarRegistros(hora);

		for (String key : this.colecaoDeEstufas.keySet()) {
			boolean statusIteracao = false;
			Estufa estufa = this.colecaoDeEstufas.get(key);

			for (RegistroEstufa registroHora : conjuntoDeRegistrosEmUmaHora) {
				if (registroHora.getEstufa().equals(estufa)) {
					statusIteracao = true;
					break;
				}
			}

			if (statusIteracao == false) {
				return false;
			}
		}

		return true;
	}

	public boolean isRegistroComTemperaturaNormal(RegistroEstufa registro) {
		return registro.isTemperaturaNormal();
	}

	public List<String> estufasEmRiscoTemperatura() {
		List<String> listaDeEstufasEmRisco = new ArrayList<>();

		for (RegistroEstufa registro : this.monitorEstufas.registrosComRisco()) {
			listaDeEstufasEmRisco.add(registro.getCodigoEstufa());
		}

		return listaDeEstufasEmRisco;
	}

	public int totalPerdas() {
		List<String> listaDeEstufasEmRisco = this.estufasEmRiscoTemperatura();

		return listaDeEstufasEmRisco.size();
	}

	public Set<RegistroEstufaUmidade> pegarRegistrosUmidade() {
		Set<RegistroEstufaUmidade> conjuntoRegistrosUmidade = new HashSet<>();
		Map<Tempo, RegistroEstufa> todosRegistros = this.monitorEstufas.getRegistros();

		for (Tempo tempo : todosRegistros.keySet()) {
			RegistroEstufa registro = todosRegistros.get(tempo);

			if (registro instanceof RegistroEstufaUmidade) {
				conjuntoRegistrosUmidade.add((RegistroEstufaUmidade) registro);
			}
		}

		return conjuntoRegistrosUmidade;
	}

	public Set<RegistroEstufaUmidade> pegarRegistrosUmidade(int hora) {
		Set<RegistroEstufaUmidade> conjuntoUmidadeHora = new HashSet<>();

		for (RegistroEstufa registroEstufa : this.pegarRegistros(hora)) {
			if (registroEstufa instanceof RegistroEstufaUmidade) {
				conjuntoUmidadeHora.add((RegistroEstufaUmidade) registroEstufa);
			}
		}

		return conjuntoUmidadeHora;
	}

	public RegistroEstufa primeiroRegistroDia() {
		return this.monitorEstufas.primeiroRegistro();
	}

	public void gerarArquivoTextoParaRegistro() {
		MonitorDeEstufas monitor = this.monitorEstufas;
		Map<Tempo, RegistroEstufa> todosRegistros = monitor.getRegistros();

		try {
			FileWriter escritor = new FileWriter("registros.txt", false);
			BufferedWriter buffer = new BufferedWriter(escritor);

			StringBuilder builder = new StringBuilder();

			for (Tempo tempo : todosRegistros.keySet()) {
				RegistroEstufa registro = todosRegistros.get(tempo);

				String umidade = (registro instanceof RegistroEstufaUmidade)
						? ("," + ((RegistroEstufaUmidade) registro).getUmidade())
						: "";

				// Tempo;Codigo;Temperatura;(Possivel umidade)
				String concat = String.format("%s,%s,%s%s\n", tempo, registro.getCodigoEstufa(),
						registro.getTemperatura(), umidade);
				builder.append(concat);
			}

			buffer.write(builder.toString());

			buffer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean colecaoDeEstufasContemEstufa(Estufa estufa) {
		for (String key : this.colecaoDeEstufas.keySet()) {
			if (this.colecaoDeEstufas.get(key).equals(estufa)) {
				return true;
			}
		}

		return false;
	}

	@Override
	public String toString() {
		return "GereEstufa [monitorEstufas=" + monitorEstufas + "\ncolecaoDeEstufas = [\n\n" + colecaoDeEstufas + "]";
	}

}
