package monitoramento;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import registro.RegistroEstufa;
import tempo.Tempo;
import tempo.TempoComparator;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 * 
 */
public class MonitorDeEstufas {

	private TreeMap<Tempo, RegistroEstufa> mapRegistros;

	public MonitorDeEstufas() {
		this.mapRegistros = new TreeMap<>(new TempoComparator());
	}

	public TreeMap<Tempo, RegistroEstufa> getRegistros() {
		return (TreeMap<Tempo, RegistroEstufa>) this.mapRegistros.clone();
	}

	public void adiciona(Tempo tempo, RegistroEstufa registro) {
		if (this.mapRegistros.containsKey(tempo)) {
			return;
		}

		this.mapRegistros.put(tempo, registro);
	}

	public Set<RegistroEstufa> busca(int hora) {
		Set<RegistroEstufa> registrosEmHora = new HashSet<>();

		for (Tempo tempoRegistro : this.mapRegistros.keySet()) {
			if (tempoRegistro.getHora() == hora) {
				registrosEmHora.add(this.mapRegistros.get(tempoRegistro));
			}
		}

		return registrosEmHora;
	}
	
	public void remove(Tempo tempo) {
		if (this.mapRegistros.containsKey(tempo)) {
			this.mapRegistros.remove(tempo);
		}
	}
	
	public List<RegistroEstufa> registrosComRisco() {
		List<RegistroEstufa> registrosEmRisco = new ArrayList<>();
		
		for (Tempo tempoRegistro : this.mapRegistros.keySet()) {
			RegistroEstufa registro = this.mapRegistros.get(tempoRegistro);
			
			if (!registro.isTemperaturaNormal()) {
				registrosEmRisco.add(registro);
			}
		}
		
		return registrosEmRisco;
	}
	
	public RegistroEstufa primeiroRegistro() {
		return this.mapRegistros.firstEntry().getValue();
	}

}
