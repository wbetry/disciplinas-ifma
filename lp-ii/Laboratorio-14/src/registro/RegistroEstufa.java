package registro;

import estufa.Estufa;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 * 
 */
abstract public class RegistroEstufa {

	private Estufa estufa;
	private String codigoEstufa;
	private double temperaturaEstufa;

	public RegistroEstufa(Estufa estufa) {
		this.estufa = estufa;
		this.codigoEstufa = estufa.getCodigo();
		this.temperaturaEstufa = estufa.getTemperaturaAtual();
	}

	public Estufa getEstufa() {
		return this.estufa;
	}

	public String getCodigoEstufa() {
		return this.codigoEstufa;
	}

	public double getTemperatura() {
		return this.temperaturaEstufa;
	}

	public boolean isTemperaturaNormal() {
		return this.getTemperatura() < estufa.getTemperaturaMaxima()
				&& this.getTemperatura() > estufa.getTemperaturaMinima();
	}

	@Override
	public String toString() {
		return "RegistroEstufa [\n\tCodigo Estufa: " + this.codigoEstufa + "\n\tTemperatura Estufa: "
				+ this.temperaturaEstufa + "\n]";
	}

}
