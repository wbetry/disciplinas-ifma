package registro;

import estufa.Estufa;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 * 
 */
public class RegistroEstufaNormal extends RegistroEstufa {

	public RegistroEstufaNormal(Estufa estufa) {
		super(estufa);
	}

	@Override
	public String toString() {
		return super.toString() + "\n";
	}

}
