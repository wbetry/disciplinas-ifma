package registro;

import estufa.EstufaUmidade;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 * 
 */
public class RegistroEstufaUmidade extends RegistroEstufa {

	private int umidadeEstufa;

	public RegistroEstufaUmidade(EstufaUmidade estufa) {
		super(estufa);

		this.umidadeEstufa = estufa.getUmidade();
	}

	@Override
	public EstufaUmidade getEstufa() {
		return (EstufaUmidade) super.getEstufa();
	}

	public int getUmidade() {
		return this.umidadeEstufa;
	}

	@Override
	public String toString() {
		return super.toString() + " RegistroEstufaUmidade [\n\tUmidade Estufa: " + this.umidadeEstufa + "\n]\n";
	}

}
