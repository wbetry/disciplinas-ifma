package tempo;

import java.util.Arrays;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 * 
 */
public class Tempo {

	private int hora;
	private int minuto;
	private int segundo;

	public Tempo(int hora, int minuto, int segundo) {
		// if ((hora < 0 || hora > 23) || (minuto < 0 || minuto > 59) || (segundo < 0 ||
		// segundo > 59)) {
		// throw new TempoException("Argumento Invalido");
		// }

		this.hora = hora;
		this.minuto = minuto;
		this.segundo = segundo;
	}

	public int getHora() {
		return this.hora;
	}

	public int getMinuto() {
		return this.minuto;
	}

	public int getSegundo() {
		return this.segundo;
	}

	public int getTotalSegundos() {
		return (this.hora * 3600) + (this.minuto * 60) + (this.segundo);
	}

	@Override
	public String toString() {
		return String.format("%02d:%02d:%02d", this.hora, this.minuto, this.segundo);
	}

	@Override
	public int hashCode() {
		int[] campos = { this.hora, this.minuto, this.segundo };

		return Arrays.hashCode(campos);
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Tempo)) {
			return false;
		}

		return this.hashCode() == obj.hashCode();
	}

}
