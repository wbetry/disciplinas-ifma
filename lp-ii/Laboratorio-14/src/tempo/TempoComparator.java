package tempo;

import java.util.Comparator;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 * 
 */
public class TempoComparator implements Comparator<Tempo> {

	@Override
	public int compare(Tempo arg0, Tempo arg1) {
		if (arg0.getTotalSegundos() < arg1.getTotalSegundos()) {
			return -1;
		}

		if (arg0.getTotalSegundos() > arg1.getTotalSegundos()) {
			return 1;
		}

		return 0;
	}

}
