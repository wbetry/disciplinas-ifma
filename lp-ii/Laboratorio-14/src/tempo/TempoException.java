package tempo;

/**
 * 
 * @author Jorge Luis Lopes Amorim
 * 
 */
public class TempoException extends Exception {

	private static final long serialVersionUID = -8925944470592854471L;
	private String mensagem;

	public TempoException() {}

	public TempoException(String menssagem) {
		this.mensagem = menssagem;
	}

	@Override
	public String getMessage() {
		return this.mensagem;
	}

}
