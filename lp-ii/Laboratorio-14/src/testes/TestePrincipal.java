package testes;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import estufa.Estufa;
import estufa.EstufaFlor;
import estufa.EstufaFruta;
import estufa.EstufaLegume;
import estufa.EstufaUmidade;
import gerencia.GereEstufa;
import registro.RegistroEstufa;
import registro.RegistroEstufaNormal;
import registro.RegistroEstufaUmidade;
import tempo.Tempo;

public class TestePrincipal {

	public static void main(String[] args) {
		// Criando o gerenciamento
		GereEstufa gerencia = new GereEstufa();

		// Criando um conjunto para posteriorment adicionar
		Set<Estufa> estufasParaAdicionar = new HashSet<>();

		// Instanciando as estufas para trabalho
		Estufa e1 = new EstufaFlor("A123", 200, 20, 35, 25, 10, 10, 100);
		Estufa e2 = new EstufaFlor("B123", 200, 20, 35, 10, 10, 10, 100);
		Estufa e3 = new EstufaFlor("C123", 100, 20, 35, 25, 10, 10, 100);
		Estufa e4 = new EstufaFruta("D123", 50, 20, 35, 25, 50, 10, 10, 100);
		Estufa e5 = new EstufaFruta("E123", 60, 20, 35, 30, 55, 10, 10, 100);
		Estufa e6 = new EstufaFruta("F123", 10, 20, 35, 40, 60, 10, 10, 100);
		Estufa e7 = new EstufaFruta("G123", 90, 20, 35, 40, 65, 10, 10, 100);
		Estufa e8 = new EstufaLegume("H123", 200, 20, 35, 25, 70, 10, 10, 100);
		Estufa e9 = new EstufaLegume("I123", 200, 20, 35, 10, 40, 10, 10, 100);
		Estufa e10 = new EstufaLegume("J123", 60, 20, 35, 25, 45, 10, 10, 100);

		// Instanciando uma estufa com codigo igual de e10 (esta nao sera adicionada)
		Estufa e11 = new EstufaLegume("J123", 60, 20, 35, 25, 40, 10, 10, 100);

		// Inserindo algumas estufas no conjunto
		estufasParaAdicionar.add(e1);
		estufasParaAdicionar.add(e2);
		estufasParaAdicionar.add(e3);
		estufasParaAdicionar.add(e4);
		estufasParaAdicionar.add(e5);
		estufasParaAdicionar.add(e6);

		// Inserindo o conjunto ao sistema de gerencia (Como conjunto)
		gerencia.insereEstufa(estufasParaAdicionar);

		// Inserindo Estufas individualmente
		gerencia.insereEstufa(e7);
		gerencia.insereEstufa(e8);
		gerencia.insereEstufa(e9);
		gerencia.insereEstufa(e10);

		// Nao insere, pois possui codigo igual ao de e10
		gerencia.insereEstufa(e11);

		// Instanciando os tempos dos registros
		Tempo t0 = new Tempo(1, 1, 1);
		Tempo t1 = new Tempo(2, 2, 2);
		Tempo t2 = new Tempo(3, 3, 3);
		Tempo t3 = new Tempo(4, 4, 4);
		Tempo t4 = new Tempo(5, 5, 5);
		Tempo t5 = new Tempo(5, 20, 20);
		Tempo t6 = new Tempo(5, 10, 0);
		Tempo t7 = new Tempo(5, 10, 10);
		Tempo t8 = new Tempo(10, 10, 10);
		Tempo t9 = new Tempo(10, 10, 20);
		Tempo t10 = new Tempo(1, 0, 0);

		RegistroEstufa r1 = new RegistroEstufaNormal(e1);
		RegistroEstufa r2 = new RegistroEstufaNormal(e2);

		// Adicionando Registros ao sistema
		gerencia.insereRegistroEstufa(t0, r1);
		gerencia.insereRegistroEstufa(t1, r2);
		gerencia.insereRegistroEstufa(t2, new RegistroEstufaNormal(e3));
		gerencia.insereRegistroEstufa(t3, new RegistroEstufaUmidade((EstufaUmidade) e4));
		gerencia.insereRegistroEstufa(t4, new RegistroEstufaUmidade((EstufaUmidade) e5));
		gerencia.insereRegistroEstufa(t5, new RegistroEstufaUmidade((EstufaUmidade) e6));
		gerencia.insereRegistroEstufa(t6, new RegistroEstufaUmidade((EstufaUmidade) e7));
		gerencia.insereRegistroEstufa(t7, new RegistroEstufaUmidade((EstufaUmidade) e8));
		gerencia.insereRegistroEstufa(t8, new RegistroEstufaUmidade((EstufaUmidade) e9));
		gerencia.insereRegistroEstufa(t9, new RegistroEstufaUmidade((EstufaUmidade) e10));
		gerencia.insereRegistroEstufa(t10, new RegistroEstufaNormal(e1));

		// Tentando inserir registro com tempo igual (Nao insere)
		gerencia.insereRegistroEstufa(t0, new RegistroEstufaNormal(e2));

		Set<RegistroEstufa> registrosDahora5 = gerencia.pegarRegistros(5);

		System.out.println("\nTodos os registros na hora 5");
		for (RegistroEstufa registroEstufa : registrosDahora5) {
			System.out.println(registroEstufa.getCodigoEstufa());
		}

		System.out.println("\nTodas as estufas tem registro na hora 5");
		System.out.println(gerencia.todasEstufasTemRegistro(5));

		System.out.println("\nr1 eh um registro com temperatra normal");
		System.out.println(gerencia.isRegistroComTemperaturaNormal(r1));

		System.out.println("\nr2 eh um registro com temperatra normal");
		System.out.println(gerencia.isRegistroComTemperaturaNormal(r2));

		List<String> estufasEmRisco = gerencia.estufasEmRiscoTemperatura();

		System.out.println("\nTodas as estufas em risco de temperatura");
		for (String codigo : estufasEmRisco) {
			System.out.println(codigo);
		}

		System.out.println("\nTotal de perdas");
		System.out.println(gerencia.totalPerdas());

		Set<RegistroEstufaUmidade> registrosUmidade = gerencia.pegarRegistrosUmidade();

		System.out.println("\nTodos os Registros de Humidade");
		for (RegistroEstufaUmidade registroEstufaUmidade : registrosUmidade) {
			System.out.println(registroEstufaUmidade.getCodigoEstufa());
		}

		Set<RegistroEstufaUmidade> registrosUmidadeHora10 = gerencia.pegarRegistrosUmidade(10);

		System.out.println("\nRegistros de umidade na hora 10");
		for (RegistroEstufaUmidade registroEstufaUmidade : registrosUmidadeHora10) {
			System.out.println(registroEstufaUmidade.getCodigoEstufa());
		}

		System.out.println("\nPrimeiro registro do dia");
		System.out.println(gerencia.primeiroRegistroDia());

		gerencia.gerarArquivoTextoParaRegistro();
	}

}
