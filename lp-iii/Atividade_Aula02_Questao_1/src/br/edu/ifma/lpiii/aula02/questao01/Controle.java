/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.aula02.questao01;

/**
 *
 * @author jorge
 */
public class Controle {

    private Produto produto;

    public Controle(Produto produto) {
        this.produto = produto;
        this.produto.testar();
    }

    @Override
    public String toString() {
        return this.produto.toString();
    }

}
