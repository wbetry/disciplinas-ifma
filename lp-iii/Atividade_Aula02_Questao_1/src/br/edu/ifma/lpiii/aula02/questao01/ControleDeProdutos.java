/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.aula02.questao01;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author jorge
 */
public class ControleDeProdutos {

    public static void main(String[] args) {
        ArrayList<Controle> controles = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            controles.add(new Controle(instanciarProdutoAleatorio("" + i)));
        }

        for (Controle controle : controles) {
            System.out.println(controle);
        }
    }
    
    private static Produto instanciarProdutoAleatorio(String serial) {
        int numero = (new Random()).nextInt(2);
        switch (numero) {
            case 0:
                return new Radio(serial);
            case 1:
                return new TV(serial);
            default:
                return instanciarProdutoAleatorio(serial);
        }
    }

}
