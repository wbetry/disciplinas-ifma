/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.aula02.questao01;

/**
 *
 * @author jorge
 */
public abstract class Produto {

    private String serial;
    protected String tipo;
    private String status;

    public Produto(String serial) {
        this.serial = serial;
        this.status = "NÃO TESTADO";
    }

    public boolean testar() {
        if (this.status.equals("APROVADO") || this.status.equals("REPROVADO")) {
            throw new RuntimeException();
        }
        
        double status = Math.random();
        this.status = status < 0.85 ? "APROVADO" : "REPROVADO";
        return status < 0.85;
    }

    @Override
    public String toString() {
        return this.serial + " " + this.tipo  + " " + this.status;
    }

}
