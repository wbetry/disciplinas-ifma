/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.aula02.questao01;

import java.util.Random;

/**
 *
 * @author jorge
 */
public class Radio extends Produto {

    private double estacao;
    private String banda;

    public Radio(String serial) {
        super(serial);
        this.tipo = "Rádio";
        this.trocaEstacao();
        this.banda = (new Random()).nextBoolean() ? "AM" : "FM";
    }

    public String escutar() {
        return this.estacao + " " + this.banda;
    }

    public void trocaEstacao() {
        this.estacao = (new Random()).nextInt(150);
    }

    public void trocaBanda() {
        if (this.banda == "AM") {
            this.banda = "FM";
        } else {
            this.banda = "AM";
        }
    }

    @Override
    public String toString() {
        return super.toString() + " " + this.estacao + " " + this.banda;
    }

}
