/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.aula02.questao01;

import java.util.Random;

/**
 *
 * @author jorge
 */
public class TV extends Produto {

    private int canal;
    
    public TV(String serial) {
        
        super(serial);
        this.tipo = "Tv";
        this.trocaCanal();
    }

    public String assistir() {
        return "" + this.canal;
    }

    public void trocaCanal() {
        this.canal = (new Random()).nextInt(150);
    }

    @Override
    public String toString() {
        return super.toString() + " " + this.canal;
    }

}
