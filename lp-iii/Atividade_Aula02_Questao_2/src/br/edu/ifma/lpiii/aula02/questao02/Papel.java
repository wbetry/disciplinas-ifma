/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.aula02.questao02;

/**
 *
 * @author jorge
 */
public class Papel extends Coisa implements Comparable<Coisa> {

    @Override
    public String toString() {
        return "Papel";
    }

    @Override
    public int compareTo(Coisa coisa) {
        if (coisa instanceof Pedra) {
            return 1;
        }
        if (coisa instanceof Tesoura) {
            return 3;
        }
        return 0;
    }

}
