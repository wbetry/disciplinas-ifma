/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.aula02.questao02;

/**
 *
 * @author jorge
 */
public class Tesoura extends Coisa implements Comparable<Coisa> {

    @Override
    public String toString() {
        return "Tesoura";
    }

    @Override
    public int compareTo(Coisa coisa) {
        if (coisa instanceof Pedra) {
            return 2;
        }
        if (coisa instanceof Papel) {
            return 3;
        }
        return 0;
    }

}
