/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.aula03.pt1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author jorge
 */
public final class TelaComBotao {
    
    private JFrame janela;
    private JButton botao;

    public TelaComBotao() {
        this.setUp();
    }
    
    public void setUp() {
        this.janela = new JFrame();
        
        this.botao = new JButton("Coloque o mouse Aqui");
        this.botao.setVisible(true);
        this.botao.setBackground(Color.CYAN);

        ActionListener cliqueNoBotao = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                botao.setText("Você clicou no Botão");
            }
        };
        
        MouseListener acoesDeMouse = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                return;
            }

            @Override
            public void mousePressed(MouseEvent e) {
                return;
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                botao.setBackground(Color.MAGENTA);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                botao.setBackground(Color.GRAY);
                botao.setText("Clique Aqui");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                botao.setBackground(Color.CYAN);
                botao.setText("Coloque o mouse Aqui");
            }
        };
        
        this.botao.addActionListener(cliqueNoBotao);
        this.botao.addMouseListener(acoesDeMouse);
        
        this.janela.add(botao);
        this.janela.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.janela.setTitle("Tela");
        this.janela.setSize(400, 400);
        this.janela.setVisible(true);
    }
    
}
